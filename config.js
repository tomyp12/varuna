// import {useContext} from 'react';
// import {AuthContext} from './scr/context/AuthContext';

const config = {
  //old DB
  // API_URL: 'http://8.215.68.178:9002',
  //new DB
  // API_URL: 'http://147.139.202.27:9003',
  API_URL: 'http://192.168.40.251:9003',

  // API_URL: 'http://10.0.10.63:9001',
  // API_URL = 'http://149.129.221.207:7009';

  //bali
  API_URL_VSOFT: 'http://192.168.15.2:3001',
  //semarang (baby face)
  // API_URL_VSOFT: 'http://192.168.1.200:80',
  // API_URL_VSOFT: 'http://192.168.1.200:3001',

  //varuna bali (vsoft)
  // IMAGE_HOME: require('./scr/assets/executive/logobali.png'),
  // IMAGE_SONG: require('./scr/assets/executive/logobali.png'),

  //varuna semarang (vsoft)
  IMAGE_HOME: require('./scr/assets/executive/logo-babyface-home.png'),
  IMAGE_SONG: require('./scr/assets/executive/logo-babyface-song.png'),

  //varuna pekalongan
  // IMAGE_HOME: require('./scr/assets/executive/logo-executive-home.png'),
  // IMAGE_SONG: require('./scr/assets/executive/logo-executive-song.png'),

  //private room
  // IMAGE_HOME: require('./scr/assets/executive/private_room.png'),
  // IMAGE_SONG: require('./scr/assets/executive/private_room.png'),
};

export default config;

// const config = () => {
//   const {deviceOutlet} = useContext(AuthContext);
//   let ParseOutlet = JSON.parse(deviceOutlet);

//   let IMAGE_HOME = require('./scr/assets/executive/logo-babyface-home.png');
//   let IMAGE_SONG = require('./scr/assets/executive/logo-babyface-song.png');

//   return {ParseOutlet, IMAGE_HOME, IMAGE_SONG};
// };
// export default config;
