import React, {Component} from 'react';
import RoutersAppOnlineUbeatz from './scr/routers/onlineUbeatz';
import RoutersAppOffLine from './scr/routers/offlineBlank';
import RoutersAppOffLineExecutiveKaraoke from './scr/routers/ExecutiveKaraoke';
import AppExecutive from './scr/routers/AppExecutive';
import {Provider} from 'react-redux';
import {store} from './scr/redux/store';
import {AuthProvider} from './scr/context/AuthContext';
import crashlytics from '@react-native-firebase/crashlytics';
// import AppNav from './scr/routers/AppNav';

import KeepAwake from 'react-native-keep-awake';
import DeviceInfo from 'react-native-device-info';
// import NetInfo from '@react-native-community/netinfo';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Active: true,
    };
  }
  componentDidMount() {
    crashlytics().log('App crash.');

    // NetInfo.addEventListener(state => {
    //   // console.log('Connection type', state.type);
    //   // console.log('Is connected?', state.isConnected);
    //   // console.log('global jairngan:', global.jaringan);
    //   global.jaringan = (
    //     state.isConnected && state.isInternetReachable
    //   ).toString();
    // });

    // DeviceInfo.getUniqueId().then(uniqueId => {
    //   // console.log(uniqueId);
    //   global.Id = parseInt(uniqueId);
    // });
    global.Id = 99;
  }

  render() {
    return (
      <Provider store={store}>
        <AuthProvider>
          <AppExecutive></AppExecutive>
          <KeepAwake />
        </AuthProvider>
      </Provider>
    );
  }
}

export default App;
