// import {useContext} from 'react';
// import {AuthContext} from '../context/AuthContext';

export const WebSockets = (data, Url_Socket) => {
  // const {deviceOutlet} = useContext(AuthContext);
  // let ParseOutlet = JSON.parse(deviceOutlet);
  // console.log('ParseOutlet', ParseOutlet);

  const ws = new WebSocket('http://192.168.40.251:9001/');
  // const ws = new WebSocket(ParseOutlet.ApiSocket);

  // console.log(global.Socket);

  // const ws = new WebSocket('http://149.129.221.207:7000/');

  // const ws = new WebSocket('http://8.215.68.178:7001/');
  if (ws) {
    ws.onerror = ws.onopen = ws.onclose = null;
    ws.close();
  }

  ws.onopen = function () {
    ws.send(JSON.stringify(data));
  };

  ws.onmessage = e => {
    var datal = e.data;
    // console.log('data ws:', e);
    // console.log('SOcket ws:', global.Socket);
    try {
      datal = JSON.parse(e.data);
      // console.log(datal);
      global.command = datal.message.command;
      global.idlagu = datal.message.idlagu;
      global.artis = datal.message.artis;
      global.judul = datal.message.judul;
      global.path = datal.message.path;
      global.playmode = datal.message.playmode;
      global.voc = datal.message.voc;
      global.volume = datal.message.volume;
      global.xvoc = datal.message.xvoc;
      global.vocalon = datal.message.vocalon;
      global.playlist = datal.message.playlist;
      global.songduration = datal.message.songduration;
      global.currentposition = datal.message.currentposition;
      global.key = datal.message.key;
      global.tempo = datal.message.tempo;
    } catch (err) {}
  };
};
