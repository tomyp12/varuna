import React, { Component, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetSTBCode } from '../../../../redux/stb/actions';
import { StbActionTypes } from '../../../../redux/stb/constants';
import { View, TextInput, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import SocketWeb from '../../../../lib/websocket'

const HomeRemote = ()=>{
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [InputStb,setInputStb]=useState({
      "code":"XXXX",
      "iduser": 99,
      "durasi": 4,
      "addtime": 1
      
  })

    const { stb, loading, variable } = useSelector((state) => ({
      stb: state.Stb.stb,
      loading: state.Stb.loading,
      variable: state.Stb.variable,
  }));


        const ClickLoginSTB= () =>{
          dispatch(Action_SetSTBCode(StbActionTypes.CONST_SET_REQUEST_STB_CODE ,InputStb));     
          if (loading== true ) {
            setTimeout(() => {
              console.log(stb.results.devices[0].namadevices);
              navigation.replace('Song')
              SocketWeb(stb.results.devices[0].namadevices,'Ready');
            }, 50);  
          }
          return  loading == false;
          

      }
      const ClickLoginScann= () =>{

        navigation.replace('Song')

    }
      


    return(
    // <Provider store={configureStore({})}>
    <View style={{height:'100%', width:'100%', alignItems:'center',  backgroundColor:'black'}}>
      <View style={{height:'40%',width:'90%', justifyContent:'center', alignItems:'center' }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text>
      </View>
      <View style={{height:150, width:'90%', justifyContent:'center', alignItems:'center' }}>
              <View style={{height:52, width:'60%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20, textAlign:'center'}} placeholder={'Code STB'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Insert code from STB</Text>
            </View>
            
        
       

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLoginSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Login STB</Text>
      </TouchableOpacity>

      <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>ClickLoginScann()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity>
    </View> 
    //  </Provider>
  );
    
};



export default HomeRemote;
