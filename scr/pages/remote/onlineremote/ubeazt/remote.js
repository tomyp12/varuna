import React, { Component, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';
import { View, TextInput, Text, Image, useWindowDimensions, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import ListSong from '../../../../component/listsong'
import SocketWeb from '../../../../lib/websocket'

const Remote = ()=>{
    const windowHeight = useWindowDimensions().height;
    const [selectListSong, setselectListSong]=useState(0);
    const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const { stb, loadingstb, variablestb } = useSelector((state) => ({
        stb: state.Stb.stb,
        loading: state.Stb.loading,
        variable: state.Stb.variable,
      }));

      useEffect(() => {

      }, []);

      const Songlist=()=>{

        navigation.replace('Song')
      
      }
      const Playlist=()=>{

        navigation.replace('Playlist')
      
      }

      const Perintah=(value)=>{
        switch (value) {
          case 'PLAY':
              console.log('PLAY');
              SocketWeb(stb.results.devices[0].namadevices,'PLAY');
            break;
          case 'STOP':
              console.log('STOP');
              SocketWeb(stb.results.devices[0].namadevices,'STOP');
            break;
          case 'PAUSE':
              console.log('PAUSE');
              SocketWeb(stb.results.devices[0].namadevices,'PAUSE');
              break;
          case 'NEXT':
                console.log('NEXT');
                SocketWeb(stb.results.devices[0].namadevices,'NEXT');
              break;
          case 'BACK':
              console.log('BACK');
              SocketWeb(stb.results.devices[0].namadevices,'BACK');
              break;
          
          default:
            break;
        } 
      }

  
    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    <View style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}>
                    <View style={{height:20, }}></View>
                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'90%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.results.devices[0].nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.results.devices[0].jam}</Text>
                                        </View>
                                    </View>
                                </View>
                    </View>

                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'80%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TouchableOpacity style={{height:'100%', width:'85%', color:'white',  padding:10}} onPress={()=>Songlist()}></TouchableOpacity>
                                      <TouchableOpacity onPress={()=>Songlist()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
               
                                </View>
                    </View>



                    <View style={{height:565,width:'90%', alignItems:'center', justifyContent:'space-around',}}>

                    <View style={{height:250,width:'100%', alignItems:'center',justifyContent:'space-between',  flexDirection:'row',}}>

                        <View style={{height:'100%',width:'30%', alignItems:'center',}}>
                                <View style={{height:40, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:14, fontWeight:'bold', marginTop:5}}>Music Vol.</Text>
                                </View>
                                
                        </View>

                        <View style={{height:'100%',width:'35%', alignItems:'center',justifyContent:'space-evenly', }}>
                            
                                <TouchableOpacity style={{height:150, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PLAY')}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Vocal</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{height:50, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PLAY')}>
                                <Text style={{fontSize:14, fontWeight:'bold', marginTop:5}}>Mute</Text>
                                </TouchableOpacity>
                        </View>
                        <View style={{height:'100%',width:'30%', alignItems:'center',justifyContent:'space-between', }}>
                        <View style={{height:40, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:14, fontWeight:'bold', marginTop:5}}>Mic Vol.</Text>
                                </View>
                            
                        </View>
                           
                 </View>



                    <View style={{height:60,width:'100%', alignItems:'center',justifyContent:'space-between',  flexDirection:'row',}}>
                         
                    <View style={{height:40, width:'25%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:12, fontWeight:'bold', marginTop:5}}>Tempo</Text>
                                </View>
                 </View>

                 <View style={{height:60,width:'100%', alignItems:'center',justifyContent:'space-between',  flexDirection:'row',}}>
                         
                    <View style={{height:40, width:'25%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:12, fontWeight:'bold', marginTop:5}}>Key</Text>
                                </View>
                 </View>

                    <View style={{height:40,width:'100%', alignItems:'center',justifyContent:'center',}}>
                         
                         <Text style={{fontSize:14, fontWeight:'bold', }}>Slider</Text>  
                 </View>

                    <View style={{height:40,width:'100%', alignItems:'center',justifyContent:'center', }}>
                         
                            <Text style={{fontSize:14, fontWeight:'bold', }}>Jamrud - Katulistiwa</Text>  
                    </View>


                      <View style={{height:80,width:'100%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', }}>
                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PLAY')}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Play</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('STOP')}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Stop</Text>                      
                                </TouchableOpacity>

                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PAUSE')}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Pause</Text>
                                
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('NEXT')}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Next</Text>
                                
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('BACK')}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Back</Text>
                                
                                </TouchableOpacity>
                              
                    </View>

                   
                    </View>

                    <View style={{height:80,width:'80%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', bottom:10, position:'absolute'}}>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Songlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/songlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Songlist</Text>
                                </TouchableOpacity>
                                <View style={{height:69, width:'33.3%',  backgroundColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/remote.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Remote</Text>                      
                                </View>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Playlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/playlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Playlist</Text>
                                </TouchableOpacity>
                              
                    </View>
                    

                    </View>

                    

      {/* <View style={{height:'40%',width:'90%', justifyContent:'center', alignItems:'center' }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text>
      </View>
      <View style={{height:150, width:'90%', justifyContent:'center', alignItems:'center' }}>
              <View style={{height:52, width:'60%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20, textAlign:'center'}} placeholder={'Code STB'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Insert code from STB</Text>
            </View>
            
        
       

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLoginSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Login STB</Text>
      </TouchableOpacity>

      <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>ClickLoginScann()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity> */}
    </View> 
    //  </Provider>
  );
    
};



export default Remote;
