import React, { Component, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';
import { View, TextInput, Text, Image, useWindowDimensions, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import ListSong from '../../../../component/listsong'
import Remote from './remote';
const Song = ()=>{
    const windowHeight = useWindowDimensions().height;
    const [selectListSong, setselectListSong]=useState(0);
    const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
    const navigation = useNavigation(); 
    const dispatch = useDispatch();

    const [Song,setSong]=useState({
      cari:"TITLE",
      mode:"SONGLIST",
      category:"ALL",
      keyword:"test"

    })

    const carilagu=()=>{

      dispatch(Action_SetListSonglist(SonglistActionTypes.CONST_SET_GET_SONGLIST,Song));   
    }

  const { songlist, loading, variable } = useSelector((state) => ({
    songlist: state.Songlist.songlist.results,
    loading: state.Songlist.loading,
    variable: state.Songlist.variable,
}));

const { stb, loadingstb, variablestb } = useSelector((state) => ({
  stb: state.Stb.stb,
  loading: state.Stb.loading,
  variable: state.Stb.variable,
}));
  
  useEffect(() => {
    console.log('res');
    dispatch(Action_SetListSonglist(SonglistActionTypes.CONST_SET_GET_SONGLIST,Song));   
    // console.log(songlist);
             if (loading== true ) {
            setTimeout(() => {
              console.log('res');
              console.log(songlist);
          }, 50);  
        }
  }, []);

  const handleSelection = (id, value) => {
    var selectedId = selectListSong
     
    if(selectedId === id)
    setselectListSong({selectedItem: null})
    else 
    setselectListSong({selectedItem: id})
    setIdLaguSongList(value);
    
 }

 const select_cari=(value)=>{

    setSong({...Song, cari:value})
    dispatch(Action_SetListSonglist(SonglistActionTypes.CONST_SET_GET_SONGLIST,Song));   

 }

 
 const Remote=()=>{

  navigation.replace('Remote')

}
const Playlist=()=>{

  navigation.replace('Playlist')

}


 const select_songmode=(value)=>{

  setSong({...Song, mode:value})
  dispatch(Action_SetListSonglist(SonglistActionTypes.CONST_SET_GET_SONGLIST,Song));   

}

const SONGLIST_HITS_NEW=()=>{

  if (Song.mode=='SONGLIST') {
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#773695', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )
    
  }if (Song.mode=='HITS'){
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#D44699', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )

  }else{

    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#F7961D', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )
  }

}

 const TITLE_ARTIST=()=>{


  if (Song.cari == 'TITLE') {
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'#e93c93', 
                                  borderColor:'#e93c93', borderWidth:1, justifyContent:'center', alignItems:'center'}} onPress={()=> select_cari('TITLE') }>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>TITLE</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'black', 
                                  borderColor:'#e93c93', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=>  select_cari('SINGER')  }>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>ARTIST</Text>
                                  </TouchableOpacity>
                      </View>
    )
  }else{
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'black', borderColor:'#e93c93', 
                                  borderWidth:1, justifyContent:'center', alignItems:'center'}}  onPress={()=> select_cari('TITLE')}>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>TITLE</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'#e93c93', borderColor:'#e93c93',
                                   borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_cari('SINGER')  }>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>ARTIST</Text>
                                  </TouchableOpacity>
                      </View>
    )
  }

  
 }

    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    <View style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}>
                    <View style={{height:20, }}></View>
                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'90%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.results.devices[0].nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.results.devices[0].jam}</Text>
                                        </View>
                                    </View>
                                </View>
                    </View>

                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'80%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TextInput style={{height:'100%', width:'85%', color:'white', textAlign:'auto', padding:10}} onChangeText={newText => setSong({...Song, keyword:newText})}></TextInput>
                                      <TouchableOpacity onPress={()=>carilagu()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
               
                                </View>
                    </View>

{TITLE_ARTIST()}

{SONGLIST_HITS_NEW()}



                    <View style={{height:460,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                    <FlatList 
                                                    data={songlist}
                                                    numColumns={0}
                                                    keyExtractor={(item, index) => String(index)}
                                                    // onEndReached={HandleOnEndReached}
                                                    onEndReachedThreshold={0.5}
                                                    showsVerticalScrollIndicator={false}
                                                    extraData={selectListSong}
                                                    // onScrollBeginDrag={()=>{
                                                    //   StopLoadingTambah=false;
                                                    // }}
                                                    
                                                    renderItem={({ item, index }) => {
                                                    return(
                                                      <ListSong 
                                                      OnSelect={()=> handleSelection(index, item.idlagu)}
                                                      OnSelectedItem={selectListSong }
                                                      onIndex={index}
                                                      title={item.judul} singer={item.artis}
                                                      OnSave={()=> console.log(item.idlagu)}
                                                      OnPlay={()=>console.log(item.idlagu)}
                                                      ></ListSong>
                 
                                                    )}}
                                                    />
                    </View>

                    <View style={{height:80,width:'80%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', bottom:10, position:'absolute'}}>
                                <View style={{height:69, width:'33.3%',  backgroundColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} >
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/songlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Songlist</Text>
                                </View>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Remote()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/remote.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Remote</Text>                      
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Playlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/playlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Playlist</Text>
                                </TouchableOpacity>
                              
                    </View>
                    

                    </View>

                    

      {/* <View style={{height:'40%',width:'90%', justifyContent:'center', alignItems:'center' }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text>
      </View>
      <View style={{height:150, width:'90%', justifyContent:'center', alignItems:'center' }}>
              <View style={{height:52, width:'60%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20, textAlign:'center'}} placeholder={'Code STB'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Insert code from STB</Text>
            </View>
            
        
       

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLoginSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Login STB</Text>
      </TouchableOpacity>

      <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>ClickLoginScann()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity> */}
    </View> 
    //  </Provider>
  );
    
};



export default Song;
