import React, { Component, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';
import { View, TextInput, Text, Image, useWindowDimensions, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import PlaylistSong from '../../../../component/playlistsong'
const Playlist = ()=>{
    const windowHeight = useWindowDimensions().height;
    const [selectListSong, setselectListSong]=useState(0);
    const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
  const command = useSelector((state) => {
      return state.Command.command;
  });
    
  const stb = useSelector((state) => {
    return state.Stb.stb;
  });

      const [Respon,SetRespon]=useState({
        "command":"command", 
        "idlagu":0, 
        "judul":"",
        "artis":"",
        "pause":0,
        "songduration":0,
        "currentposition":0,
        "vocalon":1/0,
        "key":0,
        "tempo":0,
        "playlist":[{
          "idlagu":0,
          "path":"",
          "voc":0,
          "xvoc":0,
          "judul":"",
          "artis":"",
          "volume":0
	        }]

      })

      // const SendSocket = (ValueAddress, CommandSend)=>{
      //   var ws = new WebSocket('http://8.215.47.1:6969/'+ValueAddress);
      //   if(ws){
      //     ws.onerror = ws.onopen = ws.onclose =null;
      //     ws.close();
      //   }
        
      //   ws.onopen = () => {
      //   // connection opened
      //   ws.send(CommandSend);  // send a message
      // };
      //   ws.onmessage=({data})=> 
      //   SetRespon({
      //     "command":data.command, 
      //     "idlagu":data.idlagu, 
      //     "judul":data.judul,
      //     "artis":data.artis,
      //     "pause":data.pause,
      //     "songduration":data.songduration,
      //     "currentposition":data.currentposition,
      //     "vocalon":data.vocalon,
      //     "key":data.key,
      //     "tempo":data.tempo,
      //     "volume":data.volume,
      //     "mic":data.mic,
      //     "playlist":[data.playlist]
      //   })
     
      //   ws.close()
      //  }

      useEffect(() => {

      }, []);

      const Songlist=()=>{

        navigation.replace('Song')
      
      }
      const Remote=()=>{

        navigation.replace('Remote')
      
      }

      const Perintah=(value)=>{
        switch (value) {
          case 'PLAY':
                console.log('PLAY');
            break;
          case 'STOP':
              console.log('STOP');
            break;
          case 'PAUSE':
              console.log('PAUSE');
              break;
          case 'NEXT':
                console.log('NEXT');
              break;
          case 'BACK':
              console.log('BACK');
              break;
          
          default:
            break;
        } 
      }

  
    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    <View style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}>
                    <View style={{height:20, }}></View>
                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'90%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.jam}</Text>
                                        </View>
                                    </View>
                                </View>
                    </View>

                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'80%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TouchableOpacity style={{height:'100%', width:'85%', color:'white',  padding:10}} onPress={()=>Songlist()}></TouchableOpacity>
                                      <TouchableOpacity onPress={()=>Songlist()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
               
                                </View>
                    </View>



                    <View style={{height:565,width:'90%', alignItems:'center', justifyContent:'space-around',}}>

                    <FlatList 
                                                    data={Respon.playlist}
                                                    numColumns={0}
                                                    keyExtractor={(item, index) => String(index)}
                                                    // onEndReached={HandleOnEndReached}
                                                    onEndReachedThreshold={0.5}
                                                    showsVerticalScrollIndicator={false}
                                                    extraData={selectListSong}
                                                    // onScrollBeginDrag={()=>{
                                                    //   StopLoadingTambah=false;
                                                    // }}
                                                    
                                                    renderItem={({ item, index }) => {
                                                    return(
                                                      <PlaylistSong 
                                                      OnSelect={()=> handleSelection(index, item.idlagu)}
                                                      OnSelectedItem={selectListSong }
                                                      onIndex={index}
                                                      title={item.judul} singer={item.artis}
                                                      OnSave={()=> OnSave(item.idlagu,item.judul, item.artis, item.path, item.volume, item.xvoc, item.voc)}
                                                      OnPlay={()=>OnPlay(item.idlagu,item.judul, item.artis, item.path, item.volume, item.xvoc, item.voc)}
                                                      ></PlaylistSong>
                 
                                                    )}}
                                                    />

                   
                    </View>

                    <View style={{height:80,width:'80%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', bottom:10, position:'absolute'}}>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Songlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/songlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Songlist</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Remote()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/remote.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Remote</Text>                      
                                </TouchableOpacity>
                                <View style={{height:69, width:'33.3%',  backgroundColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/playlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Playlist</Text>
                                </View>
                              
                    </View>
                    

                    </View>

                    


    </View> 

  );
    
};



export default Playlist;
