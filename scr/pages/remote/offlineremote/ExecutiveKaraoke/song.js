import React, { Component, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';
import { View, TextInput, Text, Image, useWindowDimensions, FlatList, ImageBackground } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import ListSong from '../../../../component/listsongexecutive'
import PlaylistSong from '../../../../component/playlistsongexecutive'
import { Action_Command } from '../../../../redux/command/actions';
import { StbActionTypes } from '../../../../redux/stb/constants';
import { CommandActionTypes } from '../../../../redux/command/constants';
import { WebSockets } from '../../../../lib/websocket'
import ButtonEff from '../../../../component/buttonEff';
import Moment from 'moment';

import Remote from './remote';
import Slider from '@react-native-community/slider';
const Song = ()=>{

  const stb = useSelector((state) => {
    return state.Stb.stb;
  });
      const windowHeight = useWindowDimensions().height;
      const [selectListSong, setselectListSong]=useState(0);
      const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
      const navigation = useNavigation(); 
      const dispatch = useDispatch();
      const [OPEN,SetOPEN]=useState(0);
      const [CARI,SetCARI]=useState(1);
      const [tmr,settmr]=useState(0);
      const [InputStb,setInputStb]=useState({
        
        "code":"XXXX",
        "iduser": 99,
        "durasi": 4,
        "addtime": 1
        
    })
   
      const [Song,setSong]=useState({
        cari:"TITLE",
        mode:"SONGLIST",
        category:"ALL",
        keyword:""
  
      })
      
  
      const OnSave =(idlagu,judul, artis, path, vol, xvoc, voc)=>{
        let y={
          "meta":"send_message",
          "message":{
            "command":"PLT",
            "idlagu":idlagu,
            "path":path,
            "voc":voc,
            "xvoc":xvoc,
            "judul":judul,
            "artis":artis,
            "volume":vol,
            "playmode":2
          }, 
          "roomID":stb.namadevices, 
          "clientID":InputStb.iduser
        }
        WebSockets(y);
   
      }
      
      useEffect(() => {

      
        if (OPEN==0) {
          OnOpen()
          SetOPEN(1)
        }
        if (CARI==1) {
          dispatch(Action_SetListSonglist(SonglistActionTypes.CONST_SET_GET_SONGLIST,Song)); 
          SetCARI(0)
        }
        const interval = setTimeout(() => {
        
          
          let m={
              "meta":"send_message",
              "message":{"command":"kosong"}, 
              "roomID":stb.namadevices, 
              "clientID":99
            }
            WebSockets(m)
            settmr(tmr+1)
            console.log(global.currentposition);
          
            
        }, 2000);
      
        return () => clearTimeout(interval);
    
          }, [tmr,CARI]);
  
          const OnPlayPlaylist=(idlagu,judul, artis, path, vol, xvoc, voc)=>{
            let y={
              "meta":"send_message",
              "message":{
                "command":"PLY",
                "idlagu":idlagu,
                "path":path,
                "voc":voc,
                "xvoc":xvoc,
                "judul":judul,
                "artis":artis,
                "volume":vol,
                "playmode":2
              }, 
              "roomID":stb.namadevices, 
              "clientID":InputStb.iduser
            }
        
            WebSockets(y);
        
          }
      const OnPlay=(idlagu,judul, artis, path, vol, xvoc, voc)=>{
        let y={
          "meta":"send_message",
          "message":{
            "command":"PLY",
            "idlagu":idlagu,
            "path":path,
            "voc":voc,
            "xvoc":xvoc,
            "judul":judul,
            "artis":artis,
            "volume":vol,
            "playmode":1
          }, 
          "roomID":stb.namadevices, 
          "clientID":InputStb.iduser
        }
  
        WebSockets(y);
  
      }
      const carilagu=()=>{
        SetCARI(1)  
      }
  
    const { songlist, loading, variable } = useSelector((state) => ({
      songlist: state.Songlist.songlist.results,
      loading: state.Songlist.loading,
      variable: state.Songlist.variable,
  }));
  
  const OnOpen=()=>{
          let x = {"command":"GO"}
  
          let m={
            "meta":"send_message",
            "message":x, 
            "roomID":stb.namadevices, 
            "clientID":InputStb.iduser
          }
         
          WebSockets(m);
  
        }
  
   
  
    const handleSelection = (id, value) => {
      var selectedId = selectListSong
       
      if(selectedId === id)
      setselectListSong({selectedItem: null})
      else 
      setselectListSong({selectedItem: id})
      setIdLaguSongList(value);
      
   }
  
   const select_cari=(value)=>{
  
      setSong({...Song, cari:value})
      SetCARI(1)
   }
  
   const Perintah=(name,value)=>{
    let x=null;
    switch (name) {
      case 'PLAY':
          console.log('PLAY');
          x={"command":"PL"}
        break;
      case 'STOP':
          console.log('STOP');
          x={"command":"STP"}
        break;
      case 'PAUSE':
          console.log('PAUSE');
          x={"command":"PS"}
          break;
      case 'NEXT':
            console.log('NEXT');
            x={"command":"NEXT"}
          break;
      case 'BACK':
          console.log('BACK');
          x={"command":"BACK"}
          break;
      case 'RELOAD':
          console.log('RELOAD');
          x={"command":"RE"}
          break;
      case 'VOL':
          console.log('VOL');
            let m =0;
            if (value==1) {
              
              m= global.volume+10
              if (m>100) {
                m=100
              }
            }        
            if (value==0) {
              m= global.volume-10
              if (m<0) {
                m=0
              }
            } 
          x={ "command":"VOMS",
                                "value":m}

          break;
      case 'MIC':
          x={"command":"VOMC",
          "value":value}
          break;
      case 'TEMP':
            let s=0;
            if (value==0) {
              s=global.tempo -1
              if (s<-3) {
                s=-3
              }
            }
            if (value==1) {
              s=global.tempo +1
              if (s>=3) {
                s=3
              }
            }
            
          x={"command":"TPM","tempo":m}
          break;
      case 'KEY':
        let j=0;
        if (value==0) {
          j=global.key -1
          if (j<-3) {
            j=-3
          }
        }
        if (value==1) {
          j=global.key +1
          if (j>=3) {
            j=3
          }
        }
          x={"command":"KYM","key":m}
          break;
      case 'TRACK':
          x={ "command":"TRB",
          "position":value}
          break;
      case 'REMOVED':
          x={"command":"RMV",
          "idlagu":value}
          break;
      case 'VOCAL':
          console.log('VOCAL');
          x={"command":"VC"}
          break;
      case 'MUTE':
          console.log('MUTE');
          x={"command":"MT"}
          break;
      case 'OUT':
            console.log('MUTE');
            x={"command":"STB"}
            navigation.push('HomeRemote')
            break;
      default:
        break;
    } 

    let m={
      "meta":"send_message",
      "message":x, 
      "roomID":stb.namadevices, 
      "clientID":99
    }
    WebSockets(m)
 
  }


   
   const Remote=()=>{
  
    navigation.push('Remote')
  
  }
  const Playlist=()=>{
  
    navigation.push('Playlist')
  
  }
  
  
   const select_songmode=(value)=>{
  
    setSong({...Song, mode:value})
    SetCARI(1)
  
  }

const SONGLIST_HITS_NEW=()=>{

  if (Song.mode=='SONGLIST') {
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#773695', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )
    
  }if (Song.mode=='HITS'){
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#D44699', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )

  }else{

    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#F7961D', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )
  }

}

 const TITLE_ARTIST=()=>{


  if (Song.cari == 'TITLE') {
    return(
      <View style={{height:44,width:'100%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                  <TouchableOpacity style={{height:40, width:'48%', backgroundColor:'#2d499c', 
                                  borderColor:'#2d499c', borderWidth:1, justifyContent:'center', alignItems:'center'}} onPress={()=> select_cari('TITLE') }>
                                          <Text style={{fontSize:14, fontWeight:'bold', color:'white'}}>TITLE</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{height:40, width:'48%', backgroundColor:'white', 
                                  borderColor:'#2d499c', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=>  select_cari('ARTIST')  }>
                                          <Text style={{fontSize:14, fontWeight:'bold', color:'black'}}>ARTIST</Text>
                                  </TouchableOpacity>
                      </View>
    )
  }else{
    return(
      <View style={{height:44,width:'100%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                  <TouchableOpacity style={{height:40, width:'48%', backgroundColor:'white', borderColor:'#2d499c', 
                                  borderWidth:1, justifyContent:'center', alignItems:'center'}}  onPress={()=> select_cari('TITLE')}>
                                          <Text style={{fontSize:14, fontWeight:'bold', color:'black'}}>TITLE</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{height:40, width:'48%', backgroundColor:'#2d499c', borderColor:'#2d499c',
                                   borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_cari('ARTIST')  }>
                                          <Text style={{fontSize:14, fontWeight:'bold', color:'white'}}>ARTIST</Text>
                                  </TouchableOpacity>
                      </View>
    )
  }

  
 }

    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    {/* < style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}> */}
                    <ImageBackground source={require('../../../../assets/executive/backgroud.png')} resizeMode={'contain'} style={{height:'98%', width:'100%' }}>
                    <View style={{height:15, }}></View>
                    <View style={{height:40,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:30, width:'93%', backgroundColor:'black', 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{Moment(stb.jam).format('DD  MMM yyyy hh:ss')}</Text>
                                        </View>
                                    </View>
                                </View>

                                
                    </View>

                    <TouchableOpacity onPress={()=>Perintah('OUT',0)} style={{height:30, width:30, backgroundColor:'black', borderRadius:50, right:40, top:21, position:'absolute'}}>
                    <Image style={{width:'100%', height:'100%', }} resizeMode='center' source={require('../../../../assets/icon/logout.png')} ></Image>
                    </TouchableOpacity>

                    {/* {SONGLIST_HITS_NEW()} */}

                    <View style={{left:35, top:9, height:42,width:782, alignItems:'center', flexDirection:'row',}}>

                    
                    <ButtonEff STYLES={{height:'100%', width:315,}} ONPRESS={()=>select_songmode('SONGLIST')} ></ButtonEff> 
                    <ButtonEff STYLES={{height:'100%', width:157,}} ONPRESS={()=>select_songmode('HIT')} ONTEXT={'HIT'}></ButtonEff> 
                    <ButtonEff STYLES={{height:'100%', width:157,}} ONPRESS={()=>select_songmode('NEW')} ONTEXT={'NEW'}></ButtonEff> 
                    <ButtonEff STYLES={{height:'100%', width:154,}} ONPRESS={()=>select_songmode('POPULAR')} ONTEXT={'POPULAR'}></ButtonEff> 

                    </View>
                    
                    <View style={{left:35, top:19,height:44,width:782,  alignItems:'center',flexDirection:'row',}}>
                      <View style={{height:'100%', width:180,  }}>


                        {TITLE_ARTIST()}
                      </View>
                                <View style={{left:3, height:40, width:554, backgroundColor:'black', borderRadius:5,
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TextInput style={{height:'100%', width:'85%', color:'white', textAlign:'auto', padding:10}} value={Song.keyword} onSubmitEditing={()=>carilagu()} onChangeText={newText => setSong({...Song, keyword:newText})}></TextInput>
                                     
                                      <TouchableOpacity onPress={()=>carilagu()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
                                     
                                      <ButtonEff STYLES={{height:'90%',  width:'7%', left:4}} ONPRESS={() => setSong({...Song, keyword:''})}></ButtonEff> 
                                </View>
                    </View>


                    <View style={{left:45, top:45,height:435,width:780, flexDirection:'row', justifyContent:'space-between',}}>
                    <FlatList 
                                                    data={songlist}
                                                    numColumns={0}
                                                    keyExtractor={(item, index) => String(index)}
                                                    onEndReachedThreshold={0.5}
                                                    showsVerticalScrollIndicator={false}
                                                    extraData={selectListSong}
                                                    renderItem={({ item, index }) => {
                                                    return(
                                                      <ListSong 
                                                      OnSelect={()=> handleSelection(index, item.idlagu)}
                                                      OnSelectedItem={selectListSong }
                                                      onIndex={index}
                                                      title={item.judul} singer={item.artis}
                                                      OnSave={()=> OnSave(item.idlagu,item.judul, item.artis, item.path, item.vol, item.xvoc, item.voc)}
                                                      OnPlay={()=>OnPlay(item.idlagu,item.judul, item.artis, item.path, item.vol, item.xvoc, item.voc)}
                                                      ></ListSong>
                 
                                                    )}}
                                                    />
                    </View>

                    <View style={{left:36, top:66,height:26,width:780, flexDirection:'row', }}>
                    <Slider
                          style={{width: '100%', height: '100%',  transform:[{rotate:'0deg'}]}}
                          minimumValue={0}
                          maximumValue={global.songduration}
                          value={global.currentposition}
                          onSlidingComplete={(value)=>Perintah('TRACK',value)}
                          minimumTrackTintColor="#2d499c"
                          maximumTrackTintColor="#2d499c"
                        />
                    </View>
                    <View style={{right:126, top:230,height:490,width:320,  flexDirection:'row', justifyContent:'space-between',position:'absolute'}}>
                    <FlatList 
                                                    data={global.playlist}
                                                    numColumns={0}
                                                    keyExtractor={(item, index) => String(index)}
                                                    onEndReachedThreshold={0.5}
                                                    showsVerticalScrollIndicator={false}
                                                    extraData={selectListSong}
          
                                                    renderItem={({ item, index }) => {
                                                    return(
                                                      <PlaylistSong 
                                                      OnSelect={()=> handleSelection(index, item.idlagu)}
                                                      OnSelectedItem={selectListSong }
                                                      onIndex={index}
                                                      title={item.judul} singer={item.artis}
                                                      // OnSave={()=> OnSave(item.idlagu,item.judul, item.artis, item.path, item.vol, item.xvoc, item.voc)}
                                                      OnPlay={()=>OnPlayPlaylist(item.idlagu,item.judul, item.artis, item.path, item.vol, item.xvoc, item.voc)}
                                                      ></PlaylistSong>
                 
                                                    )}}
                                                    />
                    </View>

                    <View style={{right:35, top:68,height:658,width:86, alignItems:'center',
                     flexDirection:'column', position:'absolute', alignItems:'center'}}>
   
                       <ButtonEff STYLES={{height:50, width:80, top:2, borderRadius:4}} ONPRESS={()=>Perintah('TEMP',1)}></ButtonEff> 
                       <ButtonEff STYLES={{height:50, width:80, top:97,  borderRadius:4}} ONPRESS={()=>Perintah('TEMP',0)}></ButtonEff> 
                       <ButtonEff STYLES={{height:50, width:80, top:131,  borderRadius:4}} ONPRESS={()=>Perintah('KEY',1)}></ButtonEff> 
                       <ButtonEff STYLES={{height:50, width:80, top:227,  borderRadius:4}} ONPRESS={()=>Perintah('KEY',0)}></ButtonEff> 
                       <ButtonEff STYLES={{height:50, width:80, top:259,  borderRadius:4}} ONPRESS={()=>Perintah('VOL',1)}></ButtonEff> 
                       <ButtonEff STYLES={{height:50, width:80, top:355,  borderRadius:4}} ONPRESS={()=>Perintah('VOL',0)}></ButtonEff> 
                   
                    </View>
                 

          <View style={{left:35, top:76, height:49,width:782, alignItems:'center', 
          flexDirection:'row', }}>

          <ButtonEff STYLES={{height:'100%', width:73,}} ONPRESS={()=>Perintah('RELOAD',0)}></ButtonEff>  
          <ButtonEff STYLES={{height:'100%', width:96,}} ONPRESS={()=>Perintah('BACK',0)}></ButtonEff>     
          <ButtonEff STYLES={{height:'100%', width:96,}} ONPRESS={()=>Perintah('PAUSE',0)}></ButtonEff> 
          <ButtonEff STYLES={{height:'100%', width:262,}} ONPRESS={()=>Perintah('PLAY',0)}></ButtonEff> 
          <ButtonEff STYLES={{height:'100%', width:96,}} ONPRESS={()=>Perintah('STOP',0)}></ButtonEff> 
          <ButtonEff STYLES={{height:'100%', width:96,}} ONPRESS={()=>Perintah('NEXT',0)}></ButtonEff> 
          <ButtonEff STYLES={{height:'100%', width:64,}} ONPRESS={()=>Perintah('VOCAL',0)}></ButtonEff> 

              </View>
                    </ImageBackground>
    </View> 
  );
    
};



export default Song;
