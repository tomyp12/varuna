import React, { Component, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';
import { View, TextInput, Text, Image, useWindowDimensions, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import PlaylistSong from '../../../../component/playlistsong'
import { WebSockets } from '../../../../lib/websocket';
const Playlist = ()=>{
    const windowHeight = useWindowDimensions().height;
    const [selectListSong, setselectListSong]=useState(0);
    const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [tmri,settmri]=useState(0);
  const command = useSelector((state) => {
      return state.Command.command;
  });
    
  const stb = useSelector((state) => {
    return state.Stb.stb;
  });

    
  const [InputStb,setInputStb]=useState({
    "code":"XXXX",
    "iduser": 99,
    "durasi": 4,
    "addtime": 1
  })
  

  const OnPlayPlaylist=(idlagu,judul, artis, path, vol, xvoc, voc)=>{
    let y={
      "meta":"send_message",
      "message":{
        "command":"PLY",
        "idlagu":idlagu,
        "path":path,
        "voc":voc,
        "xvoc":xvoc,
        "judul":judul,
        "artis":artis,
        "volume":vol,
        "playmode":2
      }, 
      "roomID":stb.namadevices, 
      "clientID":InputStb.iduser
    }

    WebSockets(y);

  }
      useEffect(() => {
        const intervali = setTimeout(() => {
              let m={
              "meta":"send_message",
              "message":{"command":"kosong"}, 
              "roomID":stb.namadevices, 
              "clientID":99
            }
            WebSockets(m)
            settmri(tmri+1)
        }, 5000);
      
        return () => clearTimeout(intervali);
      }, [tmri]);

      const Songlist=()=>{

        navigation.replace('Song')
      
      }
      const Remote=()=>{

        navigation.replace('Remote')
      
      }

      const Perintah=(value)=>{
        switch (value) {
          case 'PLAY':
                console.log('PLAY');
            break;
          case 'STOP':
              console.log('STOP');
            break;
          case 'PAUSE':
              console.log('PAUSE');
              break;
          case 'NEXT':
                console.log('NEXT');
              break;
          case 'BACK':
              console.log('BACK');
              break;
          
          default:
            break;
        } 
      }
      function shuffleArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          const temp = array[i];
          array[i] = array[j];
          array[j] = temp;
        }
        return array;
      }

      const GantiUrutan=()=>{
        shuffleArray(global.playlist)

        let m={
          "meta":"send_message",
          "message":{"command":"PRT", "playlist":global.playlist}, 
          "roomID":stb.namadevices, 
          "clientID":99
        }
        WebSockets(m)
      }
      const handleSelection = (id, value) => {
        var selectedId = selectListSong
         
        if(selectedId === id)
        setselectListSong({selectedItem: null})
        else 
        setselectListSong({selectedItem: id})
        setIdLaguSongList(value);
        
     }
    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    <View style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}>
                    <View style={{height:20, }}></View>
                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'90%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.jam}</Text>
                                        </View>
                                    </View>
                                </View>
                    </View>

                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'80%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TouchableOpacity style={{height:'100%', width:'85%', color:'white',  padding:10}} onPress={()=>Songlist()}></TouchableOpacity>
                                      <TouchableOpacity onPress={()=>Songlist()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
               
                                </View>
                    </View>



                    <View style={{height:565,width:'90%', alignItems:'center', justifyContent:'space-around',}}>

                    <FlatList 
                                                    data={global.playlist}
                                                    numColumns={0}
                                                    keyExtractor={(item, index) => String(index)}
                                                    // onEndReached={HandleOnEndReached}
                                                    onEndReachedThreshold={0.5}
                                                    showsVerticalScrollIndicator={false}
                                                    extraData={selectListSong}
                                                    // onScrollBeginDrag={()=>{
                                                    //   StopLoadingTambah=false;
                                                    // }}
                                                    
                                                    renderItem={({ item, index }) => {
                                                    return(
                                                      <PlaylistSong 
                                                      OnSelect={()=> handleSelection(index, item.idlagu)}
                                                      OnSelectedItem={selectListSong }
                                                      onIndex={index}
                                                      title={item.judul} singer={item.artis}
                                                      // OnSave={()=> OnSave(item.idlagu,item.judul, item.artis, item.path, item.volume, item.xvoc, item.voc)}
                                                      OnPlay={()=>OnPlayPlaylist(item.idlagu,item.judul, item.artis, item.path, item.volume, item.xvoc, item.voc)}
                                                      ></PlaylistSong>
                 
                                                    )}}
                                                    />

                   
                    </View>

                    <TouchableOpacity style={{width:50, height:50, borderRadius:50, backgroundColor:'green', right:20, bottom:100, position:'absolute', justifyContent:'center', alignItems:'center'}} onPress={()=> GantiUrutan()}>
                      <Text style={{fontSize:10}}>Shuffle</Text>
                    </TouchableOpacity>

                    <View style={{height:80,width:'80%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', bottom:10, position:'absolute'}}>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Songlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/songlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Songlist</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Remote()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/remote.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Remote</Text>                      
                                </TouchableOpacity>
                                <View style={{height:69, width:'33.3%',  backgroundColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/playlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Playlist</Text>
                                </View>
                              
                    </View>
                    

                    </View>

                    


    </View> 

  );
    
};



export default Playlist;
