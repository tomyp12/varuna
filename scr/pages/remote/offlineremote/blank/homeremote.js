import React, { Component, useState, useCallback, useEffect  } from 'react';
import { SafeAreaView, TouchableOpacity, ToastAndroid } from 'react-native';
import { useDispatch, useSelector,shallowEqual } from 'react-redux';
import { Action_Fake, Action_SetSTBCode } from '../../../../redux/stb/actions';
import { Action_Command } from '../../../../redux/command/actions';
import { StbActionTypes } from '../../../../redux/stb/constants';
import { CommandActionTypes } from '../../../../redux/command/constants';
import { View, TextInput, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { WebSockets } from '../../../../lib/websocket'
import Stb from '../../../../redux/stb/reducers';

const HomeRemote = ()=>{
    const navigation = useNavigation(); 
    const dispatch = useDispatch(); 
    const [InputStb,setInputStb]=useState({
      "code":"XXXX",
      "iduser": 99,
      "durasi": 4,
      "addtime": 1
      
  })
    const [LoginSTB,SetLoginSTB]=useState(0);
    const [SendSTB,SetSendSTB]=useState(0);

  const showToastWithGravity = (input) => {
    ToastAndroid.showWithGravity(
      input,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
      ToastAndroid.LONG
    );
  }; 

const command = useSelector((state) => {
    return state.Command.command;
});
  
const stb = useSelector((state) => {
  return state.Stb.stb;
});



useEffect(() => {

  if (SendSTB==1) {
    SetSendSTB(0);
      navigation.push('Song')
  }
  if (LoginSTB==1) {

      try {
        let join={
          "meta":"join_room",
          "message":{"command":"join"}, 
          "roomID":stb.namadevices, 
          "clientID":99
        }
  
        WebSockets(join)
        SetLoginSTB(0);
        setTimeout(() => {

          SetSendSTB(1);
        }, 500);
        
      }catch(error){
        
      }
    
  }
  
}, [stb,command, SendSTB])

const GetResponSTB=()=>{
  console.log(stb);
}


        const ClickLoginSTB= ()=>{

          dispatch(Action_SetSTBCode(StbActionTypes.CONST_SET_REQUEST_STB_CODE ,InputStb))
          SetLoginSTB(1);
      
      }
 
    return(
    <View style={{height:'100%', width:'100%', alignItems:'center',  backgroundColor:'black'}}>
      <View style={{height:'40%',width:'90%', justifyContent:'center', alignItems:'center' }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text>
      </View>
      <View style={{height:150, width:'90%', justifyContent:'center', alignItems:'center' }}>
              <View style={{height:52, width:'60%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20, textAlign:'center'}} placeholder={'Code STB'}
                  onChangeText={newText => setInputStb({...InputStb, code:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Insert code from STB</Text>
            </View>
            
        
       

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLoginSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Login STB</Text>
      </TouchableOpacity>

      <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>GetResponSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity>
      {/* <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>ClickLoginSca()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity> */}
    </View> 
    //  </Provider>
  );
    
};



export default HomeRemote;
