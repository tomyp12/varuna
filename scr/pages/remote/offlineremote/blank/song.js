import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, Text, TextInput, TouchableOpacity, useWindowDimensions, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ListSong from '../../../../component/listsong';
import { WebSockets } from '../../../../lib/websocket';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';

const Song = ()=>{

const stb = useSelector((state) => {
  return state.Stb.stb;
});
    const windowHeight = useWindowDimensions().height;
    const [selectListSong, setselectListSong]=useState(0);
    const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [OPEN,SetOPEN]=useState(0);
    const [CARI,SetCARI]=useState(1);
    const [InputStb,setInputStb]=useState({
      "code":"XXXX",
      "iduser": 99,
      "durasi": 4,
      "addtime": 1
      
  })
 
    const [Song,setSong]=useState({
      cari:"TITLE",
      mode:"SONGLIST",
      category:"ALL",
      keyword:"test"

    })
    

    const OnSave =(idlagu,judul, artis, path, vol, xvoc, voc)=>{
      let y={
        "meta":"send_message",
        "message":{
          "command":"PLT",
          "idlagu":idlagu,
          "path":path,
          "voc":voc,
          "xvoc":xvoc,
          "judul":judul,
          "artis":artis,
          "volume":vol,
          "playmode":2
        }, 
        "roomID":stb.namadevices, 
        "clientID":InputStb.iduser
      }
      WebSockets(y);
 
    }
    

    const OnPlay=(idlagu,judul, artis, path, vol, xvoc, voc)=>{
      let y={
        "meta":"send_message",
        "message":{
          "command":"PLY",
          "idlagu":idlagu,
          "path":path,
          "voc":voc,
          "xvoc":xvoc,
          "judul":judul,
          "artis":artis,
          "volume":vol,
          "playmode":1
        }, 
        "roomID":stb.namadevices, 
        "clientID":InputStb.iduser
      }

      WebSockets(y);

    }
    const carilagu=()=>{
      SetCARI(1)  
    }

  const { songlist, loading, variable } = useSelector((state) => ({
    songlist: state.Songlist.songlist.results,
    loading: state.Songlist.loading,
    variable: state.Songlist.variable,
}));

const OnOpen=()=>{
        let x = {"command":"GO"}

        let m={
          "meta":"send_message",
          "message":x, 
          "roomID":stb.namadevices, 
          "clientID":InputStb.iduser
        }
       
        WebSockets(m);

      }

  useEffect(() => {
    if (OPEN==0) {
      OnOpen()
      SetOPEN(1)
    }
    if (CARI==1) {
      dispatch(Action_SetListSonglist(SonglistActionTypes.CONST_SET_GET_SONGLIST,Song)); 
      SetCARI(0)
    }
 
  }, [CARI]);

  const handleSelection = (id, value) => {
    var selectedId = selectListSong
     
    if(selectedId === id)
    setselectListSong({selectedItem: null})
    else 
    setselectListSong({selectedItem: id})
    setIdLaguSongList(value);
    
 }

 const select_cari=(value)=>{

    setSong({...Song, cari:value})
    SetCARI(1)
 }

 
 const Remote=()=>{

  navigation.push('Remote')

}
const Playlist=()=>{

  navigation.push('Playlist')

}


 const select_songmode=(value)=>{

  setSong({...Song, mode:value})
  SetCARI(1)

}

const SONGLIST_HITS_NEW=()=>{

  if (Song.mode=='SONGLIST') {
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#773695', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )
    
  }if (Song.mode=='HITS'){
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#D44699', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )

  }else{

    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', borderColor:'#773695', 
              borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('SONGLIST') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>SONGLIST</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'black', 
              borderColor:'#D44699', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('HITS') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>HITS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:44, width:'32%', backgroundColor:'#F7961D', 
              borderColor:'#F7961D', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_songmode('NEW') }>
                      <Text style={{fontSize:12, fontWeight:'normal'}}>NEW</Text>
              </TouchableOpacity>
      </View>
    )
  }

}

 const TITLE_ARTIST=()=>{


  if (Song.cari == 'TITLE') {
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'#e93c93', 
                                  borderColor:'#e93c93', borderWidth:1, justifyContent:'center', alignItems:'center'}} onPress={()=> select_cari('TITLE') }>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>TITLE</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'black', 
                                  borderColor:'#e93c93', borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=>  select_cari('SINGER')  }>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>ARTIST</Text>
                                  </TouchableOpacity>
                      </View>
    )
  }else{
    return(
      <View style={{height:55,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'black', borderColor:'#e93c93', 
                                  borderWidth:1, justifyContent:'center', alignItems:'center'}}  onPress={()=> select_cari('TITLE')}>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>TITLE</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{height:44, width:'48%', backgroundColor:'#e93c93', borderColor:'#e93c93',
                                   borderWidth:1,justifyContent:'center', alignItems:'center'}} onPress={()=> select_cari('SINGER')  }>
                                          <Text style={{fontSize:20, fontWeight:'bold'}}>ARTIST</Text>
                                  </TouchableOpacity>
                      </View>
    )
  }

  
 }

    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    <View style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}>
                    <View style={{height:20, }}></View>
                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'90%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.jam}</Text>
                                        </View>
                                    </View>
                                </View>
                    </View>

                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'80%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TextInput style={{height:'100%', width:'85%', color:'white', textAlign:'auto', padding:10}} onChangeText={newText => setSong({...Song, keyword:newText})}></TextInput>
                                      <TouchableOpacity onPress={()=>carilagu()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
               
                                </View>
                    </View>

{TITLE_ARTIST()}

{SONGLIST_HITS_NEW()}



                    <View style={{height:460,width:'90%', alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                    <FlatList 
                                                    data={songlist}
                                                    numColumns={0}
                                                    keyExtractor={(item, index) => String(index)}
                                                    // onEndReached={HandleOnEndReached}
                                                    onEndReachedThreshold={0.5}
                                                    showsVerticalScrollIndicator={false}
                                                    extraData={selectListSong}
                                                    // onScrollBeginDrag={()=>{
                                                    //   StopLoadingTambah=false;
                                                    // }}
                                                    
                                                    renderItem={({ item, index }) => {
                                                    return(
                                                      <ListSong 
                                                      OnSelect={()=> handleSelection(index, item.idlagu)}
                                                      OnSelectedItem={selectListSong }
                                                      onIndex={index}
                                                      title={item.judul} singer={item.artis}
                                                      OnSave={()=> OnSave(item.idlagu,item.judul, item.artis, item.path, item.vol, item.xvoc, item.voc)}
                                                      OnPlay={()=>OnPlay(item.idlagu,item.judul, item.artis, item.path, item.vol, item.xvoc, item.voc)}
                                                      ></ListSong>
                 
                                                    )}}
                                                    />
                    </View>

                    <View style={{height:80,width:'80%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', bottom:10, position:'absolute'}}>
                                <View style={{height:69, width:'33.3%',  backgroundColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} >
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/songlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Songlist</Text>
                                </View>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Remote()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/remote.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Remote</Text>                      
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Playlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/playlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Playlist</Text>
                                </TouchableOpacity>
                              
                    </View>
                    

                    </View>

                    

      {/* <View style={{height:'40%',width:'90%', justifyContent:'center', alignItems:'center' }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text>
      </View>
      <View style={{height:150, width:'90%', justifyContent:'center', alignItems:'center' }}>
              <View style={{height:52, width:'60%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20, textAlign:'center'}} placeholder={'Code STB'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Insert code from STB</Text>
            </View>
            
        
       

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLoginSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Login STB</Text>
      </TouchableOpacity>

      <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>ClickLoginScann()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity> */}
    </View> 
    //  </Provider>
  );
    
};



export default Song;
