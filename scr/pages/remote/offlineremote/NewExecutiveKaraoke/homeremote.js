import React, {
  Component,
  useState,
  useCallback,
  useEffect,
  useContext,
} from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  ToastAndroid,
  BackHandler,
  Alert,
  Image,
  Modal,
  StyleSheet,
  Animated,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Action_Fake, Action_SetSTBCode} from '../../../../redux/stb/actions';
import {Action_Command} from '../../../../redux/command/actions';
import {StbActionTypes} from '../../../../redux/stb/constants';
import {CommandActionTypes} from '../../../../redux/command/constants';
import {View, TextInput, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {WebSockets} from '../../../../lib/websocket';
import Stb from '../../../../redux/stb/reducers';
import {AuthContext} from '../../../../context/AuthContext';
import {normalize} from '../../../../component/style/Metrics';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Action_GetSTBServer} from '../../../../redux/stb_server/actions';
import {StbServerTypes} from '../../../../redux/stb_server/constants';
import * as Animatable from 'react-native-animatable';
import config from '../../../../../config';
import {APICore_STBCheckinCode} from '../../../../api';
import AsyncStorage from '@react-native-async-storage/async-storage';

const HomeRemote = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {loginSTB, Outlet, deviceOutlet} = useContext(AuthContext);
  let ParseOutlet = JSON.parse(deviceOutlet);

  const [OutletInfo, setOutletInfo] = useState({
    ApiDB: ParseOutlet?.ApiDB,
    ApiSocket: ParseOutlet?.ApiSocket,
    CodeOutlet: ParseOutlet?.CodeOutlet,
  });

  const [FadeIn, setFadIn] = useState(false);
  const [AlertMessage, setAlertMessage] = useState('');
  const [ModalAlert, setModalAlert] = useState(false);

  const [InputStb, setInputStb] = useState({
    code: 'XXXX',
    iduser: global.Id,
    durasi: 4,
    addtime: 1,
  });
  const [LoginSTB, SetLoginSTB] = useState(0);
  const [SendSTB, SetSendSTB] = useState(0);
  const [GetSocket, setGetSocket] = useState(0);

  const [tmr, settmr] = useState(0);

  const [ModalOutlet, setModalOutlet] = useState(false);
  // const {IMAGE_HOME, IMAGE_SONG} = config();

  const showToastWithGravity = input => {
    ToastAndroid.showWithGravity(
      input,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
      ToastAndroid.LONG,
    );
  };

  const {Server, loading, variable} = useSelector(state => ({
    Server: state.ServerStb.stbserver,
    loading: state.Songlist.loading,
    variable: state.Songlist.variable,
  }));

  const command = useSelector(state => {
    return state.Command.command;
  });

  const stb = useSelector(state => {
    return state.Stb.stb;
  });

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Executive Karaoke', 'Anda Ingin Keluar Aplikasi ?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'YES',
          onPress: () => BackHandler.exitApp(),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    if (SendSTB == 1) {
      SetSendSTB(0);
      loginSTB(stb);
    }
    if (LoginSTB == 1 && stb) {
      // console.log('stb', stb);
      // setModalAlert(true);
      // setAlertMessage(`stb : ${stb?.namadevices}, API : ${global.ID}`);

      setTimeout(() => {
        try {
          let join = {
            meta: 'join_room',
            message: {command: 'join'},
            roomID: stb.namadevices,
            clientID: InputStb.iduser,
          };
          WebSockets(join, ParseOutlet?.ApiSocket);
          SetLoginSTB(0);
          setTimeout(() => {
            SetSendSTB(1);
          }, 500);
        } catch (error) {
          console.log('ERROR:', error);
          setAlertMessage('ERROR', error);
        }
      }, 4000);

      // setTimeout(() => {
      //   setModalAlert(false);
      // }, 3000);
    }

    // if (GetSocket == 1) {
    // if (Server && Server.setting) {
    //   global.Socket = `http://${Server.setting[0].socket_url}/`;
    //   global.URL_SongList = `http://${Server.setting[0].url_dblagu}`;

    //   Outlet(OutletInfo);
    //   setModalOutlet(false);
    //   setGetSocket(0);
    // } else {
    //   if (GetSocket == 1) {
    //     setFadIn(true);
    //     setAlertMessage('Api Ws or wrong Outlet Code!');
    //     setGetSocket(0);

    //     setTimeout(() => {
    //       setFadIn(false);
    //     }, 3000);
    //   }
    // }
    // }

    // const interval = setTimeout(() => {
    //   settmr(tmr + 1);
    // }, 4000);

    return () => backHandler.remove();
  }, [stb, command, SendSTB, tmr, Server, GetSocket]);

  // console.log('stb', stb);

  const GetResponSTB = () => {
    console.log(stb);
  };

  const ClickLoginSTB = () => {
    if (InputStb.code == 'XXXX') {
      setModalAlert(true);
      setAlertMessage('Empty code stb!');

      setTimeout(() => {
        setModalAlert(false);
      }, 3000);
    } else {
      dispatch(
        Action_SetSTBCode(StbActionTypes.CONST_SET_REQUEST_STB_CODE, InputStb),
      );

      SetLoginSTB(1);
    }
  };

  const _HandleSaveOutlet = async () => {
    if (!OutletInfo.ApiDB || !OutletInfo.ApiSocket || !OutletInfo.CodeOutlet) {
      setFadIn(true);
      setAlertMessage('Field cannot be empty!');

      setTimeout(() => {
        setFadIn(false);
      }, 3000);
    } else {
      // global.DB = `${OutletInfo.ApiDB}`;
      await Outlet(OutletInfo);

      // dispatch(
      //   Action_GetSTBServer(
      //     StbServerTypes.CONST_SET_GET_STB_SERVER,
      //     OutletInfo,
      //   ),
      // );

      setModalOutlet(false);

      setGetSocket(1);
    }
  };

  return (
    <View
      style={
        //   {
        //   height: '100%',
        //   width: '100%',
        //   alignItems: 'center',
        //   backgroundColor: '#222222',
        // }
        style.Container
      }>
      <View
        style={{
          height: 285,
          width: '90%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {/* <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text> */}
        <Image
          resizeMode="center"
          source={config.IMAGE_HOME}
          // source={IMAGE_HOME}
          style={{height: '100%', width: '70%'}}></Image>
        {/* // ></Image> */}
      </View>

      {/* <TouchableOpacity
        style={{
          position: 'absolute',
          right: 10,
          top: 15,
        }}
        onPress={() => setModalOutlet(true)}>
        <Ionicons name="ellipsis-vertical" size={30} color={'#FFF'} />
      </TouchableOpacity> */}

      <View
        style={{
          // height: 150,
          width: '50%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            height: 52,
            width: '60%',
            backgroundColor: 'white',
            borderRadius: 12,
            flexDirection: 'row',
          }}>
          <TextInput
            style={{
              height: '100%',
              width: '100%',
              color: '#707070',
              fontSize: 28,
              padding: 5,
              textAlign: 'center',
              fontWeight: 'bold',
            }}
            placeholder={'Code STB'}
            placeholderTextColor={'#707070'}
            keyboardType="numeric"
            onChangeText={newText =>
              setInputStb({...InputStb, code: newText})
            }></TextInput>
          <View style={{width: 10}}></View>
          <TouchableOpacity
            style={{
              height: 50,
              width: 50,
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => ClickLoginSTB()}>
            <Image
              source={require('../../../../assets/executive/inout.png')}
              resizeMode="contain"
              style={{height: '100%', width: '100%'}}></Image>
          </TouchableOpacity>
        </View>

        <View
          style={{
            paddingVertical: 25,
            width: '60%',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{
              height: 52,
              width: '40%',
              backgroundColor: '#707070',
              borderRadius: 12,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => ClickLoginSTB()}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Login STB</Text>
          </TouchableOpacity>
        </View>
      </View>

      <Modal visible={ModalOutlet} transparent={true}>
        <View style={{...style.ContainerModal, justifyContent: 'flex-start'}}>
          <View
            style={{
              ...style.ContainerModal,
              backgroundColor: 'transparent',
              width: '100%',
            }}>
            <View
              style={{
                // height: 52,
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  paddingTop: 25,
                  width: '60%',
                }}>
                <TextInput
                  style={{
                    height: 52,
                    width: '100%',
                    backgroundColor: 'white',
                    borderRadius: 12,
                    color: '#707070',
                    fontSize: 28,
                    paddingHorizontal: 5,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}
                  placeholder="Insert Api DB"
                  placeholderTextColor={'#707070'}
                  value={OutletInfo.ApiDB}
                  onChangeText={newText =>
                    setOutletInfo({...OutletInfo, ApiDB: newText})
                  }></TextInput>

                <Text style={{color: 'white'}}>example : http://API</Text>
              </View>

              <View
                style={{
                  paddingTop: 25,
                  width: '60%',
                }}>
                <TextInput
                  style={{
                    height: 52,
                    width: '100%',
                    backgroundColor: 'white',
                    borderRadius: 12,
                    color: '#707070',
                    fontSize: 28,
                    paddingHorizontal: 5,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}
                  placeholder="Insert Api Socket"
                  placeholderTextColor={'#707070'}
                  value={OutletInfo.ApiSocket}
                  onChangeText={newText =>
                    setOutletInfo({...OutletInfo, ApiSocket: newText})
                  }></TextInput>

                <Text style={{color: 'white'}}>example : http://API/</Text>
              </View>

              <View
                style={{
                  paddingTop: 25,
                  width: '60%',
                }}>
                <TextInput
                  style={{
                    height: 52,
                    width: '100%',
                    backgroundColor: 'white',
                    borderRadius: 12,
                    color: '#707070',
                    fontSize: 28,
                    paddingHorizontal: 5,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}
                  placeholder="Insert Code Outlet"
                  placeholderTextColor={'#707070'}
                  value={OutletInfo.CodeOutlet}
                  onChangeText={newText =>
                    setOutletInfo({...OutletInfo, CodeOutlet: newText})
                  }></TextInput>

                <Text style={{color: 'white'}}>example : IDN</Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  paddingTop: 25,
                  justifyContent: 'space-evenly',
                  height: 52,
                  width: '60%',
                }}>
                <TouchableOpacity
                  style={{
                    height: 52,
                    width: '40%',
                    backgroundColor: '#707070',
                    borderRadius: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => setModalOutlet(false)}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                    Cancel
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{
                    height: 52,
                    width: '40%',
                    backgroundColor: '#707070',
                    borderRadius: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => _HandleSaveOutlet()}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                    Save
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          {FadeIn && (
            <Animatable.View
              animation="fadeInDown"
              duration={500}
              style={{
                position: 'absolute',
                backgroundColor: 'rgba(0, 0, 0, 0.7)',
                padding: 10,
                borderRadius: 12,
              }}>
              <Text
                style={{
                  fontSize: 28,
                  color: '#FFF',
                }}>
                {AlertMessage}
              </Text>
            </Animatable.View>
          )}
        </View>
      </Modal>

      <Modal visible={ModalAlert} transparent={true}>
        <View
          style={{
            ...style.ContainerModal,
            justifyContent: 'flex-start',
            backgroundColor: 'transparent',
          }}>
          <Animatable.View
            animation="fadeInDown"
            duration={500}
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0.7)',
              borderRadius: 12,
              padding: 10,
            }}>
            <Text
              style={{
                fontSize: 28,
                color: '#FFF',
              }}>
              {AlertMessage}
              {/* stb : {stb?.namadevices}
              API : {global.DB} */}
            </Text>
          </Animatable.View>
        </View>
      </Modal>
    </View>
  );
};

export default HomeRemote;

const style = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#222222',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ContainerModal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
});
