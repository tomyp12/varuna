import React, {Component, useEffect, useState, useContext} from 'react';
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  ToastAndroid,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Action_SetListSonglist} from '../../../../redux/songlist/actions';
import {SonglistActionTypes} from '../../../../redux/songlist/constants';
import {Action_GetSTBServer} from '../../../../redux/stb_server/actions';
import {StbServerTypes} from '../../../../redux/stb_server/constants';
import {
  View,
  TextInput,
  Text,
  Image,
  useWindowDimensions,
  FlatList,
  BackHandler,
  Alert,
  ImageBackground,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import ListSong from '../../../../component/listsongexecutive';
import PlaylistSong from '../../../../component/playlistsongexecutive';
import OurMenu from '../../../../component/ourmenuexecutive';
import BillingList from '../../../../component/billingexecutive';
import CartList from '../../../../component/cartexecutive';
import {Action_Command} from '../../../../redux/command/actions';
import {StbActionTypes} from '../../../../redux/stb/constants';
import {CommandActionTypes} from '../../../../redux/command/constants';
import {WebSockets} from '../../../../lib/websocket';
import ButtonEff from '../../../../component/buttonEff';
import {NumericFormat} from 'react-number-format';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Popover, {
  PopoverMode,
  PopoverPlacement,
} from 'react-native-popover-view';

import Remote from './remote';
import Slider from '@react-native-community/slider';
import CategoryList from '../../../../component/categoryexecutive';
import {AuthContext} from '../../../../context/AuthContext';
import {FlashList} from '@shopify/flash-list';
import {normalize} from '../../../../component/style/Metrics';
import config from '../../../../../config';

const Song = props => {
  const stb = useSelector(state => {
    return state.Stb.stb;
  });

  const {logoutSTB, deviceSTB, REFRESH} = useContext(AuthContext);
  let CodeSTB = JSON.parse(deviceSTB);

  const windowHeight = useWindowDimensions().height;
  const [selectListSong, setselectListSong] = useState(0);
  const [selectPlaylist, setselectPlaylist] = useState(0);
  // const [selectLisMenu, setselectListMenu] = useState(0);
  const [IdLaguSongList, setIdLaguSongList] = React.useState(0);
  const [IdLaguPlayList, setIdLaguPlayList] = React.useState(0);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [OPEN, SetOPEN] = useState(0);
  const [CARI, SetCARI] = useState(1);
  const [Condition, setCondition] = useState(1);
  const [Menu, setMenu] = useState(0);
  const [tmr, settmr] = useState(0);
  const [InputStb, setInputStb] = useState({
    code: 'XXXX',
    iduser: global.Id,
    durasi: 4,
    addtime: 1,
  });
  const [ViewSong, setViewSong] = useState(false);
  const [ViewMenu, setViewMenu] = useState(false);
  const [ViewCategory, setViewCategory] = useState(false);
  const [ViewBilling, setViewBilling] = useState(false);
  const [ViewMenuSong, setViewMenuSong] = useState(true);
  const [ViewCart, setViewCart] = useState(false);
  const [Toast, setToast] = useState({
    OrderSuccess: false,
    OrderFailed: false,
    Category: false,
    MenuFailed: false,
    BillingFailed: false,
    WaiterFailed: false,
    CategoryFailed: false,
    CheckConnection: false,
    Recently: false,
  });
  const [orderMsg, setorderMsg] = useState({
    Success: '',
    Failed: '',
  });

  const [RefreshPlaylist, setRefreshPlaylist] = useState('false');
  const [OnSearch, setOnSearch] = useState(false);
  const [OnPlayList, setOnPlayList] = useState('All Song');
  const [ListMenu, setListMenu] = useState('');
  const [Billing, setBilling] = useState('');
  const [Category, setCategory] = useState('');
  const [FocusButton, setFocusButton] = useState({
    SongsBackground: true,
    ArtistBackground: false,
  });
  const [SearchCondition, setSearchCondition] = useState(
    {OurMenu: false},
    {YourBilling: true},
    {hide: false},
  );
  const [QtyOrder, setQtyOrder] = useState({
    qty: 1,
    note: '',
  });
  const [cartOrder, setCartOrder] = useState([]);
  const [CartTotal, setCartTotal] = useState([]);
  const [orderItems, setOrderItems] = useState([]);
  const [ModalMenu, setModalMenu] = useState(props.onModal);
  const [GetDetail, setGetDetail] = useState('');
  const [OnOff, setOnOff] = useState({MusicOnOff: false, VocalOnOff: false});
  const [volumeStandby, setVolumeStandby] = useState(0);
  const [PopoverCondition, setPopoverCondition] = useState(
    {PopoverTempo: false},
    {PopoverKey: false},
  );
  const [LimitRefresh, setLimitRefresh] = useState(2);
  const [MsRefresh, setMsRefresh] = useState(5000);
  const [PlayListCheck, setPlayListCheck] = useState([]);

  const [Song, setSong] = useState({
    cari: 'TITLE',
    mode: 'SONGLIST',
    category: 'ALL',
    keyword: '',
  });

  const [Play_List, setPlay_List] = useState('');

  const [SearchMenu, setSearchMenu] = useState(['Name', 'SGroup']);
  const [ParamsMenu, setParamsMenu] = useState('');
  const [ParamsCategory, setParamsCategory] = useState('');
  const [SearchSong, setSearchSong] = useState(['judul']);
  const [ParamsSong, setParamsSong] = useState('');
  const [showPopover, setShowPopover] = useState(false);

  const getSearchMenu = () => {
    if (ParamsMenu.length >= 3 && ParamsCategory == '') {
      var filter = ListMenu.filter(item => {
        return (
          item.Name.toString().toLowerCase().indexOf(ParamsMenu.toLowerCase()) >
          -1
        );
      });
      return filter;
    }
    if (ParamsMenu.length >= 3 && ParamsCategory) {
      var filter = ListMenu.filter(item => {
        return (
          item.SGroup == ParamsCategory &&
          item.Name.toString().toLowerCase().indexOf(ParamsMenu.toLowerCase()) >
            -1
        );
      });
      return filter;
    }
    if (ParamsCategory && ParamsMenu.length < 3) {
      var filter = ListMenu.filter(item => {
        return item.SGroup == ParamsCategory;
      });
      return filter;
    } else {
      return ListMenu;
    }
  };

  const getSearchSong = items => {
    if (ParamsSong.length >= 3 && FocusButton.SongsBackground) {
      var filter = items.filter(item => {
        return (
          item.judul
            .toString()
            .toLowerCase()
            .indexOf(ParamsSong.toLowerCase()) > -1
        );
      });
      return filter;
    }
    if (ParamsSong.length >= 3 && FocusButton.ArtistBackground) {
      var filter = items.filter(item => {
        return (
          item.artis
            .toString()
            .toLowerCase()
            .indexOf(ParamsSong.toLowerCase()) > -1
        );
      });
      return filter;
    } else {
      return items;
    }
  };

  const changePlay = value => {
    if (value == 'PLAY' || value == 'NEXT' || value == 'RELOAD') {
      setOnOff({...OnOff, MusicOnOff: false});
    } else if (value == 'STOP' || value == 'PAUSE') {
      setOnOff({...OnOff, MusicOnOff: true});
    } else {
      setOnOff({...OnOff, MusicOnOff: false});
    }
  };

  const OnSave = (idlagu, judul, artis, path, vol, xvoc, voc) => {
    let y = {
      meta: 'send_message',
      message: {
        command: 'PLT',
        idlagu: idlagu,
        path: path,
        voc: voc,
        xvoc: xvoc,
        judul: judul,
        artis: artis,
        volume: vol,
        playmode: 2,
      },
      roomID: CodeSTB?.namadevices,
      clientID: InputStb?.iduser,
    };

    WebSockets(y);
  };

  // console.log('ws', global.Socket);
  // console.log('lagu', global.URL_SongList);
  useEffect(() => {
    // console.log('Global song:', global.playlist);
    // console.log('Global Status:', global.Socket);

    var axios = require('axios');
    const backAction = () => {
      Alert.alert('Executive Karaoke', 'Anda Ingin Keluar Remote ?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'YES',
          onPress: () => Perintah('OUT'),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    if (OPEN == 0) {
      // console.log('nah go');
      OnOpen();
      SetOPEN(1);
    }
    if (CARI == 1 && global.jaringan == 'true') {
      dispatch(
        Action_SetListSonglist(
          SonglistActionTypes.CONST_SET_GET_SONGLIST,
          Song,
        ),
      );
      SetCARI(0);
      setTimeout(() => {
        setOnSearch(false);
      }, 1000);
    }
    setOnOff({...OnOff, VocalOnOff: global.vocalon});
    if (Condition == 2) {
      // console.log('sini', Billing);
      var subtotal = 0;
      var discount = 0;
      var tax = 0;
      var grandTotal = 0;

      var configration = {
        method: 'get',
        url: `${config.API_URL_VSOFT}/api/bill/${CodeSTB?.nama}`,
        // url: `http://112.78.39.62:8123/api/bill/${CodeSTB?.nama}`,
        headers: {
          Accept: 'application/json',
          Username: 'varuna',
          Password: 'v4runa123',
        },
      };
      axios(configration)
        .then(response => {
          if (response.data.data.length >= 1) {
            subtotal = response.data.data.map(value => value.Total);
            discount = response.data.data.map(value => value.DiscRp);
            tax = response.data.data.map(
              value => value.Tax + value.ServiceCharge,
            );
            grandTotal = response.data.data.map(value => value.GrandTotal);

            setBilling({
              ...Billing,
              detail: response.data.data,
              subtotal: Math.round(
                subtotal.reduce((value, value1) => value + value1),
              ),
              discount: Math.round(
                discount.reduce((value, value1) => value + value1),
              ),
              tax: Math.round(tax.reduce((value, value1) => value + value1)),

              grandTotal: Math.round(
                grandTotal.reduce((value, value1) => value + value1),
              ),
            });
          }
          setCondition(1);
        })
        .catch(error => {
          console.log(error);
          // setBilling(error);
          if (error.message && global.jaringan == 'true') {
            setCondition(1);
            setorderMsg({...orderMsg, Failed: error.message});
            setToast({...Toast, BillingFailed: true});
            setTimeout(() => {
              setToast({...Toast, BillingFailed: false});
            }, 3000);
          }
        });
    }
    if (Menu == 1) {
      // console.log('menu');
      var configration = {
        method: 'get',
        url: `${config.API_URL_VSOFT}/api/categories`,
        // url: `http://112.78.39.62:8123/api/categories`,
        headers: {
          Accept: 'application/json',
          Username: 'varuna',
          Password: 'v4runa123',
        },
      };
      axios(configration)
        .then(response => {
          // console.log('CATEGORY:');
          setCategory(
            response.data.data.sort((a, b) =>
              a.sub_name.toLowerCase() > b.sub_name.toLowerCase() ? 1 : -1,
            ),
          );
        })
        .catch(error => {
          if (error.message && global.jaringan == 'true') {
            setorderMsg({...orderMsg, Failed: error.message});
            setToast({...Toast, CategoryFailed: true});
            setTimeout(() => {
              setToast({...Toast, CategoryFailed: false});
            }, 3000);
          }
        });

      var configration = {
        method: 'get',
        url: `${config.API_URL_VSOFT}/api/menu/EB`,
        // url: 'http://112.78.39.62:8123/api/menu/VB',
        headers: {
          Accept: 'application/json',
          Username: 'varuna',
          Password: 'v4runa123',
        },
      };
      axios(configration)
        .then(response => {
          setListMenu(response.data.data);
          setMenu(0);
        })
        .catch(error => {
          console.log(error);
          if (error.message && global.jaringan == 'true') {
            setMenu(0);
            setorderMsg({...orderMsg, Failed: error.message});
            setToast({...Toast, MenuFailed: true});
            setTimeout(() => {
              setToast({...Toast, MenuFailed: false});
            }, 3000);
          }
        });
    }
    if (global.jaringan != 'true') {
      if (
        Toast.BillingFailed == true ||
        Toast.OrderSuccess == true ||
        Toast.CategoryFailed == true ||
        Toast.MenuFailed == true
      ) {
        setTimeout(() => {
          setToast({
            ...Toast,
            CheckConnection: true,
            BillingFailed: false,
            OrderSuccess: false,
            CategoryFailed: false,
            MenuFailed: false,
          });
        }, 3000);
      } else {
        setToast({...Toast, CheckConnection: true});
      }
    } else {
      setToast({...Toast, CheckConnection: false});
    }

    if (LimitRefresh > 0) {
      setTimeout(() => {
        if (global.playlist == undefined) {
          let limit = LimitRefresh - 1;
          let ms = MsRefresh + 5000;
          setLimitRefresh(limit);
          setMsRefresh(ms);

          // console.log('limit berkurang :', LimitRefresh);
          // console.log('limit  :', limit);

          Refresh(deviceSTB);
        }
      }, MsRefresh);
    }
    if (LimitRefresh == 0 && global.playlist == undefined) {
      setRefreshPlaylist('true');
      // console.log('limit udah :', LimitRefresh);
    }
    // if (LimitRefresh == 0 && global.playlist != undefined) {
    //   console.log('global ada dan limit', LimitRefresh);

    // setRefreshPlaylist('false');
    // setLimitRefresh(2);
    // setMsRefresh(5000);
    // }

    const interval = setTimeout(() => {
      let m = {
        meta: 'send_message',
        message: {command: 'kosong'},
        roomID: CodeSTB?.namadevices,
        clientID: InputStb?.iduser,
      };
      WebSockets(m);
      settmr(tmr + 1);
    }, 2000);

    return () => backHandler.remove() + clearTimeout(interval);
  }, [tmr, CARI, Condition]);

  const Refresh = value => {
    // console.log('Refresh');
    REFRESH(value);
    setTimeout(() => {
      SetOPEN(0);
    }, 4000);
  };

  const showToastWithGravityAndOffset = () => {
    ToastAndroid.showWithGravityAndOffset(
      'Check Your Connection !',
      ToastAndroid.LONG,
      ToastAndroid.TOP,
    );
  };

  const OnPlayPlaylist = (idlagu, judul, artis, path, vol, xvoc, voc) => {
    let y = {
      meta: 'send_message',
      message: {
        command: 'PLY',
        idlagu: idlagu,
        path: path,
        voc: voc,
        xvoc: xvoc,
        judul: judul,
        artis: artis,
        volume: vol,
        playmode: 2,
      },
      roomID: CodeSTB?.namadevices,
      clientID: InputStb?.iduser,
    };

    WebSockets(y);
  };

  const OnPlay = (idlagu, judul, artis, path, vol, xvoc, voc) => {
    console.log('sini on play');
    let y = {
      meta: 'send_message',
      message: {
        command: 'PLY',
        idlagu: idlagu,
        path: path,
        voc: voc,
        xvoc: xvoc,
        judul: judul,
        artis: artis,
        volume: vol,
        playmode: 1,
      },
      roomID: CodeSTB?.namadevices,
      clientID: InputStb?.iduser,
    };

    WebSockets(y);
  };
  const carilagu = () => {
    SetCARI(1);
  };

  const carimenu = () => {
    setMenu(1);
  };
  // console.log('global.URL_SongList', global.URL_SongList);
  // console.log('global.Socket', global.Socket);
  // console.log('global.DB', global.DB);

  const {songlist, loading, variable, Server} = useSelector(state => ({
    songlist: state.Songlist.songlist?.results,
    loading: state.Songlist.loading,
    variable: state.Songlist.variable,
  }));

  const OnOpen = async () => {
    let m = {
      meta: 'send_message',
      message: {command: 'GO'},
      roomID: CodeSTB?.namadevices,
      clientID: InputStb?.iduser,
    };

    WebSockets(m);
  };

  const handleSelectionListSong = (id, value) => {
    var selectedId = selectListSong;

    if (selectedId === id) setselectListSong({selectedItem: null});
    else setselectListSong({selectedItem: id});
    setIdLaguSongList(value);
  };

  const handleSelectionPlaylist = (id, value) => {
    var selectedId = selectPlaylist;

    if (selectedId === id) setselectPlaylist({selectedItem: null});
    else setselectPlaylist({selectedItem: id});
    setIdLaguPlayList(value);
  };

  const select_cari = value => {
    setSong({...Song, cari: value});
    SetCARI(1);
  };

  const Perintah = (name, value) => {
    let x = null;
    switch (name) {
      case 'PLAY':
        console.log('PLAY');
        x = {command: 'PL'};
        // setOnOff({...OnOff, MusicOnOff: false});
        changePlay('PLAY');
        break;
      case 'STOP':
        console.log('STOP');
        x = {command: 'STP'};
        // setOnOff({...OnOff, MusicOnOff: true});
        changePlay('STOP');

        break;
      case 'PAUSE':
        console.log('PAUSE');
        x = {command: 'PS'};
        // setOnOff({...OnOff, MusicOnOff: true});
        changePlay('PAUSE');

        break;
      case 'NEXT':
        console.log('NEXT');
        x = {command: 'NEXT'};
        // setOnOff({...OnOff, MusicOnOff: false});
        changePlay('NEXT');

        break;
      case 'BACK':
        console.log('BACK');
        x = {command: 'BACK'};
        break;
      case 'RELOAD':
        console.log('RELOAD');
        x = {command: 'RE'};
        // setOnOff({...OnOff, MusicOnOff: false});
        changePlay('RELOAD');

        break;
      case 'VOL':
        console.log('VOL');
        let m = global.volume;
        if (value == 1) {
          m = global.volume + 10;
          if (m >= 100) {
            m = 100;
            setVolumeStandby(m);
          } else {
            setVolumeStandby(m);
          }
        }
        if (value == 0) {
          m = global.volume - 10;
          if (m <= 0) {
            m = 0;
            setVolumeStandby(m);
          } else {
            setVolumeStandby(m);
          }
        }
        x = {command: 'VOMS', value: m};

        break;
      case 'MIC':
        x = {command: 'VOMC', value: value};
        break;
      case 'TEMP':
        let s = 0;
        let tempomusic = '';
        if (value == 0) {
          s = global.tempo - 1;
          tempomusic = 'TPM';
          if (s < -3) {
            s = -3;
          }
        }
        if (value == 1) {
          s = global.tempo + 1;
          tempomusic = 'TPP';
          if (s >= 3) {
            s = 3;
          }
        }

        console.log('TEMPO:', s);
        x = {command: tempomusic, tempo: s};
        break;
      case 'KEY':
        let j = 0;
        let keymusic = '';
        if (value == 0) {
          j = global.key - 1;
          keymusic = 'KYM';
          if (j < -3) {
            j = -3;
          }
        }
        if (value == 1) {
          j = global.key + 1;
          keymusic = 'KYP';
          if (j >= 3) {
            j = 3;
          }
        }
        console.log('KEY:', j);
        x = {command: keymusic, key: j};
        break;
      case 'TRACK':
        x = {command: 'TRB', position: value};
        break;
      case 'REMOVED':
        x = {command: 'RMV', idlagu: value};
        break;
      case 'VOCAL':
        console.log('VOCAL');
        x = {command: 'VC'};
        break;
      case 'MUTE':
        if (global.volume == 0) {
          x = {command: 'VOMS', value: volumeStandby};
          setOnOff({...OnOff, VocalOnOff: false});
          break;
        } else {
          console.log('MUTE');
          setOnOff({...OnOff, VocalOnOff: true});
          x = {command: 'MT'};
          break;
        }
        break;
      case 'OUT':
        console.log('OUT');
        x = {command: 'STB'};
        logoutSTB();
        break;
      default:
        break;
    }

    let m = {
      meta: 'send_message',
      message: x,
      roomID: CodeSTB?.namadevices,
      clientID: InputStb?.iduser,
    };
    WebSockets(m);
  };

  const Remote = () => {
    navigation.push('Remote');
  };
  const Playlist = () => {
    navigation.push('Playlist');
  };

  const select_songmode = value => {
    setSong({...Song, mode: value, category: 'ALL'});
    SetCARI(1);
  };

  const select_songcategory = (value, value1) => {
    if (value1) {
      setSong({...Song, mode: value1, category: value});
      SetCARI(1);
    } else {
      setSong({...Song, category: value});
      SetCARI(1);
    }
  };

  const select_songrecently = Mode => {
    setSong({
      ...Song,
      mode: Mode,
      keyword: `${CodeSTB?.nama}`,
      category: 'ALL',
    });

    SetCARI(1);
  };

  const SelectSong = (Categori, Mode) => {
    if (Categori) {
      select_songcategory(Categori, Mode);
    } else if (Mode == 'RECENTLY') {
      select_songrecently(Mode);
    } else {
      select_songmode(Mode);
    }
  };

  const arraymoveUP = (arr, fromIndex, toIndex) => {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);

    let x = {
      meta: 'send_message',
      message: {command: 'PRT', playlist: arr},
      roomID: CodeSTB?.namadevices,
      clientID: InputStb?.iduser,
    };

    WebSockets(x);
  };

  const select_UPDOWN = (value, condition) => {
    if (condition === 'UP') {
      var resultTarget = 0;
    }
    if (condition === 'DOWN') {
      var resultTarget = value + 1;
    }
    arraymoveUP(global.playlist, value, resultTarget);
  };

  const Select_Delete = value => {
    cartOrder.splice(value, 1);
    orderItems.splice(value, 1);
    CartTotal.splice(value, 1);
  };

  const ChangeNote = (notes, i) => {
    setCartOrder(
      cartOrder.map((value, index) =>
        index == i
          ? {
              ...value,
              note: notes,
            }
          : value,
      ),
    );
    setOrderItems(
      orderItems.map((value, index) =>
        index == i
          ? {
              ...value,
              note: notes,
            }
          : value,
      ),
    );
  };

  const callWaiters = () => {
    var axios = require('axios');

    var configration = {
      // method: 'put',
      method: 'post',
      url: `${config.API_URL_VSOFT}/api/rooms/${CodeSTB?.nama}/callWaiter`,
      // url: `http://112.78.39.62:8123/api/rooms/${CodeSTB?.nama}/callWaiter`,
      headers: {
        Accept: 'application/json',
        Username: 'varuna',
        Password: 'v4runa123',
      },
    };
    axios(configration)
      .then(response => {
        console.log('CALL', response);
      })
      .catch(error => {
        console.log('error:', error);
        // setorderMsg({...orderMsg, Failed: error});
        // setToast({...Toast, WaiterFailed: true});
        // setTimeout(() => {
        //   setToast({...Toast, WaiterFailed: false});
        // }, 3000);
      });
  };

  const Cart = props => {
    var item = {
      code: props.Code,
      name: props.Name,
      harga: props.Harga,
      // note: [QtyOrder.note],
      // note: QtyOrder.note,
      note: '',
      qty: 1,
    };

    var itemOrder = {
      code: props.Code,
      // note: [QtyOrder.note],
      note: '',
      qty: 1,
    };

    cartOrder.push(item);
    orderItems.push(itemOrder);
    CartTotal.push(props.Harga);
    setQtyOrder({
      qty: 1,
      note: '',
    });

    // console.log(CartTotal.length);
  };

  const Order = () => {
    var axios = require('axios');
    var data = {
      // room_no: 666,
      room_no: CodeSTB?.nama,
      order_items: orderItems,
    };

    var configration = {
      method: 'post',
      url: `${config.API_URL_VSOFT}/api/order`,
      // url: `http://112.78.39.62:8123/api/order`,
      headers: {
        Accept: 'application/json',
        Username: 'varuna',
        Password: 'v4runa123',
      },
      data: data,
    };
    axios(configration)
      .then(response => {
        // console.log('Response:', response);
        setCartOrder([]);
        setOrderItems([]);
        setToast({...Toast, OrderSuccess: true});
        setTimeout(() => {
          setToast({...Toast, OrderSuccess: false});
        }, 3000);
      })
      .catch(error => {
        // console.log('error:', error);
        setorderMsg({...orderMsg, Failed: error});
        setToast({...Toast, OrderFailed: true});
        setTimeout(() => {
          setToast({...Toast, OrderFailed: false});
        }, 3000);
      });
  };

  const ListIcon = [
    {
      Name: 'ALL',
      SubName: 'SONG',
      Mode: 'SONGLIST',
      Category: '',
      OnPlaylist: 'All Song',
      Search: true,
    },
    {
      Name: 'NEW',
      SubName: 'SONG',
      Mode: 'NEW',
      Category: '',
      OnPlaylist: 'New Song',
      Search: false,
    },
    {
      Name: 'POPULAR',
      SubName: 'SONG',
      Mode: 'POPULAR',
      Category: '',
      OnPlaylist: 'Popular Song',
      Search: false,
    },
    {
      Name: 'HITS',
      SubName: 'SONG',
      Mode: 'HITS',
      Category: 'ALL',
      OnPlaylist: 'Hits Song',
      Search: false,
    },
    {
      Name: 'RECENTLY',
      SubName: 'SONG',
      Mode: 'RECENTLY',
      Category: '',
      OnPlaylist: 'Recently Song',
      Search: false,
    },
    {
      Name: '',
      SubName: 'INDONESIA',
      Mode: 'SONGLIST',
      Category: 'INDONESIA',
      OnPlaylist: 'Indonesia',
      Search: true,
    },
    {
      Name: '',
      SubName: 'WESTERN',
      Mode: 'SONGLIST',
      Category: 'BARAT',
      OnPlaylist: 'Western',
      Search: true,
    },
    {
      Name: '',
      SubName: 'MANDARIN',
      Mode: 'SONGLIST',
      Category: 'MANDARIN',
      OnPlaylist: 'Mandarin',
      Search: true,
    },
    {
      Name: '',
      SubName: 'JAPANESE',
      Mode: 'SONGLIST',
      Category: 'JAPAN',
      OnPlaylist: 'Japanese',
      Search: true,
    },
    {
      Name: '',
      SubName: 'INDIA',
      Mode: 'SONGLIST',
      Category: 'INDIA',
      OnPlaylist: 'India',
      Search: true,
    },
    {
      Name: '',
      SubName: 'MALAYSIAN',
      Mode: 'SONGLIST',
      Category: 'MALAYSIA',
      OnPlaylist: 'MALAYSIAN',
      Search: true,
    },
    {
      Name: '',
      SubName: 'KOREAN',
      Mode: 'SONGLIST',
      Category: 'KOREA',
      OnPlaylist: 'Korean',
      Search: true,
    },
    {
      Name: '',
      SubName: 'EDM',
      Mode: 'SONGLIST',
      Category: 'EDM',
      OnPlaylist: 'Edm',
      Search: true,
    },
    // {
    //   Name: 'HOUSE',
    //   SubName: 'MUSIC',
    //   Mode: 'NEW',
    //   Category: 'EDM',
    //   OnPlaylist: 'House Music',
    //   Search: true,
    // },
  ];

  const _LoadingHit = () => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <ActivityIndicator size="large" color="#bcbcbb" />
      </View>
    );
  };

  const _ListEmptyComponent = () => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontSize: 15,
            fontWeight: '800',
            color: '#707070',
          }}>
          Data not found!
        </Text>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        width: '100%',
        backgroundColor: '#222222',
        minHeight: Math.round(windowHeight),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <View
        style={{
          width: '10%',
          height: '100%',
          backgroundColor: ModalMenu == true ? '#0d0d0d' : null,
          opacity: ModalMenu == true ? 0.9 : null,
        }}>
        <View
          style={{
            width: '100%',
            height: '15%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={config.IMAGE_SONG}
            resizeMode="contain"
            style={{height: '70%', width: '70%'}}></Image>
        </View>
        <View
          style={{
            width: '100%',
            height: '25%',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: '#FFF',
            }}>
            Temp
          </Text>

          <TouchableOpacity
            style={{
              width: 60,
              height: 60,
              borderRadius: 60,
              backgroundColor: '#707070',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Perintah('TEMP', 1)}>
            <Image
              source={require('../../../../assets/executive/plus.png')}
              resizeMode="contain"
              style={{height: '50%', width: '50%'}}></Image>
          </TouchableOpacity>
          <Text style={{color: '#FFF'}}>{global.tempo ? global.tempo : 0}</Text>
          <TouchableOpacity
            style={{
              width: 60,
              height: 60,
              borderRadius: 60,
              backgroundColor: '#707070',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Perintah('TEMP', 0)}>
            <Image
              source={require('../../../../assets/executive/minus.png')}
              resizeMode="contain"
              style={{height: '50%', width: '50%'}}></Image>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            height: '25%',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: '#FFF',
            }}>
            Key
          </Text>
          <TouchableOpacity
            style={{
              width: 60,
              height: 60,
              borderRadius: 60,
              backgroundColor: '#707070',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Perintah('KEY', 1)}>
            <Image
              source={require('../../../../assets/executive/plus.png')}
              resizeMode="contain"
              style={{height: '50%', width: '50%'}}></Image>
          </TouchableOpacity>
          <Text style={{color: '#FFF'}}>{global.key ? global.key : 0}</Text>
          <TouchableOpacity
            style={{
              width: 60,
              height: 60,
              borderRadius: 60,
              backgroundColor: '#707070',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Perintah('KEY', 0)}>
            <Image
              source={require('../../../../assets/executive/minus.png')}
              resizeMode="contain"
              style={{height: '50%', width: '50%'}}></Image>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            height: '25%',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: '#FFF',
            }}>
            Vol.
          </Text>
          <TouchableOpacity
            style={{
              width: 60,
              height: 60,
              borderRadius: 60,
              backgroundColor: '#707070',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Perintah('VOL', 1)}>
            <Image
              source={require('../../../../assets/executive/plus.png')}
              resizeMode="contain"
              style={{height: '50%', width: '50%'}}></Image>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: 60,
              height: 60,
              borderRadius: 60,
              backgroundColor: '#707070',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => Perintah('VOL', 0)}>
            <Image
              source={require('../../../../assets/executive/minus.png')}
              resizeMode="contain"
              style={{height: '50%', width: '50%'}}></Image>
          </TouchableOpacity>
          <Text style={{fontSize: 12, color: '#FFF'}}>Version 4.4 L</Text>
        </View>
      </View>

      <View
        style={{
          width: '90%',
          height: '100%',
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: ModalMenu == true ? '#0d0d0d' : '#222222',
            opacity: ModalMenu == true ? 0.9 : null,
            height: '15%',
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
          }}>
          <Text style={{fontSize: 14, color: '#ffff', fontWeight: 'bold'}}>
            Welcome Mr. Aa Bb Ccc
          </Text>

          {ViewMenuSong == true && (
            <>
              {ViewSong == false ? (
                <TouchableOpacity
                  onPress={() => setViewSong(true) + setOnPlayList('All Song')}
                  style={{
                    height: 50,
                    width: 400,
                    backgroundColor: 'white',
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      color: '#707070',
                      fontWeight: 'normal',
                      flex: 1,
                      textAlign: 'center',
                    }}>
                    Search Your Music
                  </Text>
                  <Image
                    source={require('../../../../assets/executive/search.png')}
                    resizeMode="contain"
                    style={{
                      height: 30,
                      width: 40,
                    }}></Image>
                </TouchableOpacity>
              ) : Song.mode == 'NEW' ||
                Song.mode == 'POPULAR' ||
                Song.mode == 'HITS' ||
                Song.mode == 'RECENTLY' ? null : (
                <View
                  style={{
                    height: 50,
                    width: 400,
                    backgroundColor: 'white',
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <TextInput
                    style={{
                      flex: 1,
                      height: '100%',
                      color: '#707070',
                      textAlign: 'auto',
                      fontSize: 20,
                      borderTopLeftRadius: 15,
                      borderBottomLeftRadius: 15,
                      paddingLeft: 10,
                    }}
                    enterKeyHint="search"
                    value={Song.keyword}
                    onSubmitEditing={({nativeEvent: {text}}) => {
                      carilagu();
                      setOnSearch(true);
                    }}
                    onChangeText={newText => {
                      setSong({...Song, keyword: newText});
                    }}></TextInput>
                  <TouchableOpacity
                    onPress={() => setSong({...Song, keyword: ''}) + carilagu()}
                    style={{
                      height: '100%',
                      borderTopRightRadius: 15,
                      borderBottomRightRadius: 15,
                      justifyContent: 'center',
                      padding: 5,
                    }}>
                    <Text
                      style={{
                        paddingRight: 5,
                        fontSize: 20,
                        color: '#707070',
                        fontWeight: 'bold',
                      }}>
                      Clear
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            </>
          )}

          {ViewMenu == true && (
            <>
              {SearchCondition.OurMenu == false && ModalMenu == false ? (
                <TouchableOpacity
                  onPress={() =>
                    setSearchCondition({...SearchCondition, OurMenu: true})
                  }
                  style={{
                    height: 50,
                    width: 400,
                    backgroundColor: 'white',
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      color: '#707070',
                      fontWeight: 'normal',
                      flex: 1,
                      textAlign: 'center',
                    }}>
                    What's Your Fav ?
                  </Text>
                  <Image
                    source={require('../../../../assets/executive/search.png')}
                    resizeMode="contain"
                    style={{
                      height: 30,
                      width: 40,
                    }}></Image>
                </TouchableOpacity>
              ) : (
                SearchCondition.OurMenu == true &&
                ModalMenu == false && (
                  <TouchableOpacity
                    onPress={
                      () =>
                        setSearchCondition({
                          ...SearchCondition,
                          OurMenu: false,
                        }) + setParamsMenu('')
                      // +
                      // setParamsCategory('')
                    }
                    style={{
                      height: 50,
                      width: 400,
                      backgroundColor: 'white',
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <TextInput
                      style={{
                        flex: 1,
                        height: '100%',
                        color: '#707070',
                        textAlign: 'auto',
                        fontSize: 20,
                        borderTopLeftRadius: 15,
                        borderBottomLeftRadius: 15,
                        paddingLeft: 10,
                      }}
                      enterKeyHint="search"
                      value={ParamsMenu}
                      placeholderTextColor={'#707070'}
                      onSubmitEditing={() => carimenu()}
                      onChangeText={newText =>
                        setParamsMenu(newText)
                      }></TextInput>
                    <View
                      style={{
                        height: '100%',
                        borderTopRightRadius: 15,
                        borderBottomRightRadius: 15,
                        justifyContent: 'center',
                        padding: 5,
                      }}>
                      <Text
                        style={{
                          paddingRight: 5,
                          fontSize: 20,
                          color: '#707070',
                          fontWeight: 'bold',
                        }}>
                        Clear
                      </Text>
                    </View>
                  </TouchableOpacity>
                )
              )}

              {ModalMenu == true && (
                <>
                  {SearchCondition.OurMenu == false ? (
                    <View
                      style={{justifyContent: 'center', alignItems: 'center'}}>
                      <TouchableOpacity
                        style={{
                          height: 50,
                          width: 400,
                          backgroundColor: 'white',
                          borderRadius: 15,
                          justifyContent: 'center',
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontSize: 20,
                            color: '#707070',
                            fontWeight: 'normal',
                            flex: 1,
                            textAlign: 'center',
                          }}>
                          What's Your Fav ?
                        </Text>
                        <Image
                          source={require('../../../../assets/executive/search.png')}
                          resizeMode="contain"
                          style={{
                            height: 30,
                            width: 40,
                          }}></Image>
                      </TouchableOpacity>
                      <View
                        style={{
                          height: 50,
                          width: 400,
                          position: 'absolute',
                          backgroundColor: '#0d0d0d',
                          opacity: 0.9,
                        }}></View>
                    </View>
                  ) : (
                    SearchCondition.OurMenu == true && (
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <TouchableOpacity
                          onPress={() =>
                            setSearchCondition({
                              ...SearchCondition,
                              OurMenu: false,
                            }) + setParamsMenu('')
                          }
                          style={{
                            height: 50,
                            width: 400,
                            backgroundColor: 'white',
                            borderRadius: 15,
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row',
                          }}>
                          <TextInput
                            style={{
                              flex: 1,
                              height: '100%',
                              color: '#707070',
                              textAlign: 'auto',
                              fontSize: 20,
                              borderTopLeftRadius: 15,
                              borderBottomLeftRadius: 15,
                              paddingLeft: 10,
                            }}
                            enterKeyHint="search"
                            value={ParamsMenu}
                            placeholderTextColor={'#707070'}
                            onSubmitEditing={() => carimenu()}
                            onChangeText={newText =>
                              setParamsMenu(newText)
                            }></TextInput>
                          <View
                            style={{
                              height: '100%',
                              borderTopRightRadius: 15,
                              borderBottomRightRadius: 15,
                              justifyContent: 'center',
                              padding: 5,
                            }}>
                            <Text
                              style={{
                                paddingRight: 5,
                                fontSize: 20,
                                color: '#707070',
                                fontWeight: 'bold',
                              }}>
                              Clear
                            </Text>
                          </View>
                        </TouchableOpacity>
                        <View
                          style={{
                            height: 50,
                            width: 400,
                            position: 'absolute',
                            backgroundColor: '#0d0d0d',
                            opacity: 0.9,
                          }}></View>
                      </View>
                    )
                  )}
                </>
              )}
            </>
          )}

          <View
            style={{
              height: 50,
              width: 150,
              flexDirection: 'row',
              justifyContent: 'flex-end',
            }}>
            <View
              style={{
                height: '100%',
                width:
                  ViewMenu == true ||
                  ViewBilling == true ||
                  ViewSong == true ||
                  ViewCart == true ||
                  ViewCategory == true
                    ? '50%'
                    : '100%',
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              {/* <TouchableOpacity
                onPress={() => Refresh(deviceSTB)}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                  padding: 10,
                }}>
                <Text style={{color: '#FFF', paddingRight: 10}}>Refresh</Text>
                <IconFw name="refresh" color={'#FFF'} size={25} />
              </TouchableOpacity> */}
            </View>

            {
              ViewMenu == true ||
              ViewBilling == true ||
              ViewSong == true ||
              ViewCart == true ||
              ViewCategory == true ? (
                <View
                  style={{
                    heipnght: '100%',
                    width: '50%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}
                    onPress={() =>
                      // ModalMenu == false &&
                      ViewMenu == true
                        ? setViewMenu(false) +
                          // setViewMenuSong(true) +
                          setSearchCondition({
                            ...SearchCondition,
                            OurMenu: false,
                          }) +
                          setParamsMenu('') +
                          setParamsCategory('') +
                          setViewSong(false) +
                          setViewCart(false) +
                          setViewCategory(true) +
                          setMenu(0)
                        : ViewBilling == true
                        ? setViewBilling(false) +
                          setViewMenuSong(true) +
                          setSearchCondition({
                            ...SearchCondition,
                            OurMenu: false,
                          }) +
                          setParamsMenu('') +
                          setParamsCategory('') +
                          setViewSong(false) +
                          setViewCart(false) +
                          setCondition(1)
                        : ViewCart == true
                        ? setViewMenu(false) +
                          setViewMenuSong(true) +
                          setSearchCondition({
                            ...SearchCondition,
                            OurMenu: false,
                          }) +
                          setParamsMenu('') +
                          setParamsCategory('') +
                          setViewSong(false) +
                          setViewCart(false)
                        : ViewSong == true
                        ? setViewSong(false) +
                          setSong({
                            cari: 'TITLE',
                            mode: 'SONGLIST',
                            category: 'ALL',
                            keyword: '',
                          }) +
                          carilagu()
                        : ViewCategory == true
                        ? setViewMenu(false) +
                          setViewMenuSong(true) +
                          setSearchCondition({
                            ...SearchCondition,
                            OurMenu: false,
                          }) +
                          setParamsMenu('') +
                          setParamsCategory('') +
                          setViewSong(false) +
                          setViewCart(false) +
                          setViewCategory(false) +
                          setMenu(0)
                        : setModalMenu(false) +
                          setQtyOrder({
                            qty: 1,
                            note: '',
                          })
                    }>
                    <Text style={{color: '#FFF'}}>Back</Text>

                    <Image
                      source={require('../../../../assets/executive/back.png')}
                      resizeMode="contain"
                      style={{
                        height: 30,
                        width: 40,
                      }}></Image>
                  </TouchableOpacity>
                </View>
              ) : null
              // (
              //   <View
              //     style={{
              //       height: 50,
              //       width: 150,
              //       justifyContent: 'center',
              //       alignItems: 'center',
              //       flexDirection: 'row',
              //     }}>
              //     <TouchableOpacity
              //       onPress={() =>
              //         setViewCart(true) +
              //         setViewMenuSong(false) +
              //         setViewMenu(false) +
              //         setModalMenu(false)
              //       }
              //       style={{
              //         height: 50,
              //         width: 50,
              //         backgroundColor: '#e67e22',
              //         borderRadius: 100,
              //         alignItems: 'center',
              //         justifyContent: 'center',
              //       }}>
              //       <Icon name="shopping-cart" color={'#FFF'} size={26}></Icon>

              //       {cartOrder.length > 0 && (
              //         <Text
              //           style={{
              //             position: 'absolute',
              //             backgroundColor: 'red',
              //             right: -5,
              //             top: -5,
              //             width: 25,
              //             height: 25,
              //             color: '#FFF',
              //             borderRadius: 50,
              //             textAlign: 'center',
              //           }}>
              //           {cartOrder.length}
              //         </Text>
              //       )}
              //     </TouchableOpacity>
              //   </View>
              // )
            }
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: '98%',
            flexDirection: 'row',
          }}>
          <View
            style={{
              width:
                ViewMenuSong == true || ViewMenu == true || ViewCategory == true
                  ? '70%'
                  : '100%',
              height: '85%',
            }}>
            {ViewMenuSong == true && (
              <>
                {ViewSong == false && (
                  <View
                    style={{
                      flex: 1,
                      // width: '100%',
                      // height: '80%',
                    }}>
                    <FlashList
                      data={ListIcon}
                      numColumns={3}
                      keyExtractor={(item, index) => index}
                      estimatedItemSize={200}
                      renderItem={({item, index}) => {
                        return (
                          <View
                            style={{
                              flex: 1,
                              alignItems: 'center',
                              padding: normalize(5),
                            }}>
                            <TouchableOpacity
                              onPress={() =>
                                !CodeSTB?.nama && item.Name == 'RECENTLY'
                                  ? setToast({...Toast, Recently: true}) +
                                    setTimeout(() => {
                                      setToast({...Toast, Recently: false});
                                    }, 2000)
                                  : SelectSong(item.Category, item.Mode) +
                                    setViewSong(true) +
                                    setOnPlayList(item.OnPlaylist)
                              }
                              style={{
                                height: 220,
                                width: 220,
                                borderRadius: 25,
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                padding: normalize(2),
                              }}>
                              {item.Name ? (
                                <View
                                  style={{
                                    alignItems: 'center',
                                  }}>
                                  <View>
                                    <Text
                                      style={{
                                        fontSize: normalize(8),
                                        color: '#707070',
                                        fontWeight: 'normal',
                                      }}>
                                      {item.Name}
                                    </Text>

                                    <Text
                                      numberOfLines={2}
                                      style={{
                                        fontSize: normalize(9),
                                        color: 'black',
                                        fontWeight: 'bold',
                                      }}>
                                      {item.SubName}
                                    </Text>
                                  </View>
                                </View>
                              ) : (
                                <View
                                  style={{
                                    alignItems: 'center',
                                  }}>
                                  <Text
                                    numberOfLines={2}
                                    style={{
                                      fontSize: normalize(9),
                                      color: 'black',
                                      fontWeight: 'bold',
                                    }}>
                                    {item.SubName}
                                  </Text>
                                </View>
                              )}
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </View>
                )}

                {ViewSong == true && (
                  <>
                    <View
                      style={{
                        paddingBottom: 10,
                        flexDirection: 'row',
                        // justifyContent: 'space-between',
                      }}>
                      <View style={{flex: 1}}>
                        {Song.mode == 'NEW' ||
                        Song.mode == 'POPULAR' ||
                        Song.mode == 'HITS' ||
                        Song.mode == 'RECENTLY' ? null : (
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              width: 210,
                            }}>
                            <TouchableOpacity
                              style={{
                                borderRadius: 100,
                                backgroundColor:
                                  FocusButton.SongsBackground == true
                                    ? '#FFF'
                                    : '#bcbcbb',
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: 5,
                                width: 100,
                              }}
                              onPress={() => {
                                setFocusButton({
                                  SongsBackground: true,
                                  ArtistBackground: false,
                                });
                                select_cari('TITLE');
                              }}>
                              <Text
                                style={{
                                  fontSize: 20,
                                  fontWeight: 'bold',
                                  color: '#707070',
                                }}>
                                Title
                              </Text>

                              {FocusButton.SongsBackground == true && (
                                <View style={{position: 'absolute', right: 0}}>
                                  <Ionicons
                                    size={20}
                                    name="checkmark-circle-sharp"
                                    color={'#e67e22'}
                                  />
                                </View>
                              )}
                            </TouchableOpacity>

                            <TouchableOpacity
                              style={{
                                borderRadius: 100,
                                backgroundColor:
                                  FocusButton.ArtistBackground == true
                                    ? '#FFF'
                                    : '#bcbcbb',
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: 5,
                                width: 100,
                              }}
                              onPress={() => {
                                setFocusButton({
                                  SongsBackground: false,
                                  ArtistBackground: true,
                                });
                                select_cari('ARTIST');
                              }}>
                              <Text
                                style={{
                                  fontSize: 20,
                                  fontWeight: 'bold',
                                  color: '#707070',
                                }}>
                                Artist
                              </Text>

                              {FocusButton.ArtistBackground == true && (
                                <View style={{position: 'absolute', right: 0}}>
                                  <Ionicons
                                    size={20}
                                    name="checkmark-circle-sharp"
                                    color={'#e67e22'}
                                  />
                                </View>
                              )}
                            </TouchableOpacity>
                          </View>
                        )}
                      </View>

                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            color: '#FFF',
                          }}>
                          {OnPlayList}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        flex: 1,
                        borderRadius: 20,
                        backgroundColor: '#FFF',
                        paddingBottom: 18,
                      }}>
                      <View
                        style={{
                          width: '100%',
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderRadius: 20,
                        }}>
                        <View
                          style={{
                            height: 55,
                            width: '80%',
                            justifyContent: 'center',
                            flexDirection: 'row',
                          }}>
                          <View
                            style={{
                              height: '100%',
                              width: '50%',
                              justifyContent: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: 20,
                                color: '#707070',
                                fontWeight: 'bold',
                              }}>
                              Song Name
                            </Text>
                          </View>
                          <View
                            style={{
                              height: '100%',
                              width: '60%',
                              justifyContent: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: 20,
                                color: '#707070',
                                fontWeight: 'bold',
                              }}>
                              Artist
                            </Text>
                          </View>
                        </View>
                      </View>

                      {OnSearch ? (
                        _LoadingHit()
                      ) : (
                        <FlashList
                          data={
                            OnPlayList == 'New Song'
                              ? getSearchSong(songlist)
                              : songlist
                          }
                          // numColumns={0}
                          // keyExtractor={(item, index) => item.idlagu}
                          onEndReachedThreshold={0.5}
                          // showsVerticalScrollIndicator={false}
                          // extraData={selectListSong}
                          ListEmptyComponent={() => _ListEmptyComponent()}
                          initialNumToRender={100}
                          estimatedItemSize={200}
                          renderItem={({item, index}) => {
                            let newItem;
                            if (
                              Song.mode == 'POPULAR' ||
                              Song.mode == 'RECENTLY'
                            ) {
                              newItem = item.idlagu;
                            } else {
                              newItem = item;
                            }
                            return (
                              <ListSong
                                // OnSelect={() =>
                                //   handleSelectionListSong(index, item.idlagu)
                                // }
                                OnSelect={() => setselectListSong(index)}
                                OnSelectedItem={selectListSong}
                                onIndex={index}
                                title={newItem.judul}
                                singer={newItem.artis}
                                OnSave={() =>
                                  OnSave(
                                    newItem.idlagu,
                                    newItem.judul,
                                    newItem.artis,
                                    newItem.path,
                                    newItem.vol,
                                    newItem.xvoc,
                                    newItem.voc,
                                  )
                                }
                                OnPlay={() =>
                                  OnPlay(
                                    newItem.idlagu,
                                    newItem.judul,
                                    newItem.artis,
                                    newItem.path,
                                    newItem.vol,
                                    newItem.xvoc,
                                    newItem.voc,
                                  )
                                }></ListSong>
                            );
                          }}
                        />
                      )}
                    </View>
                  </>
                )}
              </>
            )}

            {ViewCategory == true && (
              <View
                style={{
                  flex: 1,
                }}>
                {Category == undefined || Category == '' ? (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator size="large" color="#bcbcbb" />
                  </View>
                ) : (
                  <FlatList
                    data={Category}
                    numColumns={3}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({item, index}) => {
                      return (
                        <CategoryList
                          OnSelect={() =>
                            setParamsCategory(item.sub_code) +
                            getSearchMenu() +
                            setViewCategory(false) +
                            setViewMenu(true)
                          }
                          onIndex={index}
                          name={item.sub_name}></CategoryList>
                      );
                    }}
                  />
                )}
              </View>
            )}

            {ViewMenu == true && (
              <View
                style={{
                  flex: 1,
                  borderRadius: 20,
                  backgroundColor: '#FFF',
                  paddingBottom: 18,
                }}>
                <View
                  style={{
                    height: 55,
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderRadius: 20,
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      height: '100%',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#707070',
                        fontWeight: 'bold',
                      }}>
                      Name
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      height: '100%',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#707070',
                        fontWeight: 'bold',
                      }}>
                      Price
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 0.1,
                      height: '100%',
                    }}></View>
                </View>

                {ListMenu == undefined || ListMenu == '' ? (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator size="large" color="#bcbcbb" />
                  </View>
                ) : (
                  <FlatList
                    data={getSearchMenu(ListMenu)}
                    numColumns={0}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({item, index}) => {
                      return (
                        <OurMenu
                          OnSave={() => Cart(item)}
                          onIndex={index}
                          name={item.Name}
                          deskripsi={item.MGroup + ' - ' + item.SGroup}
                          harga={Math.round(item.Harga)}
                          image={require('../../../../assets/executive/plus.png')}></OurMenu>
                      );
                    }}
                  />
                )}
              </View>
            )}

            {ViewBilling == true && (
              <View
                style={{
                  width: '100%',
                  height: '82%',
                  padding: 10,
                }}>
                <Text
                  style={{
                    fontSize: 25,
                    padding: 10,
                    color: '#FFF',
                  }}>
                  Order - {CodeSTB?.nama}
                </Text>

                <View
                  style={{
                    width: '100%',
                    height: '89%',
                    borderRadius: 20,
                    backgroundColor: '#FFF',
                    paddingBottom: 18,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 20,
                      padding: 10,
                      paddingBottom: 0,
                    }}>
                    <View
                      style={{
                        flex: 2,
                        alignSelf: 'stretch',
                      }}>
                      <Text
                        style={{
                          padding: 10,
                          fontSize: 20,
                          color: '#707070',
                          fontWeight: 'bold',
                        }}>
                        Items
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 0.5,
                        alignSelf: 'stretch',
                      }}>
                      <Text
                        style={{
                          padding: 10,
                          fontSize: 20,
                          color: '#707070',
                          fontWeight: 'bold',
                        }}>
                        Qty
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignSelf: 'stretch',
                      }}>
                      <Text
                        style={{
                          padding: 10,
                          fontSize: 20,
                          color: '#707070',
                          fontWeight: 'bold',
                        }}>
                        Amount
                      </Text>
                    </View>
                  </View>

                  {Billing == '' || Billing == undefined ? (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <ActivityIndicator size="large" color="#bcbcbb" />
                    </View>
                  ) : Billing.detail == '' || Billing.detail == undefined ? (
                    <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 10,
                      }}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: '#707070',
                          fontWeight: 'bold',
                        }}>
                        No Bill
                      </Text>
                    </View>
                  ) : (
                    <FlatList
                      data={Billing.detail}
                      numColumns={0}
                      keyExtractor={(item, index) => String(index)}
                      renderItem={({item, index}) => {
                        return (
                          <BillingList
                            onIndex={index}
                            name={item.NamaItem}
                            qty={item.Qty}
                            total={Math.round(item.Total)}></BillingList>
                        );
                      }}
                    />
                  )}
                </View>

                <Modal
                  animationType="fade"
                  transparent={true}
                  visible={Toast.BillingFailed}>
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      marginTop: 50,
                      alignItems: 'center',
                    }}
                    onPress={() => setToast({...Toast, BillingFailed: false})}>
                    <Text
                      style={{
                        backgroundColor: 'red',
                        padding: 10,
                        borderRadius: 100,
                        color: '#FFF',
                        fontWeight: 'bold',
                      }}>
                      {orderMsg.Failed}
                    </Text>
                  </TouchableOpacity>
                </Modal>
              </View>
            )}

            <View
              style={{
                width: '100%',
                height: '18%',
                flexDirection: 'row',
                backgroundColor: ModalMenu == true ? '#0d0d0d' : null,
                opacity: ModalMenu == true ? 0.9 : null,
              }}>
              <View
                style={{
                  width:
                    ViewBilling == true || ViewCart == true ? '70%' : '100%',
                  height: '100%',
                  flexDirection: 'row',
                }}>
                {/* <View
                  style={{
                    width: '50.3%',
                    height: '100%',
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    // paddingVertical: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => callWaiters()}
                    style={{
                      height: 50,
                      width: 50,
                      backgroundColor: '#e67e22',
                      borderRadius: 100,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon name="concierge-bell" color={'#FFF'} size={30}></Icon>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      backgroundColor: '#bcbcbb',
                      borderTopLeftRadius: 15,
                      borderBottomLeftRadius: 15,
                      padding: 5,
                      paddingRight: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                      height: 50,
                      left: 7,
                    }}
                    onPress={() =>
                      setViewCategory(true) +
                      setViewMenuSong(false) +
                      setViewSong(false) +
                      setViewBilling(false) +
                      setModalMenu(false) +
                      setViewCart(false) +
                      setParamsMenu('') +
                      setParamsCategory('') +
                      setSearchCondition({
                        ...SearchCondition,
                        OurMenu: false,
                      }) +
                      setMenu(1)
                    }>
                    <Image
                      source={require('../../../../assets/executive/ourmenu.png')}
                      resizeMode="contain"
                      style={{
                        height: 25,
                        width: 50,
                      }}></Image>
                    <Text
                      style={{fontSize: 18, color: '#FFF', fontWeight: 'bold'}}>
                      Our Menu
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      backgroundColor: '#bcbcbb',
                      borderTopRightRadius: 15,
                      borderBottomRightRadius: 15,
                      padding: 5,
                      paddingRight: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                      height: 50,
                    }}
                    onPress={() =>
                      setViewBilling(true) +
                      setViewMenuSong(false) +
                      setViewMenu(false) +
                      setModalMenu(false) +
                      setViewCart(false) +
                      setViewCategory(false) +
                      setCondition(2) +
                      setParamsMenu('') +
                      setParamsCategory('') +
                      setSearchCondition({
                        ...SearchCondition,
                        OurMenu: false,
                      })
                    }>
                    <Image
                      source={require('../../../../assets/executive/billing.png')}
                      resizeMode="contain"
                      style={{
                        height: 25,
                        width: 50,
                      }}></Image>
                    <Text
                      style={{fontSize: 18, color: '#FFF', fontWeight: 'bold'}}>
                      Your Billing
                    </Text>
                  </TouchableOpacity>
                </View> */}
                {ViewMenuSong == true && (
                  <View
                    style={{
                      // width: '47%',
                      width: '97%',
                      height: '100%',
                      justifyContent: 'center',
                      // paddingVertical: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 14,
                        color: '#FFF',
                        fontWeight: 'bold',
                        left: 15,
                      }}>
                      {global.playlist
                        ? 'Now Playing : ' + global.judul + ' - ' + global.artis
                        : 'Now Playing : No Song'}
                    </Text>
                    <Slider
                      style={{
                        width: '100%',
                        height: '20%',
                        transform: [{rotate: '0deg'}],
                      }}
                      minimumValue={0}
                      maximumValue={global.songduration}
                      value={global.currentposition}
                      onSlidingComplete={value => Perintah('TRACK', value)}
                      minimumTrackTintColor="white"
                      maximumTrackTintColor="#707070"
                      thumbTintColor="white"
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: 10,
                          color: '#FFF',
                          fontWeight: 'bold',
                          left: 15,
                        }}>
                        {global.currentposition !== undefined
                          ? new Date(global.currentposition)
                              .toLocaleTimeString()
                              .substring(3)
                          : '-'}
                      </Text>
                      <Text
                        style={{
                          fontSize: 10,
                          color: '#FFF',
                          fontWeight: 'bold',
                          right: 15,
                        }}>
                        {global.songduration !== undefined
                          ? new Date(global.songduration)
                              .toLocaleTimeString()
                              .substring(3)
                          : '-'}
                      </Text>
                    </View>
                  </View>
                )}
              </View>

              {ViewBilling == true && (
                <View
                  style={{
                    width: '30%',
                    height: '100%',
                    flexDirection: 'row',
                    paddingRight: 10,
                  }}>
                  <View>
                    <Text
                      style={{
                        color: '#b3b3b3',
                      }}>
                      Subtotal
                    </Text>
                    <Text
                      style={{
                        color: '#b3b3b3',
                      }}>
                      Discount
                    </Text>
                    <Text
                      style={{
                        color: '#b3b3b3',
                      }}>
                      Tax & Service Charge
                    </Text>
                    <Text
                      style={{
                        color: '#FFF',
                        fontWeight: 'bold',
                      }}>
                      Total Amount
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '45%',
                      paddingLeft: 10,
                    }}>
                    <NumericFormat
                      value={Billing.subtotal}
                      displayType="text"
                      thousandSeparator
                      prefix="Rp "
                      renderText={value => (
                        <Text
                          style={{
                            color: '#b3b3b3',
                          }}>
                          {value}
                        </Text>
                      )}
                    />
                    <NumericFormat
                      value={Billing.discount}
                      displayType="text"
                      thousandSeparator
                      prefix="Rp "
                      renderText={value => (
                        <Text
                          style={{
                            color: '#b3b3b3',
                          }}>
                          {value}
                        </Text>
                      )}
                    />
                    <NumericFormat
                      value={Billing.tax}
                      displayType="text"
                      thousandSeparator
                      prefix="Rp "
                      renderText={value => (
                        <Text
                          style={{
                            color: '#b3b3b3',
                          }}>
                          {value}
                        </Text>
                      )}
                    />
                    <NumericFormat
                      value={Billing.grandTotal}
                      displayType="text"
                      thousandSeparator
                      prefix="Rp "
                      renderText={value => (
                        <Text
                          style={{
                            color: '#FFF',
                            fontWeight: 'bold',
                          }}>
                          {value}
                        </Text>
                      )}
                    />
                  </View>
                </View>
              )}
            </View>
          </View>

          {ViewMenuSong == true && (
            <View
              style={{
                width: '30%',
                height: '100%',
                // justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '90%',
                  backgroundColor: 'white',
                  height: '82%',
                  borderRadius: 30,
                }}>
                <View
                  style={{
                    height: '15%',
                    width: '100%',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    padding: 15,
                  }}>
                  <Text
                    style={{
                      fontSize: 25,
                      color: '#707070',
                      fontWeight: 'normal',
                    }}>
                    Play List
                  </Text>

                  {/* <TouchableOpacity
                    onPress={() =>
                      SelectSong('', 'RECENTLY') +
                      setViewSong(true) +
                      setOnPlayList('Recently Song')
                    }>
                    <MaterialCommunityIcons
                      name="history"
                      color={'#707070'}
                      size={30}
                    />
                  </TouchableOpacity> */}
                </View>

                <View style={{height: '60%', width: '100%'}}>
                  {global.playlist == undefined ? (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <ActivityIndicator size="large" color="#bcbcbb" />
                      {RefreshPlaylist == 'true' && (
                        <TouchableOpacity
                          // onPress={Refresh(deviceSTB)}
                          onPress={() =>
                            Refresh(deviceSTB) +
                            setRefreshPlaylist('false') +
                            setLimitRefresh(2) +
                            setMsRefresh(5000)
                          }
                          style={{
                            backgroundColor: '#bcbcbb',
                            borderRadius: 5,
                            padding: 5,
                          }}>
                          <Text style={{color: '#fff'}}>Refresh</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  ) : (
                    <FlatList
                      data={global.playlist}
                      numColumns={0}
                      keyExtractor={(item, index) => index}
                      onEndReachedThreshold={0.5}
                      showsVerticalScrollIndicator={false}
                      initialNumToRender={10}
                      extraData={selectListSong}
                      renderItem={({item, index}) => {
                        return (
                          <PlaylistSong
                            // OnSelect={() =>
                            //   handleSelectionPlaylist(index, item.idlagu)
                            // }
                            OnSelect={() => setselectPlaylist(index)}
                            OnSelectedItem={selectPlaylist}
                            onIndex={index}
                            title={item.judul}
                            singer={item.artis}
                            ONUP={() => select_UPDOWN(index, 'UP')}
                            ONDOWN={() => select_UPDOWN(index, 'DOWN')}
                            // ONDEL={() => select_DEL(index)}
                            ONDEL={() => Perintah('REMOVED', item.idlagu)}
                            PlayCondition={changePlay}
                            OnPlay={() =>
                              OnPlayPlaylist(
                                item.idlagu,
                                item.judul,
                                item.artis,
                                item.path,
                                item.vol,
                                item.xvoc,
                                item.voc,
                              )
                            }></PlaylistSong>
                        );
                      }}
                    />
                  )}
                </View>

                <View style={{height: '25%', width: '100%'}}>
                  <View
                    style={{
                      height: '60%',
                      width: '100%',
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        height: '100%',
                        width: '25%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          height: 50,
                          width: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        // onPress={() => Perintah('BACK', 0)}
                        // onPress={() => Perintah('RELOAD', 0)}>
                        onPress={() =>
                          global.currentposition == undefined ||
                          global.currentposition == 0
                            ? null + console.log('false')
                            : Perintah('RELOAD', 0) + console.log('true')
                        }>
                        <Ionicons
                          size={40}
                          name="play-skip-back-sharp"
                          color={'#707070'}
                        />
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        height: '100%',
                        width: '25%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          height: 50,
                          width: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderRadius: 100,
                          borderWidth: 5,
                          borderColor: '#707070',
                        }}
                        onPress={
                          OnOff.MusicOnOff == false
                            ? () => Perintah('PAUSE', 0)
                            : () => Perintah('PLAY', 0)
                        }>
                        {OnOff.MusicOnOff == false ? (
                          <Ionicons
                            size={25}
                            name="pause-sharp"
                            color={'#707070'}
                          />
                        ) : (
                          <Ionicons
                            size={25}
                            name="play-sharp"
                            color={'#707070'}
                          />
                        )}
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        height: '100%',
                        width: '25%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          height: 50,
                          width: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        onPress={() => Perintah('STOP')}>
                        <View
                          style={{
                            height: 45,
                            width: 45,
                            backgroundColor: '#707070',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}></View>
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        height: '100%',
                        width: '25%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        style={{
                          height: 50,
                          width: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        onPress={() =>
                          global.playlist == undefined ||
                          global.playlist.length == 0
                            ? null
                            : Perintah('NEXT', 0)
                        }>
                        <Ionicons
                          size={40}
                          name="play-skip-forward-sharp"
                          color={'#707070'}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View
                    style={{
                      height: '40%',
                      width: '100%',
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}>
                    <View>
                      <TouchableOpacity
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          flexDirection: 'row',
                          backgroundColor: '#707070',
                          borderRadius: 15,
                          padding: 8,
                        }}
                        onPress={() => Perintah('VOCAL')}>
                        <MaterialCommunityIcons
                          size={25}
                          name="account-music-outline"
                          color={'#FFF'}
                        />

                        <Text
                          style={{
                            fontSize: 18,
                            color: '#FFF',
                            marginLeft: 5,
                          }}>
                          {OnOff.VocalOnOff == true ? 'Vocal On' : 'Vocal Off'}
                        </Text>
                      </TouchableOpacity>
                    </View>

                    {/* <TouchableOpacity
                      style={{
                        height: '100%',
                        width: '35%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() => Perintah('RELOAD', 0)}>
                      <Image
                        source={require('../../../../assets/executive/REFRESH.png')}
                        resizeMode="contain"
                        style={{height: '50%', width: '50%'}}></Image>
                    </TouchableOpacity> */}
                  </View>
                </View>
              </View>
            </View>
          )}

          {ViewCategory == true || ViewMenu == true ? (
            <View
              style={{
                width: '30%',
                height: '100%',
                // justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '90%',
                  backgroundColor: 'white',
                  height: '82%',
                  borderRadius: 30,
                }}>
                <View
                  style={{
                    flex: 0.3,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 25,
                      color: '#707070',
                      fontWeight: 'normal',
                      left: 20,
                    }}>
                    Order List
                  </Text>
                </View>

                <View
                  style={{
                    flex: 2,
                  }}>
                  {cartOrder == '' || cartOrder == undefined ? (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#707070',
                        }}>
                        No items
                      </Text>
                    </View>
                  ) : (
                    <FlatList
                      data={cartOrder}
                      numColumns={0}
                      keyExtractor={(item, index) => index}
                      renderItem={({item, index}) => {
                        return (
                          <CartList
                            onIndex={index}
                            name={item.name}
                            qty={item.qty}
                            total={Math.round(item.harga)}
                            note={item.note}
                            Delete={() => Select_Delete(index)}
                            ChildNote={{
                              note: ChangeNote.bind(this),
                            }}></CartList>
                        );
                      }}
                    />
                  )}
                </View>

                <View
                  style={{
                    flex: 0.3,
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    borderTopWidth: 1.3,
                    borderColor: '#222222',
                    paddingLeft: 10,
                    paddingRight: 10,
                    flexDirection: 'row',
                  }}>
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: '#707070',
                        fontWeight: 'bold',
                      }}>
                      Total
                    </Text>
                    <NumericFormat
                      value={CartTotal.reduce(
                        (total, value) => total + value,
                        0,
                      )}
                      displayType="text"
                      thousandSeparator
                      prefix="Rp "
                      renderText={value => (
                        <Text
                          style={{
                            fontSize: 18,
                            color: '#707070',
                            fontWeight: 'bold',
                          }}>
                          {value}
                        </Text>
                      )}
                    />
                  </View>
                  <TouchableOpacity
                    onPress={() => Order()}
                    style={{
                      // flex: 1,
                      // width: '58%',
                      // height: 50,
                      borderRadius: 10,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: '#e67e22',
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: 'white',
                        padding: 10,
                        fontWeight: 'bold',
                      }}>
                      Order Now
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <Modal
                animationType="fade"
                transparent={true}
                visible={Toast.OrderSuccess}>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    marginTop: 50,
                    alignItems: 'center',
                  }}
                  onPress={() => setToast({...Toast, OrderSuccess: false})}>
                  <Text
                    style={{
                      backgroundColor: '#e67e22',
                      padding: 10,
                      borderRadius: 100,
                      color: '#FFF',
                      fontWeight: 'bold',
                    }}>
                    Successfully ordered
                  </Text>
                </TouchableOpacity>
              </Modal>
            </View>
          ) : null}
        </View>

        <Modal
          animationType="fade"
          transparent={true}
          visible={Toast.CheckConnection}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#0d0d0d',
              opacity: 0.9,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#FFF', fontSize: 18}}>
              Check your connection !
            </Text>
          </View>
        </Modal>

        <Modal animationType="fade" transparent={true} visible={Toast.Recently}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#0d0d0d',
              opacity: 0.9,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#FFF', fontSize: 18}}>
              Room name not found !
            </Text>
          </View>
        </Modal>
      </View>
    </View>
  );
};

export default Song;
