import React, { Component, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListSonglist } from '../../../../redux/songlist/actions';
import { SonglistActionTypes } from '../../../../redux/songlist/constants';
import { View, TextInput, Text, Image, useWindowDimensions, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import ListSong from '../../../../component/listsong'
import SendSocket from '../../../../lib/websocket'
import Slider from '@react-native-community/slider';
import { Action_Command } from '../../../../redux/command/actions';
import { CommandActionTypes } from '../../../../redux/command/constants';

const Remote = ()=>{
    const windowHeight = useWindowDimensions().height;
    const [selectListSong, setselectListSong]=useState(0);
    const [IdLaguSongList, setIdLaguSongList]=React.useState(0);
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
  const [SendSTB,SetSendSTB]=useState(0);
  const command = useSelector((state) => {
      return state.Command.command;
  });
    
  const stb = useSelector((state) => {
    return state.Stb.stb;
  });

      const [InputStb,setInputStb]=useState({
        "code":"XXXX",
        "iduser": 99,
        "durasi": 4,
        "addtime": 1
        
    })

      const [Respon,SetRespon]=useState({
        "command":"command", 
        "idlagu":0, 
        "judul":"",
        "artis":"",
        "pause":0,
        "songduration":0,
        "currentposition":0,
        "vocalon":1/0,
        "key":0,
        "tempo":0,
        "playlist":[{
          "idlagu":0,
          "path":"",
          "voc":0,
          "xvoc":0,
          "judul":"",
          "artis":"",
          "volume":0
	        }]

      })
      const [Tempo,setTempo]=useState({
        "command":"TPM",
        "tempo":0,
      })
      const [Key,setKey]=useState({
        "command":"TPM",
        "key":0,
      })
      const [Trackbar,setTracbar]=useState({
        "command":"TRB",
        "position":0,
      })
      const [Vol,setVol]=useState({
        "command":"VOMS",
        "value":0,
      })
      const [Mic,setMic]=useState({
        "command":"VOMC",
        "value":0,
      })
      const [Removed,setRemoved]=useState({
        "command":"RMV",
        "idlagu":0
      })
    
     

      useEffect(() => {

        if (SendSTB==1) {
          console.log(command);
          SetSendSTB(0);
        } 

      }, [command, SendSTB]);

      const Songlist=()=>{

        navigation.replace('Song')
      
      }
      const Playlist=()=>{

        navigation.replace('Playlist')
      
      }

      const Perintah=(name,value)=>{
        let x=null;
        switch (name) {
          case 'PLAY':
              console.log('PLAY');
              // SendSocket(stb.results.devices[0].namadevices,'PL');
              x={"command":"PL"}
            break;
          case 'STOP':
              console.log('STOP');
              // SendSocket(stb.results.devices[0].namadevices,'STP');
              x={"command":"STP"}
            break;
          case 'PAUSE':
              console.log('PAUSE');
              // SendSocket(stb.results.devices[0].namadevices,'PS');
              x={"command":"PS"}
              break;
          case 'NEXT':
                console.log('NEXT');
                // SendSocket(stb.results.devices[0].namadevices,'NEXT');
                x={"command":"NEXT"}
              break;
          case 'BACK':
              console.log('BACK');
              // SendSocket(stb.results.devices[0].namadevices,'BACK');
              x={"command":"BACK"}
              break;
          case 'RELOAD':
              console.log('RELOAD');
              // SendSocket(stb.results.devices[0].namadevices,'RE');
              x={"command":"RE"}
              break;
          case 'VOL':
              console.log('VOL');
              setVol({
                ...Vol,value:parseInt(value)
              })
              // SendSocket(stb.results.devices[0].namadevices,JSON.stringify(Vol));
              x={ "command":"VOMS",
              "value":value}
              break;
          case 'MIC':
         
              // SendSocket(stb.results.devices[0].namadevices,JSON.stringify(Mic));
              x={"command":"VOMC",
              "value":value}
              break;
          case 'TEMP':
          
              // SendSocket(stb.results.devices[0].namadevices,JSON.stringify(Tempo));
              x={"command":"TPM","tempo":value}
              break;
          case 'KEY':
           
              x={"command":"KYM","key":value}
              break;
          case 'TRACKBAR':
            
              // SendSocket(stb.results.devices[0].namadevices,JSON.stringify(Trackbar));
              x={ "command":"TRB",
              "position":value}
              break;
          case 'REMOVED':
              
              // SendSocket(stb.results.devices[0].namadevices,JSON.stringify(Removed));
              x={"command":"RMV",
              "idlagu":value}
              break;
          case 'VOCAL':
              console.log('VOCAL');
      
              // SendSocket(stb.results.devices[0].namadevices,'VC');
              x={"command":"VC"}
              break;
          case 'MUTE':
              console.log('MUTE');
        
              // SendSocket(stb.results.devices[0].namadevices,'MT');
              x={"command":"MT"}
              break;
          default:
            break;
        } 

        let m={
          "meta":"send_message",
          "message":x, 
          "roomID":stb.namadevices, 
          "clientID":99
        }
        dispatch(Action_Command(CommandActionTypes.CONST_COMMAND_SEND,m))
        setTimeout(() => {

          SetSendSTB(1);
        }, 500);
      }

  
    return(

<View style={{flex:1, width:'100%', backgroundColor:'black', minHeight: Math.round(windowHeight), justifyContent:'center', alignItems:'center'}}>
                    <View style={{height:'95%', width:'95%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, alignItems:'center'}}>
                    <View style={{height:20, }}></View>
                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'90%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/people.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:13, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.nama}</Text>
                                        </View>
                                    </View>
                                    <View style={{height:'100%', width:'50%', backgroundColor:'pink', flexDirection:'row',}}>
                                        <View style={{height:'100%', width:'30%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                            <Image style={{width:'50%', height:'50%', }} resizeMode='center' source={require('../../../../assets/ubeatz/time.png')} ></Image>
                                        </View>
                                        <View style={{height:'100%', width:'70%', backgroundColor:'black', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'#FFFF', textAlign:'center', borderRadius:5,}}>{stb.jam}</Text>
                                        </View>
                                    </View>
                                </View>
                    </View>

                    <View style={{height:55,width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <View style={{height:44, width:'80%', backgroundColor:'black', borderColor:'#e93c93', borderWidth:1, 
                                    alignItems:'center', flexDirection:'row', justifyContent:'space-between',}}>
                                      <TouchableOpacity style={{height:'100%', width:'85%', color:'white',  padding:10}} onPress={()=>Songlist()}></TouchableOpacity>
                                      <TouchableOpacity onPress={()=>Songlist()} style={{height:'100%', width:'15%',  alignItems:'center', justifyContent:'center'}}>
                                      <Image style={{width:'70%', height:'70%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='center' source={require('../../../../assets/ubeatz/search.png')}></Image>
                                      </TouchableOpacity>
               
                                </View>
                    </View>



                    <View style={{height:565,width:'90%', alignItems:'center', justifyContent:'space-around',}}>

                    <View style={{height:250,width:'100%', alignItems:'center',justifyContent:'space-between',  flexDirection:'row',}}>

                        <View style={{height:'100%',width:'30%', alignItems:'center',}}>
                                <View style={{height:40, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:14, fontWeight:'bold', marginTop:5}}>Music Vol.</Text>
                                </View>
                                <View style={{height:'100%', width:'100%',  justifyContent:'center', alignItems:'center'}}>
                                <Slider
                                      style={{width: 250, height: '100%', transform:[{rotate:'-90deg'}]}}
                                      minimumValue={0}
                                      maximumValue={100}
                                      value={Respon.volume}
                                      vertical={true}
                                      onSlidingComplete={(value)=>Perintah('VOL',value)}
                                      minimumTrackTintColor="#FFFFFF"
                                      maximumTrackTintColor="#000000"
                                    />

                                </View>
                                
                        </View>

                        <View style={{height:'100%',width:'35%', alignItems:'center',justifyContent:'space-evenly', }}>
                            
                                <TouchableOpacity style={{height:150, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('VOCAL',0)}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Vocal</Text>
                                
                                {Respon.vocalon==0 && <View style={{height:20, width:30, backgroundColor:'red', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:10, fontWeight:'bold', color:'white'}}>ON</Text>
                                </View>}
                                {Respon.vocalon==1 && <View style={{height:20, width:30, backgroundColor:'green', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:10, fontWeight:'bold', color:'white'}}>OFF</Text>
                                </View>}
                                </TouchableOpacity>

                                <TouchableOpacity onPress={()=>Perintah('MUTE',0)} style={{height:50, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PLAY',0)}>
                                <Text style={{fontSize:14, fontWeight:'bold', marginTop:5}}>Mute</Text>
                                </TouchableOpacity>
                        </View>
                        <View style={{height:'100%',width:'30%', alignItems:'center',justifyContent:'space-between', }}>
                        <View style={{height:40, width:'100%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:14, fontWeight:'bold', marginTop:5}}>Mic Vol.</Text>
                                </View>
                                <View style={{height:'100%', width:'100%',  justifyContent:'center', alignItems:'center'}}>
                                <Slider
                                      style={{width: 250, height: '100%', transform:[{rotate:'-90deg'}]}}
                                      minimumValue={0}
                                      maximumValue={100}
                                      value={Respon.mic}
                                      onSlidingComplete={(value)=> Perintah('MIC',value)}
                                      vertical={true}
                                      minimumTrackTintColor="#FFFFFF"
                                      maximumTrackTintColor="#000000"
                                    />

                                </View>
                            
                        </View>
                           
                 </View>

                 <View style={{height:50,width:'100%'}} />

                    <View style={{height:60,width:'100%', alignItems:'center',justifyContent:'space-between',  flexDirection:'row',}}>
                         
                    <View style={{height:40, width:'25%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:12, fontWeight:'bold', marginTop:5}}>Tempo</Text>
                                </View>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',-3)} style={{height:15,width:30, backgroundColor:'red', marginLeft:10}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',-2)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',-1)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',0)} style={{height:15,width:30, backgroundColor:'yellow'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',1)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',2)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('TEMP',3)} style={{height:15,width:30, backgroundColor:'red'}}></TouchableOpacity>
                 </View>

                 <View style={{height:60,width:'100%', alignItems:'center',justifyContent:'space-between',  flexDirection:'row',}}>
                         
                    <View style={{height:40, width:'25%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:12, fontWeight:'bold', marginTop:5}}>Key</Text>
                                </View>
                                <TouchableOpacity onPress={()=>Perintah('KEY',-3)}  style={{height:15,width:30, backgroundColor:'red', marginLeft:10}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('KEY',-2)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('KEY',-1)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>              
                                <TouchableOpacity onPress={()=>Perintah('KEY',0)} style={{height:15,width:30, backgroundColor:'yellow'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('KEY',1)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('KEY',2)} style={{height:15,width:30, backgroundColor:'green'}}></TouchableOpacity>
                                <TouchableOpacity onPress={()=>Perintah('KEY',3)} style={{height:15,width:30, backgroundColor:'red'}}></TouchableOpacity>
                 </View>

                    <View style={{height:40,width:'100%', alignItems:'center',justifyContent:'center',}}>
                    <Slider
                          style={{width: '100%', height: 40}}
                          minimumValue={0}
                          maximumValue={Respon.songduration}
                          value={Respon.currentposition}
                          onSlidingComplete={(value)=>Perintah('TRACBAR',value)}
                          minimumTrackTintColor="#FFFFFF"
                          maximumTrackTintColor="#000000"
                        />
                         {/* <Text style={{fontSize:14, fontWeight:'bold', }}>Slider</Text>   */}
                 </View>

                    <View style={{height:40,width:'100%', alignItems:'center',justifyContent:'center', }}>
                         
                            <Text style={{fontSize:14, fontWeight:'bold', }}>{Respon.judul} - {Respon.artis}</Text>  
                    </View>


                      <View style={{height:80,width:'100%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', }}>
                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PLAY',0)}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Play</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('STOP',0)}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Stop</Text>                      
                                </TouchableOpacity>

                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('PAUSE',0)}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Pause</Text>
                                
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('NEXT',0)}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Next</Text>
                                
                                </TouchableOpacity>
                                <TouchableOpacity style={{height:69, width:'20%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Perintah('RELOAD',0)}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/play.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Reload</Text>
                                
                                </TouchableOpacity>
                              
                    </View>

                   
                    </View>

                    <View style={{height:80,width:'80%', alignItems:'center', flexDirection:'row', justifyContent:'space-between', bottom:10, position:'absolute'}}>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}} onPress={()=>Songlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/songlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Songlist</Text>
                                </TouchableOpacity>
                                <View style={{height:69, width:'33.3%',  backgroundColor:'#25B0E6', borderWidth:1,alignItems:'center', justifyContent:'center'}}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/remote.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Remote</Text>                      
                                </View>
                                <TouchableOpacity style={{height:69, width:'33.3%', backgroundColor:'black', borderColor:'#25B0E6', borderWidth:1, alignItems:'center', justifyContent:'center'}} onPress={()=>Playlist()}>
                                <Image style={{width:'50%', height:'50%', alignItems:'center', justifyContent:'center', alignItems:'center' }} resizeMode='contain' source={require('../../../../assets/ubeatz/playlist.png')}></Image>
                                <Text style={{fontSize:10, fontWeight:'bold', marginTop:5}}>Playlist</Text>
                                </TouchableOpacity>
                              
                    </View>
                    

                    </View>

                    

      {/* <View style={{height:'40%',width:'90%', justifyContent:'center', alignItems:'center' }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>App Remote</Text>
      </View>
      <View style={{height:150, width:'90%', justifyContent:'center', alignItems:'center' }}>
              <View style={{height:52, width:'60%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20, textAlign:'center'}} placeholder={'Code STB'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Insert code from STB</Text>
            </View>
            
        
       

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLoginSTB()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Login STB</Text>
      </TouchableOpacity>

      <Text style={{fontSize:12,  marginTop:30, color:'#EFEFEF'}}>----- or ------</Text>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', top:30}} onPress={()=>ClickLoginScann()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Scann</Text>
      </TouchableOpacity> */}
    </View> 
    //  </Provider>
  );
    
};



export default Remote;
