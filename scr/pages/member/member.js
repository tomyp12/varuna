import React, { Component, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListUser } from '../../redux/auth/actions';
import { Action_SetListSonglist } from '../../redux/songlist/actions';
import { UserActionTypes } from '../../redux/auth/constants';
import { View, TextInput, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Member = ()=>{
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [User,setUser]=useState({
      username:"Phone Number",
      password:"Password"
    })

    const { user, loading, variable } = useSelector((state) => ({
      user: state.User.user,
      loading: state.User.loading,
      variable: state.User.variable,
  }));

  const Shared= () =>{

    console.log('Shared');
}
    const ClickRedeem= () =>{

          navigation.push('Redeem')
      }
      
      const EditProfile= () =>{
      
       navigation.replace('EditProfile')
    }
    const ClickHomeRemote= () =>{
      
      // console.log('Lupa Password');
      navigation.push('HomeRemote')
   }


    return(
    // <Provider store={configureStore({})}>
    <View style={{height:'100%', width:'100%', alignItems:'center',  backgroundColor:'black'}}>
        <View style={{height:270,  width:'90%', justifyContent:'space-evenly', alignItems:'center', flexDirection:'column',  top:10}}>

        <View style={{height:100, width:100, backgroundColor:'#EFEFEF',  borderRadius:100}}></View>
        <Text style={{fontSize:14, fontWeight:'bold', top:10}}>{user.username}</Text>
        <Text style={{fontSize:14, fontWeight:'normal',top:0}}>{user.email}</Text>
        <Text style={{fontSize:14,  paddingLeft:5, color:'blue', textDecorationLine: 'underline'}} onPress={()=>EditProfile()}>Edit Profile</Text>
        <View style={{height:50, width:'80%', justifyContent:'space-around', alignContent:'center', flexDirection:'row', }}>
          <TouchableOpacity style={{height:50,width:'40%',  borderRadius:12, justifyContent:'center', alignItems:'center'}} >
          <Text style={{fontSize:14, fontWeight:'bold'}}>Points</Text>
          <Text style={{fontSize:12, fontWeight:'normal'}}>{user.details[0].point}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{height:50,width:'40%',  borderRadius:12, justifyContent:'center', alignItems:'center'}} >
          <Text style={{fontSize:14, fontWeight:'bold'}}>Uniqcode</Text>
          <Text style={{fontSize:12, fontWeight:'normal'}}>{user.details[0].id}</Text>
        </TouchableOpacity>
        </View>
        </View>
      

     

        <View style={{height:80, width:'80%', justifyContent:'space-around', alignContent:'center', flexDirection:'row',top:20 }}>
          <TouchableOpacity style={{height:50,width:'40%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>Shared()}>
          <Text style={{fontSize:14, fontWeight:'bold'}}>Share</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{height:50,width:'40%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickHomeRemote()}>
          <Text style={{fontSize:14, fontWeight:'bold'}}>Remote</Text>
        </TouchableOpacity>
        </View>

      

      <View style={{height:52,width:'90%', alignItems:'center', flexDirection:'row', top:10 }}>
          <Text style={{fontSize:14,  paddingLeft:5}}>My Voucher</Text>
          <TouchableOpacity style={{height:40,width:40, borderRadius:50, backgroundColor:'green', 
          borderRadius:50, justifyContent:'center', alignItems:'center', right:10, position:'absolute'}} onPress={()=>ClickRedeem()}>
              <Text style={{fontSize:20, fontWeight:'bold'}}>+</Text>
            </TouchableOpacity>

      </View>

      <View style={{height:500,width:'90%', alignItems:'center', flexDirection:'column', top:15 }}>
      
          <TouchableOpacity style={{height:112,width:'90%',  backgroundColor:'white', marginTop:10,
              borderRadius:12, justifyContent:'center', alignItems:'center', }}>
              <View style={{position:'absolute', width:161,bottom:0, right:0, alignItems:'center', backgroundColor:'#FFCBCB', height:27, borderTopStartRadius:12, justifyContent:'center' }}>
              <Text style={{fontSize:10, fontWeight:'normal', color:'#000000'}}>Voucher Code : X956KS43OA</Text>
              </View>
          </TouchableOpacity>
          <TouchableOpacity style={{height:112,width:'90%',  backgroundColor:'white', marginTop:10,
              borderRadius:12, justifyContent:'center', alignItems:'center', }}>
              <View style={{position:'absolute', width:161,bottom:0, right:0, alignItems:'center', backgroundColor:'#FFCBCB', height:27, borderTopStartRadius:12, justifyContent:'center' }}>
              <Text style={{fontSize:10, fontWeight:'normal', color:'#000000'}}>Voucher Code : X956KS43OA</Text>
              </View>
          </TouchableOpacity>
          <TouchableOpacity style={{height:112,width:'90%',  backgroundColor:'white', marginTop:10,
              borderRadius:12, justifyContent:'center', alignItems:'center', }}>
              <View style={{position:'absolute', width:161,bottom:0, right:0, alignItems:'center', backgroundColor:'#FFCBCB', height:27, borderTopStartRadius:12, justifyContent:'center' }}>
              <Text style={{fontSize:10, fontWeight:'normal', color:'#000000'}}>Voucher Code : X956KS43OA</Text>
              </View>
          </TouchableOpacity>


          

      </View>

    </View> 
    
    
    //  </Provider>
  );
    
};



export default Member;
