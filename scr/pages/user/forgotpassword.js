import React, { Component, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListUser } from '../../redux/auth/actions';
import { Action_SetListSonglist } from '../../redux/songlist/actions';
import { UserActionTypes } from '../../redux/auth/constants';
import { View, TextInput, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const ForgotPassword = ()=>{
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [User,setUser]=useState({
      username:"Phone Number",
      password:"Password"
    })

    const { user, loading, variable } = useSelector((state) => ({
      user: state.User.user,
      loading: state.User.loading,
      variable: state.User.variable,
  }));

    const Confirm=()=>{
      navigation.replace('NewPassword')
    }
      
    const Verify=()=>{
      console.log('verify');
    }
    const SendCode=()=>{
      console.log('Send Code');
    }


    return(
    <View style={{height:'100%', width:'100%', alignItems:'center', justifyContent:'center', backgroundColor:'black'}}>
      <View style={{height:60,width:'90%', }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>Forgot Password</Text>
      </View>

      <View style={{height:170,  width:'70%', justifyContent:'space-evenly', alignItems:'center', flexDirection:'row'}}>
     
        <View style={{height:52, width:'90%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
              <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20}} placeholder={'Insert your e-mail'}
              onChangeText={newText => setUser({...User, username:newText})}
              ></TextInput>
               
            </View>
            <TouchableOpacity style={{height:40,width:'20%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', left:20}} onPress={()=>Verify()}>
        <Text style={{fontSize:12, fontWeight:'bold'}}>Verify</Text>
      </TouchableOpacity>

        </View>

        <View style={{height:170,  width:'90%', justifyContent:'space-evenly', alignItems:'center', flexDirection:'column'}}>
        <Text style={{fontSize:14,  paddingLeft:5}}>Your phone number ?</Text>
        <TouchableOpacity style={{height:40,width:'20%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', left:20}} onPress={()=>SendCode()}>
        <Text style={{fontSize:12, fontWeight:'bold'}}>Send Code</Text>
      </TouchableOpacity>
        <View style={{height:52, width:'90%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
              <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20}} placeholder={'Code'}
              onChangeText={newText => setUser({...User, username:newText})}
              ></TextInput>
            </View>

        </View>
            
 
        <View style={{height:80}}></View>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>Confirm()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Confirm</Text>
      </TouchableOpacity>

     
    </View> 
    //  </Provider>
  );
    
};



export default ForgotPassword;
