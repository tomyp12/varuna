import React, { Component, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListUser } from '../../redux/auth/actions';
import { Action_SetListSonglist } from '../../redux/songlist/actions';
import { UserActionTypes } from '../../redux/auth/constants';
import { View, TextInput, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const NewPassword = ()=>{
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [User,setUser]=useState({
      username:"Phone Number",
      password:"Password"
    })

    const { user, loading, variable } = useSelector((state) => ({
      user: state.User.user,
      loading: state.User.loading,
      variable: state.User.variable,
  }));


      const SavePassword=()=>{
        navigation.replace('Login')
      }


    return(
    <View style={{height:'100%', width:'100%', alignItems:'center', justifyContent:'center', backgroundColor:'black'}}>
      <View style={{height:60,width:'90%', }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>New Password</Text>
      </View>

  

        <View style={{height:170,  width:'90%', justifyContent:'space-evenly', alignItems:'center', flexDirection:'column'}}>
        
        <View style={{height:60, width:'90%'}}>
              <View style={{height:52, width:'100%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20}} placeholder={'Password'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Password must have 8 character</Text>
            </View>
            <View style={{height:60, width:'90%'}}>
              <View style={{height:52, width:'100%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
                  <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20}} placeholder={'Confirm Password'}
                  onChangeText={newText => setUser({...User, password:newText})}
                  ></TextInput>
              </View>
              <Text style={{fontSize:12,  paddingLeft:5}}>Password must have 8 character</Text>
            </View>
        </View>
        
            
 
        <View style={{height:80}}></View>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>SavePassword()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Save</Text>
      </TouchableOpacity>

     
    </View> 
    //  </Provider>
  );
    
};



export default NewPassword;
