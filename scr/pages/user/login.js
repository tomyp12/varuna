import React, { Component, useState } from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Action_SetListUser } from '../../redux/auth/actions';
import { Action_SetListSonglist } from '../../redux/songlist/actions';
import { UserActionTypes } from '../../redux/auth/constants';
import { View, TextInput, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
// import { GoogleSignin, GoogleSigninButton } from '@react-native-google-signin/google-signin';

const Login = ()=>{
    const navigation = useNavigation(); 
    const dispatch = useDispatch();
    const [User,setUser]=useState({
      username:"Phone Number",
      password:"Password"
    })

    const { user, loading, variable } = useSelector((state) => ({
      user: state.User.user,
      loading: state.User.loading,
      variable: state.User.variable,
  }));


        const ClickLogin= () =>{
          console.log(user);
          dispatch(Action_SetListUser(UserActionTypes.CONST_SET_REQUEST_USER ,User));     
          if (loading== true ) {
            setTimeout(() => {
              navigation.replace('Member')
            }, 50);  
          }
          return  loading == false;
          

      }
      
      const ClickDaftar= () =>{
      
       console.log('daftar');
       navigation.replace('Register')
    }
    const ClickForgotPassword= () =>{
      
      // console.log('Lupa Password');
      navigation.push('ForgotPassword')
   }


    return(
    // <Provider store={configureStore({})}>
    <View style={{height:'100%', width:'100%', alignItems:'center', justifyContent:'center', backgroundColor:'black'}}>
      <View style={{height:60,width:'90%', }}>
      <Text style={{fontSize:30, fontWeight:'bold', paddingLeft:5}}>Sign In</Text>
      </View>
        <View style={{height:170,  width:'90%', justifyContent:'space-evenly', alignItems:'center', flexDirection:'column'}}>

        <View style={{height:52, width:'90%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
              <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20}} placeholder={'User Name'} 
              onChangeText={newText => setUser({...User, username:newText})}
              ></TextInput>
            </View>

            <View style={{height:52, width:'90%', backgroundColor:'#EFEFEF', opacity:0.3, borderRadius:12}}>
              <TextInput style={{height:50, width:'100%', color:'white', fontSize:20, paddingLeft:20, paddingRight:20}} placeholder={'Password'} 
              onChangeText={newText => setUser({...User, password:newText})}
              ></TextInput>
            </View>


        </View>
            
        <View style={{height:60,width:'90%', }}>
      {/* <Text style={{fontSize:12,  paddingLeft:20}}>Password must have 8 character</Text> */}
      <Text style={{fontSize:14,  right:0, position:"absolute"}} onPress={()=>ClickForgotPassword()}>Forgot Password?</Text>
      </View>

        <View style={{height:80}}></View>

      <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center'}} onPress={()=>ClickLogin()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Sign In</Text>
      </TouchableOpacity>
      {/* <TouchableOpacity style={{height:60,width:'80%', backgroundColor:'green', borderRadius:12, justifyContent:'center', alignItems:'center', marginTop:10}} onPress={()=>ClickLogin()}>
        <Text style={{fontSize:20, fontWeight:'bold'}}>Sign In Google</Text>
      </TouchableOpacity> */}
      

{/* <GoogleSigninButton
  style={{ width: 192, height: 48 }}
  size={GoogleSigninButton.Size.Wide}
  color={GoogleSigninButton.Color.Dark}
  // onPress={this._signIn}
  // disabled={this.state.isSigninInProgress}
/>; */}

      <View style={{height:80,width:'90%', justifyContent:'center', alignItems:'center', flexDirection:'row' }}>
      <Text style={{fontSize:14,  paddingLeft:5}}>Not a member? Please </Text>
      <Text style={{fontSize:14,  paddingLeft:5, color:'blue', textDecorationLine: 'underline'}} onPress={()=>ClickDaftar()}>Sign Up here</Text>

      </View>
    </View> 
    //  </Provider>
  );
    
};



export default Login;
