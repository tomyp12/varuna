import React, {createContext, useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {WebSockets} from '../lib/websocket';
import {Action_GetSTBServer} from '../redux/stb_server/actions';
import {StbServerTypes} from '../redux/stb_server/constants';
// import {Action_SetListUser} from '../redux/actions';
// import RoutersApp from '../routers/onlineUbeatz';

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState('false');
  const [userToken, setUserToken] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  const [userPhoto, setUserPhoto] = useState(null);
  const [deviceSTB, setDeviceSTB] = useState(null);
  const [deviceOutlet, setdeviceOutlet] = useState(null);

  const loginSTB = value => {
    setDeviceSTB(JSON.stringify(value));
    AsyncStorage.setItem('device', JSON.stringify(value));
  };

  const logoutSTB = async () => {
    setDeviceSTB(null);
    setdeviceOutlet(null);
    AsyncStorage.removeItem('device');
    // AsyncStorage.removeItem('Socket');
    // AsyncStorage.removeItem('Outlet');
  };

  const Outlet = value => {
    // AsyncStorage.setItem('Socket', global.Socket);
    // AsyncStorage.setItem('SongURL', global.URL_SongList);
    // AsyncStorage.setItem('DB', global.DB);
    AsyncStorage.setItem('Outlet', JSON.stringify(value));

    setdeviceOutlet(JSON.stringify(value));
  };

  const isLoggedIn = async () => {
    try {
      setIsLoading('true');

      let getUserToken = await AsyncStorage.getItem('userToken');

      let getUserInfo = await AsyncStorage.getItem('userInfo');
      getUserInfo = JSON.parse(getUserInfo);

      let getUserPhoto = await AsyncStorage.getItem('userPhoto');

      let getSTB = await AsyncStorage.getItem('device');
      // let Socket = await AsyncStorage.getItem('Socket');
      // let URL_SongList = await AsyncStorage.getItem('SongURL');
      // let DB = await AsyncStorage.getItem('DB');

      let getOutlet = await AsyncStorage.getItem('Outlet');

      // if (Socket) {
      //   global.Socket = Socket;
      //   global.URL_SongList = URL_SongList;
      //   global.DB = DB;
      // }

      setDeviceSTB(getSTB);
      setdeviceOutlet(getOutlet);
      if (getSTB !== null) {
        setTimeout(() => {
          try {
            let STB = JSON.parse(getSTB);
            let join = {
              meta: 'join_room',
              message: {command: 'join'},
              roomID: STB.namadevices,
              clientID: STB.userid,
            };

            WebSockets(join);
          } catch (err) {
            console.log('error authcontext isloggedin', err);
          }
        }, 4000);
      }

      if (getUserInfo) {
        setUserToken(getUserToken);
        setUserInfo(getUserInfo);
        setUserPhoto(getUserPhoto);
      }

      setTimeout(() => {
        // if (
        //   (global.Socket != undefined &&
        //     global.Id != undefined &&
        //     global.jaringan != undefined) ||
        //   (global.Socket != '' && global.Id != '' && global.jaringan != 'false')
        // ) {
        if (
          (global.Id != undefined && global.jaringan != undefined) ||
          (global.Id != '' && global.jaringan != 'false')
        ) {
          setIsLoading('false');
        }
      }, 2000);
    } catch (error) {
      console.log('error: ' + error);
    }
  };

  const REFRESH = value => {
    let getSTB = value;
    try {
      let STB = JSON.parse(getSTB);
      let join = {
        meta: 'join_room',
        message: {command: 'join'},
        roomID: STB.namadevices,
        clientID: STB.userid,
      };

      WebSockets(join);
    } catch (err) {
      console.log('Error Refresh', err);
    }
  };

  const RefreshPage = () => {
    dispatch(Action_GetSTBServer(StbServerTypes.CONST_SET_GET_STB_SERVER));

    setTimeout(() => {
      // if (
      //   (global.Socket != undefined &&
      //     global.Id != undefined &&
      //     global.jaringan != undefined) ||
      //   (global.Socket != '' && global.Id != '' && global.jaringan != 'false')
      // ) {
      if (
        (global.Id != undefined && global.jaringan != undefined) ||
        (global.Id != '' && global.jaringan != 'false')
      ) {
        setIsLoading('false');
      }
    }, 2000);
  };

  const isLogOutlet = async () => {
    try {
      // let Socket = await AsyncStorage.getItem('Socket');
      // let URL_SongList = await AsyncStorage.getItem('SongURL');
      // let DB = await AsyncStorage.getItem('DB');
      let getOutlet = await AsyncStorage.getItem('Outlet');

      // if (Socket) {
      //   global.Socket = Socket;
      //   global.URL_SongList = URL_SongList;
      //   global.DB = DB;
      // }

      setdeviceOutlet(getOutlet);
    } catch (error) {
      console.log('error: ' + error);
    }
  };

  useEffect(() => {
    isLoggedIn();
    isLogOutlet();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        isLoading,
        deviceSTB,
        deviceOutlet,
        loginSTB,
        logoutSTB,
        REFRESH,
        RefreshPage,
        Outlet,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
