import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import HomeRemote from '../pages/remote/offlineremote/NewExecutiveKaraoke/homeremote';
import Song from '../pages/remote/offlineremote/NewExecutiveKaraoke/song';
import Remote from '../pages/remote/offlineremote/NewExecutiveKaraoke/remote';
import Playlist from '../pages/remote/offlineremote/NewExecutiveKaraoke/playlist';

import {NavigationContainer} from '@react-navigation/native';

const Stack = createNativeStackNavigator();

const RoutersAppCode = () => {
  return (
    // <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="Song"
        component={Song}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Remote"
        component={Remote}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Playlist"
        component={Playlist}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
    // </NavigationContainer>
  );
};

export default RoutersAppCode;
