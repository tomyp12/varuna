import { createNativeStackNavigator  } from '@react-navigation/native-stack';
import React from 'react';
import  HomeRemote  from '../pages/remote/offlineremote/blank/homeremote';
import  Song  from '../pages/remote/offlineremote/blank/song';
import  Remote  from '../pages/remote/offlineremote/blank/remote';
import  Playlist  from '../pages/remote/offlineremote/blank/playlist';

import { NavigationContainer } from '@react-navigation/native';
   
const Stack = createNativeStackNavigator();

const RoutersApp = ()=> {
  return (
    <NavigationContainer>
         <Stack.Navigator>
          <Stack.Screen name="HomeRemote" component={HomeRemote} options={{ headerShown:false }} />
          <Stack.Screen name="Song" component={Song} options={{ headerShown:false }} />
          <Stack.Screen name="Remote" component={Remote} options={{ headerShown:false }} />
          <Stack.Screen name="Playlist" component={Playlist} options={{ headerShown:false }} />

     </Stack.Navigator> 
     
     </NavigationContainer>
    
  );
}

export default RoutersApp;