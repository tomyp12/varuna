import { createNativeStackNavigator  } from '@react-navigation/native-stack';
import React from 'react';
import Login  from '../pages/user/login';
import  Register  from '../pages/user/register';
import  Member  from '../pages/member/member';
import  Redeem  from '../pages/voucher/redeem';
import  HomeRemote  from '../pages/remote/onlineremote/ubeazt/homeremote';
import  Song  from '../pages/remote/onlineremote/ubeazt/song';
import  Remote  from '../pages/remote/onlineremote/ubeazt/remote';
import  Playlist  from '../pages/remote/onlineremote/ubeazt/remote';
import ForgotPassword from '../pages/user/forgotpassword'
import EditProfile from '../pages/user/editProfile'
import  NewPassword  from '../pages/user/newpassword';
import { NavigationContainer } from '@react-navigation/native';
   
const Stack = createNativeStackNavigator();

const RoutersApp = ()=> {
  return (
    <NavigationContainer>
         <Stack.Navigator>
          <Stack.Screen name="Login" component={Login} options={{ headerShown:false }} />
          <Stack.Screen name="Register" component={Register} options={{ headerShown:false }} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{ headerShown:false }} />
          <Stack.Screen name="Member" component={Member} options={{ headerShown:false }} />
          <Stack.Screen name="Redeem" component={Redeem} options={{ headerShown:false }} />
          <Stack.Screen name="HomeRemote" component={HomeRemote} options={{ headerShown:false }} />
          <Stack.Screen name="Song" component={Song} options={{ headerShown:false }} />
          <Stack.Screen name="Remote" component={Remote} options={{ headerShown:false }} />
          <Stack.Screen name="Playlist" component={Playlist} options={{ headerShown:false }} />
          <Stack.Screen name="NewPassword" component={NewPassword} options={{ headerShown:false }} />
          <Stack.Screen name="EditProfile" component={EditProfile} options={{ headerShown:false }} />
     </Stack.Navigator> 
     
     </NavigationContainer>
    
  );
}

export default RoutersApp;