import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import HomeRemote from '../pages/remote/offlineremote/NewExecutiveKaraoke/homeremote';
import Song from '../pages/remote/offlineremote/NewExecutiveKaraoke/song';
import Remote from '../pages/remote/offlineremote/NewExecutiveKaraoke/remote';
import Playlist from '../pages/remote/offlineremote/NewExecutiveKaraoke/playlist';

import {NavigationContainer} from '@react-navigation/native';

const Stack = createNativeStackNavigator();

const RoutersApp = () => {
  return (
    // <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="HomeRemote"
        component={HomeRemote}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
    // </NavigationContainer>
  );
};

export default RoutersApp;
