import React, {useContext, useEffect, useState} from 'react';
import {AuthContext} from '../context/AuthContext';

import {
  ActivityIndicator,
  View,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import RoutersAppOffLineExecutiveKaraoke from './ExecutiveKaraoke';
import RoutersAppOffLineExecutiveKaraokeCode from './ExecutiveKaraokeCode';
import {useDispatch, useSelector} from 'react-redux';

import {Action_GetSTBServer} from '../redux/stb_server/actions';
import {StbServerTypes} from '../redux/stb_server/constants';
import NetInfo from '@react-native-community/netinfo';
import config from '../../config';

const AppExecutive = () => {
  const dispatch = useDispatch();
  const {Server, loading, variable} = useSelector(state => ({
    Server: state.ServerStb.stbserver,
    loading: state.Songlist.loading,
    variable: state.Songlist.variable,
  }));
  const {isLoading, userToken, deviceSTB, RefreshPage} =
    useContext(AuthContext);
  const [Refresh_Page, setRefresh_Page] = useState('false');

  useEffect(() => {
    // dispatch(Action_GetSTBServer(StbServerTypes.CONST_SET_GET_STB_SERVER));
    NetInfo.addEventListener(state => {
      // console.log('Connection type', state.type);
      // console.log(
      //   'Is connected?',
      //   state.isConnected && state.isInternetReachable,
      // );
      global.jaringan = (
        state.isConnected && state.isInternetReachable
      ).toString();
    });

    if (global.jaringan != 'true') {
      PageRefresh('true');
    } else {
      PageRefresh('false');
    }
  }, []);

  const PageRefresh = value => {
    setTimeout(() => {
      setRefresh_Page(value);
    }, 4000);
  };

  if (
    isLoading == 'true' ||
    global.jaringan != 'true'
    // ||
    // global.Socket == undefined
  ) {
    // if (global.jaringan == 'true' && Server) {
    //   if (Server.setting) {
    //     global.Socket = `http://${Server.setting[0].socket_url}/`;
    //   }
    // }
    return (
      <View
        style={{
          height: '100%',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#222222',
        }}>
        <View
          style={{
            height: 285,
            width: '90%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            resizeMode="center"
            source={config.IMAGE_SONG}
            style={{height: '40%', width: '40%'}}></Image>
          {/* style={{height: '100%', width: '100%'}}></Image> */}
          <ActivityIndicator size="large" color="#bcbcbb" />
          {Refresh_Page == 'true' && (
            <TouchableOpacity
              onPress={() =>
                setTimeout(() => {
                  RefreshPage();
                }, 2000)
              }
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#bcbcbb',
                borderRadius: 10,
              }}>
              <Text
                style={{
                  color: '#FFF',
                  //  paddingRight: 10
                }}>
                Refresh
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  } else if (global.jaringan == 'true') {
    return (
      <NavigationContainer>
        {deviceSTB !== null ? (
          <RoutersAppOffLineExecutiveKaraokeCode />
        ) : (
          <RoutersAppOffLineExecutiveKaraoke />
          // <RoutersAppOffLineExecutiveKaraokeCode />
        )}
      </NavigationContainer>
    );
  }
};

export default AppExecutive;
