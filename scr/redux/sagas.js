// @flow
import {all} from 'redux-saga/effects';

import SagaGroup_Songlist from './songlist/saga';
import SagaGroup_User from './auth/saga';
import SagaGroup_STB from './stb/saga';
import SagaGroup_Command from './command/saga';
import SagaGroup_STBServer from './stb_server/saga';

export default function* rootSaga(): any {
  yield all([
    SagaGroup_Songlist(),
    SagaGroup_User(),
    SagaGroup_STB(),
    SagaGroup_Command(),
    SagaGroup_STBServer(),
  ]);
}
