// @flow
import {combineReducers} from 'redux';

import Songlist from './songlist/reducers';
import User from './auth/reducers';
import Stb from './stb/reducers';
import ServerStb from './stb_server/reducers';
import Command from './command/reducers';

export default (combineReducers({
  Songlist,
  User,
  Stb,
  Command,
  ServerStb,
}): any);
