
// @flow
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import {
    APICore_ListUser as ListUserApi,

} from '../../api/index';
import { APICore } from '../../api/apiCore';
import { Action_GetListUser_Error, Action_GetListUser_Success } from './actions';
import { UserActionTypes } from './constants';


const api = new APICore();



function* Saga_GetListUser({ payload: { data } }) {

    try {
        const response = yield call(ListUserApi, data);
        const datarespon = response.data;
        yield put(Action_GetListUser_Success(UserActionTypes.CONST_SET_REQUEST_USER, datarespon));
          } catch (error) {
        yield put(Action_GetListUser_Error(UserActionTypes.CONST_SET_REQUEST_USER, error));
    }
}

export function* WactSaga_GetListUser(): any {
    yield takeEvery(UserActionTypes.CONST_SET_REQUEST_USER, Saga_GetListUser);
}



function* SagaGroup_User(): any {
    yield all([
        fork(WactSaga_GetListUser),
    ]);
}

export default SagaGroup_User;
