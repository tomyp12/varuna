// @flow
import { UserActionTypes } from './constants';

type UserAction = { type: string, payload: {} | string };


export const Action_SetListUser = (actionType: string, data:any): UserAction => ({
    type: UserActionTypes.CONST_SET_REQUEST_USER,
    payload: { actionType ,data},
});
export const Action_GetListUser_Success = (actionType: string, data: any): UserAction => ({
    type: UserActionTypes.CONST_API_RESPONSE_USER_SUCCESS,
    payload: { actionType, data },
});
export const Action_GetListUser_Error = (actionType: string, error: any): UserAction => ({
    type: UserActionTypes.CONST_API_RESPONSE_USER_ERROR,
    payload: { actionType, error },
});
