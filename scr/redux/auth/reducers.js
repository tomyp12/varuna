// @flow
import { UserActionTypes } from './constants';


const INIT_STATE = {
    user:[],
    variable:'NOVITRA',
    loading: false,
    
};

type UserAction = { type: string, payload: { actionType?: string, data?: any, error?: string } };
type State = { user?: [] | null, loading?: boolean,  +value?: boolean };

const User = (state: State = INIT_STATE, action: UserAction): any => {
  
     switch (action.type) {
        case UserActionTypes.CONST_API_RESPONSE_USER_SUCCESS:
            
            switch (action.payload.actionType) {
               
                case UserActionTypes.CONST_SET_REQUEST_USER: {
                    return {
                        ...state,
                        loading: true, 
                        user: action.payload.data,
                        variable:'SUCCESS'
                    };
                }
                default:
                    return { ...state };
            }

        case UserActionTypes.CONST_API_RESPONSE_USER_ERROR:
            switch (action.payload.actionType) {
                case UserActionTypes.CONST_SET_REQUEST_USER: {
                    return {
                        ...state,
                        loading: false, 
                        user: action.payload.data,
                        variable:'ERROR'
                    };
                }
                default:
                    return { ...state };
            }

        case UserActionTypes.CONST_SET_REQUEST_USER:
                return { ...state, loading: false, variable:'PROCESS' };    
       
        default:
            return { ...state };
    }
};

export default User;
