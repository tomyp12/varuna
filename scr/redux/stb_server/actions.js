// @flow
import {StbServerTypes} from './constants';

type STBServer = {type: string, payload: {} | string};

export const Action_GetSTBServer = (
  actionType: string,
  data: any,
): STBServer => ({
  type: StbServerTypes.CONST_SET_GET_STB_SERVER,
  payload: {actionType, data},
});
export const Action_GetSTBServer_Success = (
  actionType: string,
  data: any,
): STBServer => ({
  type: StbServerTypes.CONST_API_RESPONSE_STB_SERVER_SUCCESS,
  payload: {actionType, data},
});
export const Action_GetSTBServer_Error = (
  actionType: string,
  error: any,
): STBServer => ({
  type: StbServerTypes.CONST_API_RESPONSE_STB_SERVER_ERROR,
  payload: {actionType, error},
});
