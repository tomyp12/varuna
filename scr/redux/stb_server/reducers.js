// @flow
import {StbServerTypes} from './constants';

const INIT_STATE = {
  stbserver: [],
  variable: 'NOVITRA',
  loading: false,
};

type StbServerAction = {
  type: string,
  payload: {actionType?: string, data?: any, error?: string},
};
type State = {stbserver?: [] | null, loading?: boolean, +value?: boolean};

const ServerStb = (state: State = INIT_STATE, action: StbServerAction): any => {
  switch (action.type) {
    case StbServerTypes.CONST_API_RESPONSE_STB_SERVER_SUCCESS:
      switch (action.payload.actionType) {
        case StbServerTypes.CONST_SET_GET_STB_SERVER: {
          return {
            ...state,
            loading: false,
            stbserver: action.payload.data,
            variable: 'SUCCESS',
          };
        }
        default:
          return {...state};
      }

    case StbServerTypes.CONST_API_RESPONSE_STB_SERVER_ERROR:
      switch (action.payload.actionType) {
        case StbServerTypes.CONST_SET_GET_STB_SERVER: {
          return {
            ...state,
            loading: false,
            stbserver: action.payload.data,
            variable: 'ERROR',
          };
        }
        default:
          return {...state};
      }

    case StbServerTypes.CONST_SET_GET_STB_SERVER:
      return {...state, loading: true, variable: 'PROCESS'};

    default:
      return {...state};
  }
};

export default ServerStb;
