// @flow
import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
import {APICore_STBServer as STBServer} from '../../api/index';
import {APICore} from '../../api/apiCore';
import {
  Action_GetSTBServer_Success,
  Action_GetSTBServer_Error,
} from './actions';
import {StbServerTypes} from './constants';

const api = new APICore();

function* Saga_GetSTBServer({payload: {data}}) {
  try {
    const response = yield call(STBServer, data);
    const datarespon = response.data.results.devices[0];
    // console.log('saga', response);
    yield put(
      Action_GetSTBServer_Success(
        StbServerTypes.CONST_SET_GET_STB_SERVER,
        datarespon,
      ),
    );
  } catch (error) {
    yield put(
      Action_GetSTBServer_Error(StbServerTypes.CONST_SET_GET_STB_SERVER, error),
    );
  }
}
export function* WactSaga_GetListSTBServer(): any {
  yield takeEvery(StbServerTypes.CONST_SET_GET_STB_SERVER, Saga_GetSTBServer);
}

function* SagaGroup_STBServer(): any {
  yield all([fork(WactSaga_GetListSTBServer)]);
}

export default SagaGroup_STBServer;
