// @flow
import {StbActionTypes} from './constants';

type STBAction = {type: string, payload: {} | string};

export const Action_SetSTBCode = (
  actionType: string,
  data: any,
): STBAction => ({
  type: StbActionTypes.CONST_SET_REQUEST_STB_CODE,
  payload: {actionType, data},
});
export const Action_GetListSTB_Success = (
  actionType: string,
  data: any,
): STBAction => ({
  type: StbActionTypes.CONST_API_RESPONSE_STB_SUCCESS,
  payload: {actionType, data},
});
export const Action_GetListSTB_Error = (
  actionType: string,
  error: any,
): STBAction => ({
  type: StbActionTypes.CONST_API_RESPONSE_STB_ERROR,
  payload: {actionType, error},
});
