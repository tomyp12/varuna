// @flow
import {StbActionTypes} from './constants';

const INIT_STATE = {
  stb: null,
  variable: 'NOVITRA',
  loading: false,
};

type StbAction = {
  type: string,
  payload: {actionType?: string, data?: any, error?: string},
};
// type State = { stb?:INIT_STATE, loading?: boolean, +value?: boolean };
type State = {stb?: null, loading?: boolean, +value?: boolean};

const Stb = (state = INIT_STATE, action: StbAction): any => {
  switch (action.type) {
    case StbActionTypes.CONST_API_RESPONSE_STB_SUCCESS:
      switch (action.payload.actionType) {
        case StbActionTypes.CONST_SET_REQUEST_STB_CODE:
          console.log('masuk', action.payload);
          // console.log('DATA:', action.payload.data);
          return {
            ...state,
            loading: false,
            stb: action.payload.data,
            variable: 'SUCCESS',
          };
        default:
          return {...state};
      }

    case StbActionTypes.CONST_API_RESPONSE_STB_ERROR:
      switch (action.payload.actionType) {
        case StbActionTypes.CONST_SET_REQUEST_STB_CODE: {
          console.log('ERROR', action.payload);
          return {
            ...state,
            loading: false,
            stb: action.payload.data,
            variable: 'ERROR',
          };
        }
        default:
          return {...state};
      }

    case StbActionTypes.CONST_SET_REQUEST_STB_CODE:
      return {...state, loading: true, variable: 'PROCESS'};
    default:
      return {...state};
  }
};

export default Stb;
