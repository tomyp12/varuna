// @flow
import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
import {APICore_STBCheckinCode as STBCheckinCodeAPI} from '../../api/index';
import {APICore} from '../../api/apiCore';
import {Action_GetListSTB_Success, Action_GetListSTB_Error} from './actions';
import {StbActionTypes} from './constants';

const api = new APICore();

function* Saga_GetSTBCode({payload: {data}}) {
  try {
    const response = yield call(STBCheckinCodeAPI, data);
    // console.log(response.data);
    yield put(
      Action_GetListSTB_Success(
        StbActionTypes.CONST_SET_REQUEST_STB_CODE,
        response.data.results.devices[0],
      ),
    );
  } catch (error) {
    // console.log('ERROR STB REQUEST'),
    yield put(
      Action_GetListSTB_Error(StbActionTypes.CONST_SET_REQUEST_STB_CODE, error),
    );
  }
}

export function* WactSaga_GetListSTBCode(): any {
  yield takeEvery(StbActionTypes.CONST_SET_REQUEST_STB_CODE, Saga_GetSTBCode);
}

function* SagaGroup_STB(): any {
  yield all([fork(WactSaga_GetListSTBCode)]);
}

export default SagaGroup_STB;
