// @flow
import { CommandActionTypes } from './constants';

type CommandAction = { type: string, payload: {} | string };


export const Action_Command = (actionType: string, data: any): CommandAction => ({
    type: CommandActionTypes.CONST_COMMAND_SEND,
    payload: {actionType, data},
});
export const Action_Command_Success = (actionType: string, data: any): CommandAction => ({
    type: CommandActionTypes.CONST_API_COMMAND_SUCCESS,
    payload: { actionType, data },
});
export const Action_Command_Error = (actionType: string, error: any): CommandAction => ({
    type: CommandActionTypes.CONST_API_COMMAND_ERROR,
    payload: { actionType, error },
});
