// @flow
import { CommandActionTypes } from './constants';


const INIT_STATE = {
    command:null,
    variable:'NOVITRA',
    loading: false,
    
};

type CommandAction = { type: string, payload: { actionType?: string, data?: any, error?: string } };
type State = { command?: [] | null, loading?: boolean, +value?: boolean };

const Command = (state=INIT_STATE, action: CommandAction): any => {
  
     switch (action.type) {
        case CommandActionTypes.CONST_API_COMMAND_SUCCESS:
            
            switch (action.payload.actionType) {
               
                case CommandActionTypes.CONST_COMMAND_SEND:
                console.log(action.payload.data);    
                {
                    return {
                        ...state,
                        loading: false, 
                        command: action.payload.data,
                        variable:'SUCCESS'
                    };
                }
                default:
                    return { ...state };
            }

        case CommandActionTypes.CONST_API_COMMAND_ERROR:
            switch (action.payload.actionType) {
                case CommandActionTypes.CONST_COMMAND_SEND: {
                    return {
                        ...state,
                        loading: false, 
                        command: action.payload.data,
                        variable:'ERROR'
                    };
                }
                default:
                    return { ...state };
            }


        case CommandActionTypes.CONST_COMMAND_SEND:
                return { ...state, loading: true, variable:'PROCESS' };    
       
        default:
            return { ...state };
    }
};

export default Command;
