// @flow
import { useEffect, useState } from 'react';
import { all, call, delay, fork, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { Action_Command_Success, Action_Command_Error } from './actions';
import { CommandActionTypes } from './constants';
import {
  APISocket_Command as ApiSocketCommand,

} from '../../api/index';
import { APISocket } from '../../api/apiSocket';
import { eventChannel, END } from 'redux-saga';

// // const socket = new APISocket();

// function* Saga_GetCommand({ payload: { data } }) {
//     try {
//       // console.log(data);
//         const response  = yield call(ApiSocketCommand,data);

// // setTimeout(() => {
//         console.log(response );
// // }, 1000);
      
    
//       yield put(Action_Command_Success(CommandActionTypes.CONST_COMMAND_SEND, response ));
        
//           } catch (error) {
//         yield put(Action_Command_Error(CommandActionTypes.CONST_COMMAND_SEND, error));
//     }
// }

// export function* WactSaga_GetCommand(): any {
//     // yield takeEvery(CommandActionTypes.CONST_COMMAND_SEND, Saga_GetCommand);
//     yield takeLatest(CommandActionTypes.CONST_COMMAND_SEND, Saga_GetCommand);
// }



// function* SagaGroup_Command(): any {
//     yield all([
//         fork(WactSaga_GetCommand),
//     ]);
// }

// export default SagaGroup_Command;

// import { eventChannel, END } from 'redux-saga';
// import { call, put, take, fork, cancel, cancelled } from 'redux-saga/effects';
// import { Action_Command_Success, Action_Command_Error } from './actions';
// import { CommandActionTypes } from './constants';
// import * as LiveDataActions from '../../redux/LiveData/LiveData.actions';
// import * as LiveDataTypes from '../../redux/LiveData/LiveData.types';

// Use this to actually throw exceptions, allows for easier debugging.
// const dispatch = put.resolve;

// function createWebSocketConnection(params) {
//   return new Promise((resolve, reject) => {
//     const socket = new WebSocket("http://8.215.47.1:6969/");

//     socket.onopen = function () {
//       resolve(socket);
//       socket.send(JSON.stringify(params))
//     };

//     socket.onerror = function (evt) {
//       reject(evt);
//     }
//   });
// }
// let result;

// function createSocketChannel(params) {
//     let response=null
//     const ws = new WebSocket("http://8.215.47.1:6969/");

//     if(ws){
//       ws.onerror = ws.onopen = ws.onclose =null;
//       ws.close();
//     }

//     ws.onopen = function () {
//       ws.send(JSON.stringify(params))
//     };

//     ws.onmessage = ({data}) => {
      
//       result=data
   
//     };

// }
function sleep(sec) { 
  return new Promise(resolve => setTimeout(resolve, sec*500)); 
}


function* Saga_GetCommand({ payload: {data} }) {
   
   try{
    //  const response  = yield call(createSocketChannel,data);
    // x=CommandActionTypes.CONST_COMMAND_SEND;
    const ws = new WebSocket("http://8.215.47.1:6969/");
    // const [a, seta ]=useState(data)
    // const that = this
    let results
    if(ws){
      ws.onerror = ws.onopen = ws.onclose =null;
      ws.close();
    }

    ws.onopen = function () {
      console.log(JSON.stringify(data));
      ws.send(JSON.stringify(data))
      
    };

   ws.onmessage = ({data}) => {

      results=data
      // console.log(data);

    };

    yield sleep(3)
    if (results==undefined) {
     return;
    };
  yield put(Action_Command_Success(CommandActionTypes.CONST_COMMAND_SEND, results ));
  
} catch (error) {
  yield put(Action_Command_Error(CommandActionTypes.CONST_COMMAND_SEND, error));
}
         
}

export function* WactSaga_GetCommand(): any {
    yield takeEvery(CommandActionTypes.CONST_COMMAND_SEND, Saga_GetCommand);
    // yield takeLatest(CommandActionTypes.CONST_COMMAND_SEND, Saga_GetCommand);
}



function* SagaGroup_Command(): any {
    yield all([
        fork(WactSaga_GetCommand),
    ]);
}

export default SagaGroup_Command;

// function* listenForSocketMessages({ payload: { data } }) {
//   let socket;
//   let socketChannel;

//   try {
//     socket        = yield call(createWebSocketConnection(data));
//     socketChannel = yield call(createSocketChannel, socket);

//     // tell the application that we have a connection
//     // yield dispatch(LiveDataActions.connectionSuccess());

//     while (true) {
//       // wait for a message from the channel
//       const payload = yield take(socketChannel);

//       // a message has been received, dispatch an action with the message payload
//       // yield dispatch(LiveDataActions.incomingEvent(payload));
//       yield put(Action_Command_Success(CommandActionTypes.CONST_COMMAND_SEND, payload ));
//     }
//   } catch (error) {
//     yield put(Action_Command_Error(CommandActionTypes.CONST_COMMAND_SEND, error));
//   } finally {
//     if (yield cancelled()) {
//       // close the channel
//       socketChannel.close();

//       // close the WebSocket connection
//       socket.close();
//     } else {
//       yield put(Action_Command_Error(CommandActionTypes.CONST_COMMAND_SEND, error));
//     }
//   }
// }

// export function* SagaGroup_Command() {
//   yield all([
//             fork(listenForSocketMessages),
//         ]);
// }

// // function* SagaGroup_Command(): any {
// //     yield all([
// //         fork(WactSaga_GetCommand),
// //     ]);
// // }

// // export default SagaGroup_Command;