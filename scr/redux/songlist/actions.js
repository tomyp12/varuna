// @flow
import {SonglistActionTypes} from './constants';

type SonglistAction = {type: string, payload: {} | string};

export const Action_SetListSonglist = (
  actionType: string,
  data: any,
): SonglistAction => ({
  type: SonglistActionTypes.CONST_SET_GET_SONGLIST,
  payload: {actionType, data},
});
export const Action_GetListSonglist_Success = (
  actionType: string,
  data: any,
): SonglistAction => ({
  type: SonglistActionTypes.CONST_API_RESPONSE_SONGLIST_SUCCESS,
  payload: {actionType, data},
});
export const Action_GetListSonglist_Error = (
  actionType: string,
  error: any,
): SonglistAction => ({
  type: SonglistActionTypes.CONST_API_RESPONSE_SONGLIST_ERROR,
  payload: {actionType, error},
});
