// @flow
import {SonglistActionTypes} from './constants';

const INIT_STATE = {
  songlist: [],
  variable: 'NOVITRA',
  loading: false,
};

type SonglistAction = {
  type: string,
  payload: {actionType?: string, data?: any, error?: string},
};
type State = {songlist?: [] | null, loading?: boolean, +value?: boolean};

const Songlist = (state: State = INIT_STATE, action: SonglistAction): any => {
  switch (action.type) {
    case SonglistActionTypes.CONST_API_RESPONSE_SONGLIST_SUCCESS:
      switch (action.payload.actionType) {
        case SonglistActionTypes.CONST_SET_GET_SONGLIST: {
          return {
            ...state,
            loading: false,
            songlist: action.payload.data,
            variable: 'SUCCESS',
          };
        }
        default:
          return {...state};
      }

    case SonglistActionTypes.CONST_API_RESPONSE_SONGLIST_ERROR:
      switch (action.payload.actionType) {
        case SonglistActionTypes.CONST_SET_GET_SONGLIST: {
          return {
            ...state,
            loading: false,
            songlist: action.payload.data,
            variable: 'ERROR',
          };
        }
        default:
          return {...state};
      }

    case SonglistActionTypes.CONST_SET_GET_SONGLIST:
      return {...state, loading: true, variable: 'PROCESS'};

    default:
      return {...state};
  }
};

export default Songlist;
