// @flow
import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
import {APICore_ListSonglist as ListSonglistApi} from '../../api/index';
import {APICore} from '../../api/apiCore';
import {
  Action_GetListSonglist_Error,
  Action_GetListSonglist_Success,
} from './actions';
import {SonglistActionTypes} from './constants';

const api = new APICore();

function* Saga_GetListSonglist({payload: {data}}) {
  try {
    const response = yield call(ListSonglistApi, data);
    const datarespon = response.data;

    yield put(
      Action_GetListSonglist_Success(
        SonglistActionTypes.CONST_SET_GET_SONGLIST,
        datarespon,
      ),
    );
  } catch (error) {
    yield put(
      Action_GetListSonglist_Error(
        SonglistActionTypes.CONST_SET_GET_SONGLIST,
        error,
      ),
    );
  }
}

export function* WactSaga_GetListSonglist(): any {
  yield takeEvery(
    SonglistActionTypes.CONST_SET_GET_SONGLIST,
    Saga_GetListSonglist,
  );
}

function* SagaGroup_Songlist(): any {
  yield all([fork(WactSaga_GetListSonglist)]);
}

export default SagaGroup_Songlist;
