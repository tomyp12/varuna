import {PixelRatio, Dimensions, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');

const guidelineBaseWidth = 375;
const guidelineBaseHeight = 812;

const horizontalScale = size => (width / guidelineBaseWidth) * size;
const verticalScale = size => (height / guidelineBaseHeight) * size;
const screenSize = Math.sqrt(width * height) / 100;

const moderateScale = (size, factor = 0.5) =>
  size + (horizontalScale(size) - size) * factor;
const imageScale = (size, factor = -1.5) =>
  size + (horizontalScale(size) - size) * factor;

// based on iphone 5s's scale
const widthScale = width / 320;
const heightScale = height / 568;
const normalize = size => {
  const newSize = size * widthScale;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
};

export {horizontalScale, verticalScale, moderateScale, imageScale, normalize};
