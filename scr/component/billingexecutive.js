import React from 'react';
import {Image, TouchableOpacity, View, Text} from 'react-native';
import {NumericFormat} from 'react-number-format';

const BillingList = props => {
  let x = props.onIndex % 2;

  if (x == 0) {
    return (
      <View
        style={{
          width: '100%',
          flexDirection: 'row',

          backgroundColor: '#bcbcbb',
          paddingRight: 10,
          paddingLeft: 10,
        }}>
        <View
          style={{
            flex: 2,
            alignSelf: 'stretch',
          }}>
          <Text
            style={{
              padding: 10,
              fontSize: 20,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.onIndex + 1 + '. ' + props.name}
          </Text>
        </View>
        <View
          style={{
            flex: 0.5,
            alignSelf: 'stretch',
          }}>
          <Text
            style={{
              padding: 10,
              fontSize: 20,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.qty}
          </Text>
        </View>
        {/* <View
                        style={{
                          flex: 1,
                          alignSelf: 'stretch',
                        }}>
                        <Text
                          style={{
                            padding: 10,
                            color: '#b3b3b3',
                          }}>
                          Notes
                        </Text>
                      </View> */}
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
          }}>
          <NumericFormat
            value={props.total}
            displayType="text"
            thousandSeparator
            prefix="Rp "
            renderText={value => (
              <Text
                style={{
                  padding: 10,
                  fontSize: 20,
                  color: '#707070',
                  fontWeight: 'bold',
                }}>
                {value}
              </Text>
            )}
          />
        </View>
      </View>
    );
  } else {
    return (
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          backgroundColor: '#dbd7d7',
          paddingRight: 10,
          paddingLeft: 10,
        }}>
        <View
          style={{
            flex: 2,
            alignSelf: 'stretch',
          }}>
          <Text
            style={{
              padding: 10,
              fontSize: 20,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.onIndex + 1 + '. ' + props.name}
          </Text>
        </View>
        <View
          style={{
            flex: 0.5,
            alignSelf: 'stretch',
          }}>
          <Text
            style={{
              padding: 10,
              fontSize: 20,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.qty}
          </Text>
        </View>
        {/* <View
                        style={{
                          flex: 1,
                          alignSelf: 'stretch',
                        }}>
                        <Text
                          style={{
                            padding: 10,
                            color: '#b3b3b3',
                          }}>
                          Notes
                        </Text>
                      </View> */}
        <View
          style={{
            flex: 1,
            alignSelf: 'stretch',
          }}>
          <NumericFormat
            value={props.total}
            displayType="text"
            thousandSeparator
            prefix="Rp "
            renderText={value => (
              <Text
                style={{
                  padding: 10,
                  fontSize: 20,
                  color: '#707070',
                  fontWeight: 'bold',
                }}>
                {value}
              </Text>
            )}
          />
        </View>
      </View>
    );
  }
};
export default BillingList;
