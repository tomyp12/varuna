import React, {useState} from 'react';
import {Image, TouchableOpacity, View, Text, Modal} from 'react-native';
import Popover from 'react-native-popover-view';
import Ionicons from 'react-native-vector-icons/Ionicons';

const PlaylistSong = props => {
  let x = props.onIndex % 2;

  const [modalSongList, setModalSongList] = useState(false);

  if (global.playlist !== undefined) {
    if (global.playlist.length !== 1) {
      lengthData = global.playlist.length - 1;
    } else {
      lengthData = 'onlyOne';
    }
  } else {
    lengthData = undefined;
  }

  // if (props.OnSelectedItem == props.onIndex) {
  //   return (
  //     <View
  //       style={{
  //         height: 60,
  //         width: '100%',
  //         flexDirection: 'row',
  //         alignItems: 'center',
  //         justifyContent: 'center',
  //         backgroundColor: '#707070',
  //       }}>
  //       <TouchableOpacity
  //         onPress={props.OnSelect}
  //         style={{
  //           height: 55,
  //           width: '88%',
  //           alignItems: 'center',
  //           flexDirection: 'row',
  //         }}>
  //         <Text
  //           style={{
  //             fontSize: 13,
  //             color: '#FFF',
  //             fontWeight: 'bold',
  //           }}>
  //           {props.onIndex + 1}
  //           {'. '}
  //         </Text>
  //         <Text
  //           style={{
  //             fontSize: 13,
  //             color: '#FFF',
  //             left: 5,
  //             fontWeight: 'bold',
  //             width: '90%',
  //           }}>
  //           {props.title + ' - ' + props.singer}
  //         </Text>
  //       </TouchableOpacity>

  //       <Popover
  //         isVisible={modalSongList}
  //         onRequestClose={() => setModalSongList(false)}
  //         popoverStyle={{
  //           backgroundColor: '#303030',
  //         }}
  //         from={
  //           <TouchableOpacity
  //             style={{
  //               height: '100%',
  //               width: '10%',
  //               justifyContent: 'center',
  //               alignItems: 'center',
  //             }}
  //             onPress={() => setModalSongList(true)}>
  //             <Image
  //               source={require('../assets/ubeatz/menu.png')}
  //               style={{
  //                 width: 20,
  //                 height: 20,
  //                 transform: [{rotate: '180deg'}],
  //               }}
  //             />
  //           </TouchableOpacity>
  //         }>
  //         <>
  //           <TouchableOpacity
  //             onPress={() => {
  //               props.OnPlay();
  //               setModalSongList(false);
  //               props.PlayCondition(true);
  //             }}
  //             style={{
  //               padding: 10,
  //               flexDirection: 'row',
  //               alignItems: 'center',
  //             }}>
  //             <Image
  //               source={require('../assets/executive/up.png')}
  //               resizeMode="contain"
  //               style={{height: 25, width: 25, transform: [{rotate: '90deg'}]}}
  //             />
  //             <Text
  //               style={{
  //                 color: '#FFFFFF',
  //                 fontSize: 18,
  //                 fontWeight: '700',
  //                 paddingLeft: 10,
  //               }}>
  //               Play
  //             </Text>
  //           </TouchableOpacity>

  //           {props.onIndex !== 0 ? (
  //             <TouchableOpacity
  //               onPress={() => {
  //                 props.ONUP();
  //                 setModalSongList(false);
  //               }}
  //               style={{
  //                 padding: 10,
  //                 flexDirection: 'row',
  //                 alignItems: 'center',
  //               }}>
  //               <Image
  //                 source={require('../assets/executive/up.png')}
  //                 resizeMode="contain"
  //                 style={{height: 25, width: 25}}
  //               />
  //               <Text
  //                 style={{
  //                   color: '#FFFFFF',
  //                   fontSize: 18,
  //                   fontWeight: '700',
  //                   paddingLeft: 10,
  //                 }}>
  //                 Priority Up
  //               </Text>
  //             </TouchableOpacity>
  //           ) : null}

  //           {lengthData !== props.onIndex && lengthData !== 'onlyOne' ? (
  //             <TouchableOpacity
  //               onPress={() => {
  //                 props.ONDOWN();
  //                 setModalSongList(false);
  //               }}
  //               style={{
  //                 padding: 10,
  //                 flexDirection: 'row',
  //                 alignItems: 'center',
  //               }}>
  //               <Image
  //                 source={require('../assets/executive/down.png')}
  //                 resizeMode="contain"
  //                 style={{height: 25, width: 25}}
  //               />
  //               <Text
  //                 style={{
  //                   color: '#FFFFFF',
  //                   fontSize: 18,
  //                   fontWeight: '700',
  //                   paddingLeft: 10,
  //                 }}>
  //                 Priority Down
  //               </Text>
  //             </TouchableOpacity>
  //           ) : null}
  //           <TouchableOpacity
  //             onPress={() => {
  //               props.ONDEL();
  //               setModalSongList(false);
  //             }}
  //             style={{
  //               padding: 10,
  //               flexDirection: 'row',
  //               alignItems: 'center',
  //             }}>
  //             <Image
  //               source={require('../assets/executive/trash.png')}
  //               resizeMode="contain"
  //               style={{height: 25, width: 25}}
  //             />
  //             <Text
  //               style={{
  //                 color: '#FFFFFF',
  //                 fontSize: 18,
  //                 fontWeight: '700',
  //                 paddingLeft: 10,
  //               }}>
  //               Delete
  //             </Text>
  //           </TouchableOpacity>
  //         </>
  //       </Popover>
  //     </View>
  //   );
  // } else {
  if (x == 0) {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#bcbcbb',
          paddingHorizontal: 5,
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{
            flex: 1,
            height: 55,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text
            style={{
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
              // left: 5,
            }}>
            {props.onIndex + 1}
            {'. '}
          </Text>
          <Text
            style={{
              fontSize: 13,
              color: '#707070',
              left: 5,
              fontWeight: 'bold',
              width: '90%',
            }}>
            {props.title + ' - ' + props.singer}
          </Text>
        </TouchableOpacity>

        <Popover
          isVisible={modalSongList}
          onRequestClose={() => setModalSongList(false)}
          popoverStyle={{
            backgroundColor: '#303030',
          }}
          from={
            <TouchableOpacity
              style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => setModalSongList(true)}>
              <Ionicons size={25} name="ellipsis-vertical" color={'#FFF'} />
            </TouchableOpacity>
          }>
          <>
            <TouchableOpacity
              onPress={() => {
                props.OnPlay();
                setModalSongList(false);
                props.PlayCondition(true);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/up.png')}
                resizeMode="contain"
                style={{
                  height: 25,
                  width: 25,
                  transform: [{rotate: '90deg'}],
                }}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Play
              </Text>
            </TouchableOpacity>

            {props.onIndex !== 0 ? (
              <TouchableOpacity
                onPress={() => {
                  props.ONUP();
                  setModalSongList(false);
                }}
                style={{
                  padding: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../assets/executive/up.png')}
                  resizeMode="contain"
                  style={{height: 25, width: 25}}
                />
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontSize: 18,
                    fontWeight: '700',
                    paddingLeft: 10,
                  }}>
                  Priority Up
                </Text>
              </TouchableOpacity>
            ) : null}

            {lengthData !== props.onIndex && lengthData !== 'onlyOne' ? (
              <TouchableOpacity
                onPress={() => {
                  props.ONDOWN();
                  setModalSongList(false);
                }}
                style={{
                  padding: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../assets/executive/down.png')}
                  resizeMode="contain"
                  style={{height: 25, width: 25}}
                />
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontSize: 18,
                    fontWeight: '700',
                    paddingLeft: 10,
                  }}>
                  Down
                </Text>
              </TouchableOpacity>
            ) : null}
            <TouchableOpacity
              onPress={() => {
                props.ONDEL();
                setModalSongList(false);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/trash.png')}
                resizeMode="contain"
                style={{height: 25, width: 25}}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Delete
              </Text>
            </TouchableOpacity>
          </>
        </Popover>
      </View>
    );
  } else {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#dbd7d7',
          paddingHorizontal: 5,
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{
            flex: 1,
            height: 55,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text
            style={{
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
              // left: 5,
            }}>
            {props.onIndex + 1}
            {'. '}
          </Text>
          <Text
            style={{
              fontSize: 13,
              color: '#707070',
              left: 5,
              fontWeight: 'bold',
              width: '90%',
            }}>
            {props.title + ' - ' + props.singer}
          </Text>
        </TouchableOpacity>

        <Popover
          isVisible={modalSongList}
          onRequestClose={() => setModalSongList(false)}
          popoverStyle={{
            backgroundColor: '#303030',
          }}
          from={
            <TouchableOpacity
              style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => setModalSongList(true)}>
              <Ionicons size={25} name="ellipsis-vertical" color={'#FFF'} />
            </TouchableOpacity>
          }>
          <>
            <TouchableOpacity
              onPress={() => {
                props.OnPlay();
                setModalSongList(false);
                props.PlayCondition(true);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/up.png')}
                resizeMode="contain"
                style={{
                  height: 25,
                  width: 25,
                  transform: [{rotate: '90deg'}],
                }}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Play
              </Text>
            </TouchableOpacity>

            {props.onIndex !== 0 ? (
              <TouchableOpacity
                onPress={() => {
                  props.ONUP();
                  setModalSongList(false);
                }}
                style={{
                  padding: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../assets/executive/up.png')}
                  resizeMode="contain"
                  style={{height: 25, width: 25}}
                />
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontSize: 18,
                    fontWeight: '700',
                    paddingLeft: 10,
                  }}>
                  Priority Up
                </Text>
              </TouchableOpacity>
            ) : null}

            {lengthData !== props.onIndex && lengthData !== 'onlyOne' ? (
              <TouchableOpacity
                onPress={() => {
                  props.ONDOWN();
                  setModalSongList(false);
                }}
                style={{
                  padding: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../assets/executive/down.png')}
                  resizeMode="contain"
                  style={{height: 25, width: 25}}
                />
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontSize: 18,
                    fontWeight: '700',
                    paddingLeft: 10,
                  }}>
                  Priority Down
                </Text>
              </TouchableOpacity>
            ) : null}
            <TouchableOpacity
              onPress={() => {
                props.ONDEL();
                setModalSongList(false);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/trash.png')}
                resizeMode="contain"
                style={{height: 25, width: 25}}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Delete
              </Text>
            </TouchableOpacity>
          </>
        </Popover>
      </View>
    );
  }
  // }
};
export default PlaylistSong;
