import React from 'react';
import {Image, TouchableOpacity, View, Text} from 'react-native';

const PlaylistSong = props => {
  if (props.OnSelectedItem.selectedItem == props.onIndex) {
    return (
      <View
        style={{
          height: 60,
          width: '99%',
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: '#1a1a1a',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{height: 55, width: '88%', justifyContent: 'center'}}>
          <Text style={{fontSize: 13, color: '#e93c93', left: 5}}>
            {props.title}
          </Text>
          <Text style={{fontSize: 14, color: '#FFFF', left: 5}}>
            {props.singer}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnPlay}
          style={{
            height: '100%',
            width: '12%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#e93c93',
              width: '100%',
              alignItems: 'center',
              borderRadius: 50,
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 9}}>play</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View
        style={{
          height: 60,
          width: '99%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{height: 55, width: '88%', justifyContent: 'center'}}>
          <Text style={{fontSize: 13, color: '#e93c93', left: 5}}>
            {props.title}
          </Text>
          <Text style={{fontSize: 14, color: '#FFFF', left: 5}}>
            {props.singer}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnPlay}
          style={{
            height: '100%',
            width: '12%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#e93c93',
              width: '100%',
              alignItems: 'center',
              borderRadius: 50,
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 9}}>play</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
};
export default PlaylistSong;
