import React, { useState } from 'react';
import { Image, TouchableOpacity, View, Text } from 'react-native';




const buttonEff=(props)=>{

    const [Eff, setEff] =useState(false);

    return(
        <TouchableOpacity  style={props.STYLES}
              onPress={props.ONPRESS} 
              onPressIn={()=> setEff(true)
              }
              onPressOut={()=> setEff(false)
              }
              activeOpacity={1}>



                    {Eff==true && 
                                <View style={{height:'100%', width:'100%',  opacity:0.8, 
                                backgroundColor:'black', justifyContent:'center', alignItems:'center', }}>

                                    <Text style={{color:'white', fontSize:14, fontWeight:'bold'}}>{props.ONTEXT}</Text>
                            
                                </View>
                                }
                    {Eff==false && 
                                    <View style={{height:'100%', width:'100%',justifyContent:'center', alignItems:'center', }}>
                                        <Text style={{color:'white', fontSize:14, fontWeight:'bold'}}>{props.ONTEXT}</Text>
                                    </View>
                  }
          </TouchableOpacity>
    )
   
}
export default buttonEff;