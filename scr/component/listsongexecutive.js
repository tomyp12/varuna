import React from 'react';
import {Image, TouchableOpacity, View, Text} from 'react-native';

const ListSong = props => {
  let x = props.onIndex % 2;

  // if (props.OnSelectedItem == props.onIndex) {
  //   return (
  //     <View
  //       style={{
  //         height: 60,
  //         width: '100%',
  //         flexDirection: 'row',
  //         alignItems: 'center',
  //         justifyContent: 'center',
  //         backgroundColor: '#707070',
  //       }}>
  //       <TouchableOpacity
  //         onPress={props.OnSelect}
  //         style={{
  //           height: 55,
  //           width: '80%',
  //           justifyContent: 'center',
  //           flexDirection: 'row',
  //         }}>
  //         <View
  //           style={{height: '100%', width: '50%', justifyContent: 'center'}}>
  //           <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
  //             {props.title}
  //           </Text>
  //         </View>
  //         <View
  //           style={{height: '100%', width: '50%', justifyContent: 'center'}}>
  //           <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
  //             {props.singer}
  //           </Text>
  //         </View>
  //       </TouchableOpacity>
  //       <TouchableOpacity
  //         onPress={props.OnSave}
  //         style={{
  //           height: 60,
  //           width: '8%',
  //           justifyContent: 'center',
  //           alignItems: 'center',
  //         }}>
  //         <View
  //           style={{
  //             height: 60,
  //             width: '100%',
  //             alignItems: 'center',
  //             justifyContent: 'center',
  //           }}>
  //           <Text style={{color: '#FFFF', fontSize: 20, fontWeight: 'bold'}}>
  //             + ADD
  //           </Text>
  //         </View>
  //       </TouchableOpacity>
  //     </View>
  //   );
  // } else {
  if (x == 0) {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#bcbcbb',
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{
            height: 55,
            width: '80%',
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <View
            style={{
              height: '100%',
              width: '50%',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#707070', fontWeight: 'bold'}}>
              {props.title}
            </Text>
          </View>
          <View
            style={{
              height: '100%',
              width: '50%',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#707070', fontWeight: 'bold'}}>
              {props.singer}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnSave}
          style={{
            height: 60,
            width: '8%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 60,
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 20, fontWeight: 'bold'}}>
              + ADD
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#dbd7d7',
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{
            height: 55,
            width: '80%',
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <View
            style={{height: '100%', width: '50%', justifyContent: 'center'}}>
            <Text style={{fontSize: 20, color: '#707070', fontWeight: 'bold'}}>
              {props.title}
            </Text>
          </View>
          <View
            style={{height: '100%', width: '50%', justifyContent: 'center'}}>
            <Text style={{fontSize: 20, color: '#707070', fontWeight: 'bold'}}>
              {props.singer}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnSave}
          style={{
            height: 60,
            width: '8%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 60,
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 20, fontWeight: 'bold'}}>
              + ADD
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  // }
};
export default ListSong;
