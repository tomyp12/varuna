import React from 'react';
import {Image, TouchableOpacity, View, Text, TextInput} from 'react-native';
import {NumericFormat} from 'react-number-format';

const CategoryList = props => {
  let x = props.onIndex % 2;

  // if (x == 0) {
  return (
    <View
      style={{
        width: '33%',
        // flexDirection: 'row',
        // backgroundColor: 'white',
        padding: 10,
      }}>
      <TouchableOpacity
        onPress={props.OnSelect}
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
          borderRadius: 10,
          padding: 10,
        }}>
        <Text
          style={{
            padding: 10,
            fontSize: 20,
            // color: '#707070',
            color: 'black',
            fontWeight: 'bold',
          }}>
          {props.name}
        </Text>
      </TouchableOpacity>
    </View>
  );
  // }
  // else {
  //   return (
  //     <View
  //       style={{
  //         width: '33%',
  //         flexDirection: 'row',
  //         backgroundColor: '#dbd7d7',
  //         paddingRight: 10,
  //         paddingLeft: 10,
  //       }}>
  //       <TouchableOpacity
  //         onPress={props.OnSelect}
  //         style={{
  //           flex: 1,
  //           alignItems: 'center',
  //           justifyContent: 'center',
  //           padding: 10,
  //         }}>
  //         <Text
  //           style={{
  //             padding: 10,
  //             fontSize: 20,
  //             color: '#707070',
  //             fontWeight: 'bold',
  //           }}>
  //           {props.name}
  //         </Text>
  //       </TouchableOpacity>
  //     </View>
  //   );
  // }
};
export default CategoryList;
