import React from 'react';
import {Image, TouchableOpacity, View, Text} from 'react-native';
import {NumericFormat} from 'react-number-format';

const OurMenu = props => {
  let x = props.onIndex % 2;

  if (x == 0) {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          paddingLeft: 10,
          paddingRight: 10,
          backgroundColor: '#bcbcbb',
        }}>
        <View
          style={{
            flex: 1,
            height: 60,
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <View
            style={{
              height: '100%',
              width: '50%',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#707070', fontWeight: 'bold'}}>
              {props.name}
            </Text>
            <Text style={{fontSize: 15, color: '#707070', fontWeight: 'bold'}}>
              {props.deskripsi}
            </Text>
          </View>
          <View
            style={{
              height: '100%',
              width: '50%',
              justifyContent: 'center',
            }}>
            <NumericFormat
              value={props.harga}
              displayType="text"
              thousandSeparator
              prefix="Rp "
              renderText={value => (
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    padding: 10,
                    color: '#707070',
                  }}>
                  {value}
                </Text>
              )}
            />
          </View>
        </View>

        <TouchableOpacity
          onPress={props.OnSave}
          style={{
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 60,
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 20, fontWeight: 'bold'}}>
              + ADD
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          paddingLeft: 10,
          paddingRight: 10,
          backgroundColor: '#dbd7d7',
        }}>
        <View
          style={{
            flex: 1,
            height: 60,
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <View
            style={{
              height: '100%',
              width: '50%',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#707070', fontWeight: 'bold'}}>
              {props.name}
            </Text>
            <Text style={{fontSize: 15, color: '#707070', fontWeight: 'bold'}}>
              {props.deskripsi}
            </Text>
          </View>
          <View
            style={{
              height: '100%',
              width: '50%',
              justifyContent: 'center',
            }}>
            <NumericFormat
              value={props.harga}
              displayType="text"
              thousandSeparator
              prefix="Rp "
              renderText={value => (
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    padding: 10,
                    color: '#707070',
                  }}>
                  {value}
                </Text>
              )}
            />
          </View>
        </View>

        <TouchableOpacity
          onPress={props.OnSave}
          style={{
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 60,
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 20, fontWeight: 'bold'}}>
              + ADD
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
};
export default OurMenu;
