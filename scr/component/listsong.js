import React from 'react';
import {Image, TouchableOpacity, View, Text} from 'react-native';

const ListSong = props => {
  if (props.OnSelectedItem.selectedItem == props.onIndex) {
    return (
      <View
        style={{
          height: 60,
          width: '99%',
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: '#1a1a1a',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{height: 55, width: '76%', justifyContent: 'center'}}>
          <Text style={{fontSize: 13, color: '#e93c93', left: 5}}>
            {props.title}
          </Text>
          <Text style={{fontSize: 14, color: '#FFFF', left: 5}}>
            {props.singer}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnPlay}
          style={{
            height: '100%',
            width: '12%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#e93c93',
              width: '100%',
              alignItems: 'center',
              borderRadius: 50,
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 9}}>play</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnSave}
          style={{
            height: '100%',
            width: '12%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: '70%',
              height: '70%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            resizeMode="center"
            source={require('../assets/icon/save.png')}></Image>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View
        style={{
          height: 60,
          width: '99%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={props.OnSelect}
          style={{height: 55, width: '76%', justifyContent: 'center'}}>
          <Text style={{fontSize: 13, color: '#e93c93', left: 5}}>
            {props.title}
          </Text>
          <Text style={{fontSize: 14, color: '#FFFF', left: 5}}>
            {props.singer}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnPlay}
          style={{
            height: '100%',
            width: '12%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#e93c93',
              width: '100%',
              alignItems: 'center',
              borderRadius: 50,
              justifyContent: 'center',
            }}>
            <Text style={{color: '#FFFF', fontSize: 9}}>play</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={props.OnSave}
          style={{
            height: '100%',
            width: '12%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: '70%',
              height: '70%',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            resizeMode="center"
            source={require('../assets/icon/save.png')}></Image>
        </TouchableOpacity>
      </View>
    );
  }
};
export default ListSong;
