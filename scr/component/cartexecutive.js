import React, {useState} from 'react';
import {Image, TouchableOpacity, View, Text, Modal} from 'react-native';
import Popover from 'react-native-popover-view';
import {NumericFormat} from 'react-number-format';
import Ionicons from 'react-native-vector-icons/Ionicons';

const CartList = props => {
  let x = props.onIndex % 2;

  const [modalCartList, setmodalCartList] = useState(false);

  if (x == 0) {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#bcbcbb',
          paddingLeft: 5,
          paddingRight: 5,
        }}>
        <View
          style={{
            height: 55,
            width: '100%',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text
            style={{
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.onIndex + 1}
            {'. '}
          </Text>
          <Text
            style={{
              flex: 1,
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.name}
          </Text>
          <Text
            style={{
              flex: 0.2,
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.qty}x
          </Text>
          <NumericFormat
            value={props.total}
            displayType="text"
            thousandSeparator
            prefix="Rp "
            renderText={value => (
              <Text
                style={{
                  flex: 0.5,
                  fontSize: 13,
                  fontWeight: 'bold',
                  color: '#707070',
                }}>
                {value}
              </Text>
            )}
          />

          <TouchableOpacity
            onPress={() => {
              props.Delete();
            }}
            style={{
              padding: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Ionicons name="trash-sharp" color={'#707070'} size={25} />
          </TouchableOpacity>
        </View>

        {/* <Popover
          isVisible={modalCartList}
          onRequestClose={() => setmodalCartList(false)}
          popoverStyle={{
            backgroundColor: '#303030',
          }}
          from={
            <TouchableOpacity
              style={{
                height: '100%',
                width: '10%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => setmodalCartList(true)}>
              <Image
                source={require('../assets/ubeatz/menu.png')}
                style={{
                  width: 20,
                  height: 20,
                  transform: [{rotate: '180deg'}],
                }}
              />
            </TouchableOpacity>
          }>
          <>
            <TouchableOpacity
              onPress={() => {
                props.OnPlay();
                setmodalCartList(false);
                props.PlayCondition(true);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/edit.png')}
                resizeMode="contain"
                style={{
                  height: 25,
                  width: 25,
                  transform: [{rotate: '90deg'}],
                }}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Edit
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                props.Delete();
                setmodalCartList(false);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/delete.png')}
                resizeMode="contain"
                style={{height: 25, width: 25}}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Delete
              </Text>
            </TouchableOpacity>
          </>
        </Popover> */}
      </View>
    );
  } else {
    return (
      <View
        style={{
          height: 60,
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#dbd7d7',
          paddingLeft: 5,
          paddingRight: 5,
        }}>
        <View
          style={{
            height: 55,
            width: '100%',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text
            style={{
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.onIndex + 1}
            {'. '}
          </Text>
          <Text
            style={{
              flex: 1,
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.name}
          </Text>
          <Text
            style={{
              flex: 0.2,
              fontSize: 13,
              color: '#707070',
              fontWeight: 'bold',
            }}>
            {props.qty}x
          </Text>
          <NumericFormat
            value={props.total}
            displayType="text"
            thousandSeparator
            prefix="Rp "
            renderText={value => (
              <Text
                style={{
                  flex: 0.5,
                  fontSize: 13,
                  fontWeight: 'bold',
                  color: '#707070',
                }}>
                {value}
              </Text>
            )}
          />

          <TouchableOpacity
            onPress={() => {
              props.Delete();
            }}
            style={{
              padding: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Ionicons name="trash-sharp" color={'#707070'} size={25} />
          </TouchableOpacity>
        </View>

        {/* <Popover
          isVisible={modalCartList}
          onRequestClose={() => setmodalCartList(false)}
          popoverStyle={{
            backgroundColor: '#303030',
          }}
          from={
            <TouchableOpacity
              style={{
                height: '100%',
                width: '10%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => setmodalCartList(true)}>
              <Image
                source={require('../assets/ubeatz/menu.png')}
                style={{
                  width: 20,
                  height: 20,
                  transform: [{rotate: '180deg'}],
                }}
              />
            </TouchableOpacity>
          }>
          <>
            <TouchableOpacity
              onPress={() => {
                props.OnPlay();
                setmodalCartList(false);
                props.PlayCondition(true);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/edit.png')}
                resizeMode="contain"
                style={{
                  height: 25,
                  width: 25,
                  transform: [{rotate: '90deg'}],
                }}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Edit
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                props.Delete();
                setmodalCartList(false);
              }}
              style={{
                padding: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/executive/delete.png')}
                resizeMode="contain"
                style={{height: 25, width: 25}}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  fontSize: 18,
                  fontWeight: '700',
                  paddingLeft: 10,
                }}>
                Delete
              </Text>
            </TouchableOpacity>
          </>
        </Popover> */}
      </View>
    );
  }
};
export default CartList;
