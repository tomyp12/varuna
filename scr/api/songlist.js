// @flow
import {APICore} from './apiCore';

const api = new APICore();

// function APICore_ListSonglist(params: any): any {
//   const baseUrl =
//     '/song/searching/?cari=' +
//     params.cari +
//     '&mode=' +
//     params.mode +
//     '&category=' +
//     params.category +
//     '&keyword=' +
//     params.keyword +
//     '';
//   return api.get(`${baseUrl}`, params);
// }
function APICore_ListSonglist(params: any): any {
  const baseUrl = `/song/searching/?cari=${params.cari}&mode=${params.mode}&category=${params.category}&keyword=${params.keyword}`;
  // const baseUrl = `${global.URL_SongList}/song/searching/?cari=${params.cari}&mode=${params.mode}&category=${params.category}&keyword=${params.keyword}`;

  return api.get(`${baseUrl}`);
}

export {APICore_ListSonglist};
