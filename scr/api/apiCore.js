import jwtDecode from 'jwt-decode';
import axios, {Axios} from 'axios';
import config from '../../config';

export const BASE_URL = config.API_URL;
// export const BASE_URL = global.DB;
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.baseURL = BASE_URL;

// intercepting to capture errors
axios.interceptors.response.use(
  response => {
    // console.log(response.results);
    return response;
  },
  error => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    let message;

    if (error && error.response && error.response.status === 404) {
      // window.location.href = '/not-found';
    } else if (error && error.response && error.response.status === 403) {
      window.location.href = '/access-denied';
    } else {
      switch (error.response.status) {
        case 401:
          message = 'Invalid credentials';
          break;
        case 403:
          message = 'Access Forbidden';
          break;
        case 404:
          message = 'Sorry! the data you are looking for could not be found';
          break;
        default: {
          message =
            error.response && error.response.data
              ? error.response.data['message']
              : error.message || error;
        }
      }
      return Promise.reject(message);
    }
  },
);

/**
 * Sets the default Product
 * @param {*} songlist
 */

const SOSMED_SESSION_SONGLIST = 'songlist';
const SOSMED_SESSION_USER = 'user';

const SOSMED_SESSION_STB = 'stb';

const setUserFromSession = () => {
  const user = sessionStorage.getItem(SOSMED_SESSION_USER);
  return user ? (typeof user == 'object' ? user : JSON.parse(user)) : null;
};

const setSonglistFromSession = () => {
  const songlist = sessionStorage.getItem(SOSMED_SESSION_SONGLIST);
  return songlist
    ? typeof songlist == 'object'
      ? songlist
      : JSON.parse(songlist)
    : null;
};

const setSTBFromSession = () => {
  const stb = sessionStorage.getItem(SOSMED_SESSION_STB);
  return stb ? (typeof stb == 'object' ? stb : JSON.parse(stb)) : null;
};

class APICore {
  /**
   * Fetches data from given url
   */
  get = (url, params) => {
    let response;

    if (params) {
      var queryString = params
        ? Object.keys(params)
            .map(key => key + '=' + params[key])
            .join('&')
        : '';
      response = axios.get(`${url}?${queryString}`, params);
    } else {
      response = axios.get(`${url}`, params);
    }
    return response;
  };

  getFile = (url, params) => {
    let response;
    if (params) {
      var queryString = params
        ? Object.keys(params)
            .map(key => key + '=' + params[key])
            .join('&')
        : '';
      response = axios.get(`${url}?${queryString}`, {responseType: 'blob'});
    } else {
      response = axios.get(`${url}`, {responseType: 'blob'});
    }
    return response;
  };

  getMultiple = (urls, params) => {
    const reqs = [];
    let queryString = '';
    if (params) {
      queryString = params
        ? Object.keys(params)
            .map(key => key + '=' + params[key])
            .join('&')
        : '';
    }

    for (const url of urls) {
      reqs.push(axios.get(`${url}?${queryString}`));
    }
    return axios.all(reqs);
  };

  /**
   * post given data to url
   */
  create = (url, data) => {
    return axios.post(url, data);
  };

  /**
   * Updates patch data
   */
  updatePatch = (url, data) => {
    return axios.patch(url, data);
  };

  /**
   * Updates data
   */
  update = (url, data) => {
    return axios.put(url, data);
  };

  /**
   * Deletes data
   */
  delete = url => {
    return axios.delete(url);
  };

  /**
   * post given data to url with file
   */
  createWithFile = (url, data) => {
    const formData = new FormData();
    for (const k in data) {
      formData.append(k, data[k]);
    }

    const config = {
      headers: {
        ...axios.defaults.headers,
        'content-type': 'multipart/form-data',
      },
    };
    return axios.post(url, formData, config);
  };

  // /**
  //  * post given data to url with file
  //  */
  // updateWithFile = (url, data) => {
  //     const formData = new FormData();
  //     for (const k in data) {
  //         formData.append(k, data[k]);
  //     }

  //     const config = {
  //         headers: {
  //             ...axios.defaults.headers,
  //             'content-type': 'multipart/form-data',
  //         },
  //     };
  //     return axios.patch(url, formData, config);
  // };

  // isUserAuthenticated = () => {
  //     const user = this.getLoggedInUser();
  //     if (!user || (user && !user.token)) {
  //         return false;
  //     }
  //     const decoded = jwtDecode(user.token);
  //     const currentTime = Date.now() / 1000;
  //     if (decoded.exp < currentTime) {
  //         console.warn('access token expired');
  //         return false;
  //     } else {
  //         return true;
  //     }
  // };

  // setLoggedInUser = (session) => {
  //     if (session) sessionStorage.setItem(AUTH_SESSION_KEY, JSON.stringify(session));
  //     else {
  //         sessionStorage.removeItem(AUTH_SESSION_KEY);
  //     }
  // };

  // /**
  //  * Returns the logged in user
  //  */
  // getLoggedInUser = () => {
  //     return getUserFromSession();
  // };

  // setUserInSession = (modifiedUser) => {
  //     let userInfo = sessionStorage.getItem(AUTH_SESSION_KEY);
  //     if (userInfo) {
  //         const { token, user } = JSON.parse(userInfo);
  //         this.setLoggedInUser({ token, ...user, ...modifiedUser });
  //     }
  // };
}

export {APICore, setSonglistFromSession, setUserFromSession, setSTBFromSession};
