// @flow
import {APICore} from './apiCore';

const api = new APICore();

function APICore_STBCheckinCode(params: any): any {
  const baseUrl = '/devices/booth-devices-checkin-code/';
  // console.log(api.update(`${baseUrl}`, params));
  return api.update(`${baseUrl}`, params);
}

function APICore_STBServer(params: any): any {
  // const baseUrl = 'http://8.215.68.178:9002/tools/devices-details/?code=TST';
  const baseUrl = `${params.ApiDB}/tools/devices-details/?code=${params.CodeOutlet}`;

  return api.get(`${baseUrl}`);
}

export {APICore_STBCheckinCode, APICore_STBServer};
