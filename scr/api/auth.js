// @flow
import { APICore } from './apiCore';

const api = new APICore();

function APICore_ListUser(params: any): any {
    const baseUrl = '/user/login/';
    return api.create(`${baseUrl}`, params);
}

export { APICore_ListUser };
